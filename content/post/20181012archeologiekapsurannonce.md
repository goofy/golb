
---
title: "Archéologie du capitalisme de surveillance"
date: 2018-10-12
author: "Christophe Masutti"
image: "/images/postimages/say.png"
description: "Annonce d'une prochaine publication"
tags: ["Libres propos", "Histoire", "Capitalisme de surveillance", "Publication"]
categories:
- Libres propos
---

Avec le temps, cela devait finalement arriver. Me voici embarqué dans l'écriture d'un ouvrage à paraître chez C&amp;F Éditions au printemps prochain. Une archéologie du capitalisme de surveillance, rien que cela. Mais c'est garanti, je ne me prendrai pas pour un penseur post-moderne (ils avaient tort&nbsp;!).

De quoi cet ouvrage va-il parler? D'abord le titre est tout à fait provisoire, qu'on se le dise. C'était juste pour vous faire cliquer [ici](https://fr.wikipedia.org/wiki/Pi%C3%A8ge_%C3%A0_clics). Blague à part, voici un petit argumentaire qui, je l'espère pourra vous faire patienter les quelques mois qui nous séparent d'une publication...

Christophe Masutti, *Archéologie du capitalisme de surveillance*, Caen, C&amp;F Éditions, 2019 (à paraître).

## Argumentaire


L'expression « capitalisme de surveillance » est employée le plus souvent non comme un concept mais comme un dénominateur, c'est-à-dire de l'ordre de la perception des phénomènes, de ceux qui nous font tomber de nos chaises presque tous les jours lorsque nous apprenons à quels trafics sont mêlées nos données personnelles. Mais il n'a pas été défini pour cela : son ambition est surtout d'être un outil, une clé de lecture pour comprendre la configuration politique et sociale de l'économie de la surveillance. Il faut donc mettre à l'épreuve ce concept et voir dans quelle mesure il permet de comprendre si l'économie de la surveillance obéit ou non à une idéologie, au-delà des pratiques. Certes, il faut donner une définition du capitalisme de surveillance (idéologique, pratique, sociale, collective, culturelle, anthropologique ou politique), mais il faut surtout en comprendre l'avènement.


Je propose dans ce livre une approche historique qui commence par une lecture différente des révolutions informatiques depuis les années 1960. Comment vient la surveillance? comment devient-elle un levier économique&nbsp;? Il faut contextualiser la critique du contrôle (en particulier par les philosophies post-modernes et les observateurs des années 1970) telle qu'elle s'est faite dans la continuité de la révolution informatique. On peut focaliser non pas sur l'évolution technologique mais sur le besoin d'information et de traitement de l'information, tout particulièrement à travers des exemples de projets publics et privés. L'informatisation est un mouvement de transformation des données en capital. Cet ouvrage sera parsemé d'études de cas et de beaucoup de citations troublantes, plus ou moins visionnaires, qui laissent penser que le capitalisme de surveillance, lui aussi, est germinal.


Qu'est-ce qui donne corps à la société de la surveillance&nbsp;? c'est l'apparition de dispositifs institutionnels de vigilance et de régulation, poussés par un débat public, politique et juridique, sur fond de crainte de l'avènement de la société de *[1984](https://fr.wikipedia.org/wiki/1984_(roman))*</a>. C'est dans ce contexte qu'au sein de l'appareillage législatif germèrent les conditions des capacités de régulation des institutions. Néanmoins la valorisation des données, en tant que propriétés, capitaux et composantes stratégiques, fini par consacrer l'économie de la surveillance comme modèle unique. Le marketing, la modélisation des comportements, la valeur vie client : la surveillance devient une activité prédictive.


Le capitalisme de surveillance est-il une affaire culturelle&nbsp;? On peut le concevoir comme une tendance en économie politique, ainsi que les premiers à avoir défini le capitalisme de surveillance proposent une lecture macro-politico-économique ([John Bellamy Foster, Robert W. McChesney, 2014](https://monthlyreview.org/2014/07/01/surveillance-capitalism/)). On peut aussi se concentrer sur les pratiques des firmes comme Google et plus généralement sur les pratiques d'extraction et de concentration des big datas ([Shoshana Zuboff, 2018](https://www.campus.de/buecher-campus-verlag/wirtschaft-gesellschaft/wirtschaft/das_zeitalter_des_ueberwachungskapitalismus-15097.html)). On peut aussi conclure à une forme de radicalité moderne des institutions dans l'acceptation sociale (collective&nbsp;?) de ce capitalisme de surveillance (on se penchera notamment sur les travaux d'[Anthony Giddens](https://fr.wikipedia.org/wiki/Anthony_Giddens) puis sur ceux de [Bernard Stiegler](https://fr.wikipedia.org/wiki/Bernard_Stiegler)). Que peut-on y opposer&nbsp;? Quels choix&nbsp;? À partir du constat extrêmement sombre que nous allons dresser dans cet ouvrage, peut-être que le temps est venu d'un mouvement réflexif et émancipateur.



