
---
title: "Le poids des cartables : de la maltraitance par négligence"
date: 2016-05-12
author: "Christophe Masutti"
image: "/images/postimages/typewriter.png"
description: "Les sacs d'écol ont toujorus été lourds, très lourds. Mais à y regarder de plus près, ce poids si néfaste pour le dos des enfants relève surtout d'une négligence de la part du corps enseignant"
tags: ["Libres propos", "Collège", "Santé", "Enfance"]
categories:
- Libres propos
---


Depuis de nombreuses années, le poids des cartables (surtout à l'école et collège) fait l'objet de questionnements, circulaires et préconisations. Il s'agit d'un problème de santé publique grave qui, à ce jour, ne semble pas faire l'objet de la préoccupation qu'il mérite dans la conscience du corps enseignant.


La question est même internationale. Elle a déjà fait l'objet de recommandations et de politiques publiques dores et déjà appliquées dans les pays. On relève cependant que l'American School Health Association a procédé à [une comparaison de nombreuses études scientifiques](https://www.ncbi.nlm.nih.gov/pubmed/23517005). Ces dernières ne présentent pas de résultats harmonisés. Ainsi le poids maximal du cartable, toutes études confondues, est compris entre 5% et 20% de l'élève. Néanmoins, l'Institut allemand de normalisation (DIN) s'est depuis longtemps [prononcé en faveur de 10%](http://www.spiegel.de/lebenundlernen/schule/aechzende-schulkinder-was-darf-ein-ranzen-wiegen-a-579357.html) du poids de l'élève. Ce pourcentage a été repris officiellement par le Ministère Français de l'Éducation Nationale à l'occasion de la circulaire du Ministre Xavier Darcos en 2008 (cf. plus bas).

En septembre 2016, alors même que mon fils entrait au collège pour la première fois, le poids de son cartable était de 9&nbsp;kg, **soit 30% du poids du porteur&nbsp;!** Au fil des rencontres avec les enseignants, soit lors des réunions de parents d'élèves, soit lors des conseils de classe, je soumettais cette question, recevant une attention unanime de principe mais aucune volonté, de la part du corps enseignant, d'améliorer concrètement la situation. Pire&nbsp;: chaque enseignant, questionné séparément, insiste toujours sur l'importance d'amener chaque jour les livres et les cahiers relatifs à sa matière enseignée, sans prendre en compte les implications évidentes en termes de santé de l'enfant.

Tous les enfants sont concernés par cette question, mais surtout les demi-pensionnaires. Comme je vais le  montrer, les solutions sont simples à mettre en œuvre mais changent aussi les pratiques des professeurs. J'affirme par conséquent que le poids du cartable et l'absence de volonté de la part des enseignants d'adapter leurs pratiques à cette question de santé publique, relève de la maltraitance par négligence.

## Il y a d'abord le Ministère

Saisi depuis longtemps par les instances représentant les parents d'élèves, le Ministère de l'Éducation Nationale a déjà montré sa préoccupation face au constat d'alerte en santé publique que représente le poids des cartables chez les enfants. Ainsi, la [Note Ministérielle du 17 octobre 1995](http://archive.wikiwix.com/cache/?url=http%3A%2F%2Fwww.fcpe.asso.fr%2Fewb_pages%2Fl%2Fliste_textes_de_references_1328.php) «&nbsp;poids des cartables&nbsp;» (BO num. 39 du 26 octobre 1995) mentionne :

> (…) les membres de la communauté éducative doivent se sentir concernés par ce problème et ont un rôle à jouer, dans ce domaine, chacun en fonction de ses responsabilités.
> 
> Les enseignants peuvent veiller à limiter leurs demandes en matière de fournitures scolaires,(…)
> 
> une réduction du poids des cartables (qui ne devrait pas dépasser 10 % du poids moyen des élèves) (…)


Les rappels furent nombreux. La question soulevée à de multiples reprises tant au Parlement qu'au Sénat. On trouve en 2008, soit 13 ans plus tard, une circulaire officielle ([2008-002 du 11-1-2008](http://www.education.gouv.fr/bo/2008/3/MENE0701925C.htm)) et assez complète, signée du Ministre de l'Éducation Nationale Xavier Darcos, dont l'introduction commence par :

> Le poids du cartable est une question de santé publique pour nos enfants&nbsp;: je souhaite que les établissements scolaires s’emparent de cette question, dès à présent, dans le cadre de la prévention du mal de dos en milieu scolaire.


On peut néanmoins regretter qu'en 2016, 21 ans plus tard, à l'occasion de la circulaire  Ministérielle relative aux fournitures scolaires ([2016-054 du 13-4-2016](http://www.education.gouv.fr/pid285/bulletin_officiel.html?cid_bo=100761)), il soit tout juste mentionné que «&nbsp;Les cahiers au format 24&nbsp;x&nbsp;32&nbsp;cm jugés trop lourds ne figurent plus sur la liste indicative depuis 2014&nbsp;», sans autre allusion à l'impact de cette liste déjà très importante sur le poids total du cartable&nbsp;!

En somme, lorsqu'il s'agit de porter haut cette question de santé publique, les élus et les ministres se mobilisent et font des déclarations. Mais lorsqu'il s'agit de gouverner effectivement le corps enseignant, et limiter concrètement le poids du cartable, aucun ordre n'est donné.

## Quelles solutions ?

Régulièrement on voit quelques élus locaux plaider pour «&nbsp;les tablettes à l'école&nbsp;», ou  mobiliser de l'argent public pour promouvoir des solutions technologiques coûteuses pour des questions essentiellement organisationnelles. Les solutions peuvent être beaucoup simples.

### Les livres scolaires

Lors du choix et de l'achat d'une collection de manuels scolaires, nombreux sont les éditeurs qui proposent d'accompagner cet achat par une version électronique utilisable en classe. Ainsi, il est toujours possible, pour l'enseignant, de projeter les pages du manuel.

En classe, il s'agit concrètement d'allumer un ordinateur et un vidéo-projecteur. Si toutes les classes n'en sont pas pourvues, il reste néanmoins que les établissements proposent au moins des solutions équivalentes (comme un ordinateur et un vidéo projecteur portables).

Pour ce qui concerne le poids des livres scolaires, la question peut donc se régler à la fois facilement et pour un coût négligeable (même si, à l'échelle d'une académie, quelques rares établissements sont encore à équiper).

Ne pas opter pour cette solution, alors même que l'équipement suffisant est présent, relève donc de la négligence pure et simple, littéralement «&nbsp;sur le dos&nbsp;» de tous les enfants.

### Les cahiers

Même si le fameux cahier aux dimensions 24&nbsp;x&nbsp;32 est déconseillé par la circulaire ministérielle, certains enseignants continuent de le réclamer dans la liste des fournitures scolaires. La raison &nbsp;: il s'agit de pouvoir y coller des feuilles au format A4, c'est-à-dire des supports de cours préparés par l'enseignant mais qui pourraient très bien, moyennant un minimum de cosmétique, passer dans un cahier de format plus petit.

Que dire des enseignants réclamant des classeurs rigides et autant de feuilles de réserve, en plus des cahiers&nbsp;?

Toujours est-il que l'un principaux facteurs d'alourdissement du cartable repose sur l'utilisation de cahiers, quel que soit leurs formats&nbsp;: en longueur d'année, la plupart de ces cahiers obligent les élèves à transporter autant de feuilles inutilisées dans la journée (et même parfois sur l'année entière). Le cahier, en soi, est un instrument bien encombrant puisqu'il consiste à trimballer des pages qui, par définition, sont inutiles chaque jour : à quoi bon promener les cours du mois dernier ou de la séquence passée ?


Là encore, il existe au moins une solution dont le seul prix à payer réside dans la mobilisation de quelques heures d'apprentissage et une surveillance un peu plus étroite&nbsp;: le parapheur (ou du moins un seul classeur souple et fin muni d’intercalaires). En pratique, il s'agit d'apprendre aux élèves à classer par matière les éléments de cours dans un parapheur général durant la semaine, puis classer l'ensemble à la maison dans des classeurs séparés (ou même un seul gros classeur). On peut aussi imaginer une évaluation spécifique en fin de trimestre où l'élève ramène exceptionnellement son classeur de la maison (pour rappel, ce classeur est même censé peser moins lourd que tous les cahiers confondus transportés chaque jour par l'élève).

Ce type d'apprentissage organisationnel fait partie des apprentissages et compétences demandés à chaque élève au collège. Il n'y a donc aucune contre-indication à ce que cette pratique soit systématique, bien au contraire.

### Les autres fournitures

Quant aux autres fournitures demandées par les enseignants, elles sont tantôt nécessaires (comme l'équipement de sport) tantôt exagérées ou même hors de propos. Il en va ainsi des tubes de gouaches demandés à chaque séance d'Art Plastique, des supports de partition (type porte-bloc) jamais utilisés, etc. Les anecdotes ne manquent pas.

<h3>Les fausses solutions</h3>

Quelques collèges (c'est le cas de celui de mon fils) proposent des casiers. Généralement en nombre modeste, ces casiers sont parfois réservés au élèves de sixième demi-pensionnaires. S'ils permettent effectivement de stocker le casque de vélo et autres affaires non-scolaires, l'usage des casiers est en réalité une fausse solution pour deux raisons&nbsp;:

- dans une journée de cours, l'élève n'a pas la possibilité d'effectuer de multiples allers-retours d'un bout à l'autre du collège pour récupérer les affaires dont il a besoin,
- le problème du poids des cartables se pose moins à l'intérieur du collège (où le cartable peut très bien être posé à terre) qu'à l'extérieur sur le chemin du collège, car c'est bien là que les risques associés se déclarent&nbsp;: risque en santé (scolioses, troubles musculo-squelettiques), mais aussi en sécurité (essayez, par exemple, de faire du vélo avec un sac pesant 30% de votre propre poids&hellip;).


Quant au partage des manuels entre élèves, l'idée commune que les enseignants suggèrent régulièrement comme s'il s'agissait de la trouvaille du siècle, il ne résout qu'une petite partie du problème et encore, il repose sur l'organisation générale des élèves entre eux, alors même que l'organisation des enseignants entre eux pourrait résoudre beaucoup plus de choses.

## De la négligence

Pour conclure, j'insiste sur ce point&nbsp;: si depuis si longtemps le poids des cartables n'a pas baissé et compte-tenu de son impact connu de tous en termes de santé publique, ce n'est pas seulement parce que les pratiques des enseignants ne sont pas adaptées, c'est parce qu'il y a un refus systématique de la part des enseignants de changer leurs propres pratiques au détriment des élèves. C'est une forme de maltraitance avérée. Car en effet comment expliquer que seuls certains établissements parviennent à faire les efforts organisationnels nécessaires alors que la majorité mène une politique de laisser-faire particulièrement condamnable&nbsp;?

Il est temps de mettre fin à cette maltraitance inadmissible, parfois ignorée des parents car il existe peu de prévention, et pour cause&nbsp;: comment un ministère pourrait-il informer les parents qu'un cartable ne doit pas dépasser 10% du poids de l'élève, alors même que les enseignants encouragent, par leurs pratiques, au transport de sacs de plus de 30% de ce poids&nbsp;?
