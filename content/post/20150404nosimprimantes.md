---
title: "Nos imprimantes nous espionnent-elles?"
date: 2015-04-04
author: "Christophe Masutti"
image: "/images/postimages/network.png"
tags: ["Libres propos", "Espionnage", "Libertés", "Politique"]
description: "Habituez-vous à ne plus regarder votre imprimante comme avant."
categories:
- Libres propos
---


En 2008, l'[Electronic Frontier Foundation](https://www.eff.org/) faisait [une annonce](https://www.eff.org/issues/printers) pour le moins étonnante&nbsp;: les imprimantes nous espionnent ! Pour cette organisation internationale de défense de la liberté d'expression sur Internet, puissance incontournable de l'analyse et des actions juridiques relevant des libertés individuelles dans le contexte de notre monde numérisé, la question ne se limitait pas à ce que pouvaient ou non divulguer les imprimantes. En 2015, après les affaires Snowden (international) et en plein dans notre débat franco-français sur le projet de loi Renseignement, une [nouvelle information](https://www.eff.org/pages/list-printers-which-do-or-do-not-display-tracking-dots) a tendance à passer inaperçue&nbsp;: tous les fabricants majeurs d'imprimantes ont passé des accords secrets avec les gouvernements. Voici un petit retour *in French* sur l'action de l'EFF et cette révélation.



C'est déjà une affaire [vieille de 10 ans](https://www.eff.org/fr/wp/investigating-machine-identification-code-technology-color-laser-printers). En 2005, déjà, alertée par des suspicions de pratiques de stéganographie cachée par les fabricants de certaines imprimantes couleur laser, l'EFF avait cracké le code des imprimantes Xerox Docucolor et [publié la méthode](https://w2.eff.org/Privacy/printers/docucolor/). Pour l'EFF, même si on pouvait comprendre que ce genre de pratique s'inscrivait légitimement dans le cadre de la lutte contre la contrefaçon de monnaie, il s'agissait surtout de se demander quel pouvait être l'usage de ces informations, leur accès par des services gouvernementaux et les éventuels recours des citoyens. Si, en effet, vous imprimez un document et souhaitez le diffuser ou pour le compte de quelqu'un d'autre dont vous ignorez l'usage qu'il fera du document, le fait que l'on puisse vous identifier comme l'imprimeur est une violation caractérisée de la vie privée. Imaginez par exemple que l'on puise vous identifier comme étant celui qui a imprimé le tract d'appel à manifestation qui vient d'être affiché à la cantine&nbsp;?

Voici, ci-dessous, une traduction des éléments majeurs de l'annonce de l'EFF en 2008 et des actions envisagées.

> Et si, à chaque fois que vous imprimez un document, ce dernier comprenait un code secret permettant d'identifier votre imprimante — et potentiellement la personne qui l'utilise. On se croirait dans un épisode d'Alias, non&nbsp;?
> 
> Malheureusement, ce scénario n'a rien d'une fiction. Dans sa soi-disante lutte contre la contrefaçon, le gouvernement américain a réussi à persuader certains fabricants d'imprimantes laser couleur d'encoder chaque page avec des informations d'identification. Cela signifie que sans que vous le sachiez ou sans votre consentement, vos actions privées peuvent devenir publiques. Un outil de communication quotidien peut devenir un outil de surveillance gouvernementale. Le pire, c'est qu'il n'y a pas de loi pour en prévenir les abus.
> 
> L'[ACLU](https://www.aclu.org/) (American Civil Liberties Union) a récemment publié un rapport révélant que le FBI a récolté plus de 1100 pages sur l'organisation depuis 2001 ainsi que des documents concernant d'autres groupes non-violents dont Greenpeace et United for Peace and Justice. Dans le climat politique actuel, il n'est pas difficile d'imaginer que le gouvernement use de la possibilité de déterminer qui a pu imprimer ces documents, à des fins bien différentes que celle de la lutte contre la contrefaçon.
> 
> Pourtant, il n'y a aucune loi qui empêche les Services Secrets d'utiliser les codes des imprimantes pour tracer secrètement l'origine de documents n'ayant aucun rapport avec la monnaie ; seule la politique de confidentialité du fabricant de votre imprimante vous protège (si toutefois elle existe). Et aucune loi ne défini quels type de documents les Services Secrets ou toute autre agence gouvernementale nationale ou étrangère sont autorisés à demander pour l'identification, sans mentionner comment un tel outil de police scientifique pourrait être développé et implémenté dans les imprimantes.
> 
> En l'absence de loi sur les livres, il n'y a rien pour arrêter les violations de la vie privée que cette technologie permet. Pour cette raison, l'EFF rassemble les informations sur ce que révèlent les imprimantes et comment elles le font —une prospective nécessaire à toute contestation judiciaire ultérieure ou à une nouvelle législation visant à protéger la vie privée. Et nous pourrions avoir besoin de votre aide.
> 
> (…) Par ailleurs, afin de documenter ce que les imprimantes révèlent, l'EFF a déposé une demande relevant de la [Freedom of Information Act (FOIA)](http://fr.wikipedia.org/wiki/Freedom_of_Information_Act) et nous vous tiendrons au courant de ce que nous découvrirons. En attendant, nous vous invitons à passer le mot (…)


Peu de temps après, l'EFF publia [une liste](https://www.eff.org/pages/list-printers-which-do-or-do-not-display-tracking-dots) régulièrement mise à jour, recensant les imprimantes qui, d'après des tests, renvoyaient ou non des codes de traçabilité (en jaune) sur leurs sorties papier. Un nombre important de fabricants et d'imprimantes y sont mentionnés&nbsp;: cherchez bien, vous y trouverez peut-être votre matériel.

Et voici qu'en ce début de l'année 2015, les réponses à la demande FOIA sont révélées par l'EFF. Sur [cette même page](https://www.eff.org/pages/list-printers-which-do-or-do-not-display-tracking-dots), un *warning* nous apprend&nbsp;:

> (Ajouté en 2015) Plusieurs documents que nous avons préalablement reçu suite à notre requête [FOIA](http://fr.wikipedia.org/wiki/Freedom_of_Information_Act) suggèrent que tous les fabricants majeurs d'imprimantes laser couleur ont conclu un accord secret avec les gouvernements pour assurer une traçabilité permettant la recherche criminalistique. Bien que nous ne sachions pas encore si cela est correct, ou comment les générations suivantes de technologies traçantes pourront fonctionner, il est probablement plus sûr de supposer que toutes les imprimantes modernes laser couleur comprennent un moyen de traçabilité qui associe les documents avec leurs numéros de série.(…)


Pour conclure&nbsp;: dans le contexte actuel de mise en place de systèmes de surveillance généralisée, la traçabilité des documents imprimés prend une mesure que seule la lutte contre la contrefaçon ne suffit pas à justifier. Ne jetez pas encore vos vieilles machines à écrire et le papier carbone, il se peut que nous en ayons encore besoin ! Quant aux faits avérés par l'EFF, je dois dire que je suis davantage affolé par leurs implications que par le danger (bien réel, cependant) pour la liberté d'expression qu'ils représentent (eux aussi). D'une part, en effet, la liberté d'expression est bien plus menacée par des projets de lois qui cachent à peine les intentions des pouvoirs politiques en place. D'autre part, je m'interroge de plus en plus sur une société qui, découvrant peu à peu l'ampleur des atteintes à la vie privée que les démocraties organisent, commence à se figurer que les modèles démocratiques en place renient non seulement leurs principes mais dévoient aussi la confiance déjà chancelante que les citoyens leur accordent. Quelles sociétés et quels régimes politiques pourraient s'en accommoder encore longtemps&nbsp;?
