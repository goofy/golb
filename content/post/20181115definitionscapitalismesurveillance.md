---
title: "Définitions du capitalisme de surveillance"
date: 2018-11-15
author: "Christophe Masutti"
image: "/images/postimages/ai.png"
description: "Quelles définitions pour le capitalisme de surveillance? il devient urgent de se poser la question"
tags: ["Libres propos", "Capitalisme de surveillance", "Histoire", "Libertés"]
categories:
- Libres propos
---

Notre quotidien est enregistré, mesuré, considéré comme une somme de procédures dont la surveillance consiste à transformer l'apparent chaos (et notre diversité) en ordre. Tous les acteurs économiques et institutionnels y ont un intérêt. Pour beaucoup, c'est d'un nouveau capitalisme qu'il s'agit : le capitalisme de surveillance. Mais peut-on lui donner une définition claire&nbsp;? Je vais essayer…



Avec son récent ouvrage, *Das Zeitalter Des ÜberwachungsKapitalismus*[^1], Shoshana Zuboff nous livre une critique des plus intelligentes de ce qu'elle nomme le capitalisme de surveillance. Fruit d'un long travail dont les  traces sont visibles depuis cinq ans (en particulier dans un article intitulé «&nbsp;[Big Other...](https://papers.ssrn.com/sol3/papers.cfm?abstract_id=2594754)&nbsp;»[^2] et quelques [sorties remarquées](https://framablog.org/2017/03/28/google-nouvel-avatar-du-capitalisme-celui-de-la-surveillance/) dans le *Frankfurter Allgemeine Zeitung*[^3]), Shoshana Zuboff a intitulé son ouvrage en écho à son travail initial, en 1988, sur la mutation du travail en procédures et informations, *In The Age Of The Smart Machine*[^4].

[^1]: ZUBOFF, Shoshana. *Das Zeitalter Des ÜberwachungsKapitalismus*. Frankfurt: Campus Verlag, 2018. (À paraître en anglais : *The Age of Surveillance Capitalism: The Fight for a Human Future at the New Frontier of Power*. New York: Public Affairs, 2019).

[^2]: ZUBOFF, Shoshana. «&nbsp;Big Other: Surveillance Capitalism and the Prospects of an Information Civilization&nbsp;». *Journal of Information Technology* 30 (2015): 75‑89.

[^3]: Voir surtout ZUBOFF, Shoshana. «&nbsp;The Secrets of Surveillance Capitalism&nbsp;». *Frankfurter Allgemeine Zeitung*, mars 2016.

[^4]: Zuboff, Shoshana. *In The Age Of The Smart Machine: The Future Of Work And Power*. New York: Basic Books, 1988.

L’œuvre de S. Zuboff ne s'inscrit pas exclusivement dans une analyse des modèles économiques. Pour cela il faut plutôt aller voir du côté de Nick Srnicek, qui se penche sur les changements des modèles capitalistes dans leurs rapports aux technologies[^5]. Pour N. Srnicek, les technologies de l'information ne sont pas uniquement des leviers d'augmentation de productivité, mais elle conditionnent aussi une économie de plateformes rendue possible par l'essor des infrastructures numériques des années 1990, combinée à une baisse de rendement de l'industrie manufacturière. Avec l'éclatement de la bulle Internet des années 2000 puis la crise de 2008, des firmes monopolistiques sont apparues (les GAFAM) et sont devenues les figures incontournables de cette transformation du capitalisme.

[^5]:  SRNICEK, Nick. *Platform Capitalism*. Cambridge: Polity, 2016. Trad. Fr.: *Capitalisme de plateforme. L’ hégémonie de l’économie numérique*. Québec: Lux Éditeur, 2018.


S. Zuboff ne s'inscrit pas non plus (tout à fait) dans une approche du capitalisme de surveillance du point de vue de l'économie politique. C'est ce que font au contraire [J. B. Foster et R. W. McChesney](https://monthlyreview.org/2014/07/01/surveillance-capitalism/), initiateurs[^6] du concept de capitalisme de surveillance (voir billet précédent) qui montrent en quoi le capitalisme de surveillance est une résurgence hégémonique de l'esprit de la politique nord-américaine sur les trois axes : militaire (maintenir des états de guerre pour le contrôle des pays), financier (les clés de l'économie mondiale) et marketing (assurer la propagande). Cette approche macro-économico-historique est tout à fait intéressante mais elle a le tort de focaliser essentiellement sur des mécanismes institutionnels et sans doute pas assez sur les pratiques d'une économie de la surveillance qui n'est pas toujours à ce point dépendante du politique, bien au contraire[^0].

[^0]: Et c'est peut être aussi ce qui empêche S. Zuboff de produire une véritable critique du capitalisme. elle a en effet tendance à ne s'en tenir qu'à une analyse des méfaits des pratiques des « capitalistes de la surveillance », comme s'il fallait en réalité défendre un « bon » capitalisme contre de méchants voyous. Je pense au contraire qu'on ne peut pas faire l'économie d'une critique en règles des modèles capitalistes qui sont justement à la source des pratiques.

[^6]: John Bellamy Foster and Robert W. McChesney, «&nbsp;Surveillance Capitalism. Monopoly-Finance Capital, the Military-Industrial Complex, and the Digital Age&nbsp;», Monthly Review, 66, Juillet 2014.

S. Zuboff, elle, s'intéresse aux pratiques et propose une conception systémique du capitalisme de surveillance à partir de ses manifestations économiques et sociales. Pour cela, elle propose un point de vue holistique et une définition qui est en même temps un programme d'analyse et un questionnement sur l'économie de nos jours. On trouve cette définition au début de son dernier ouvrage. En voici une traduction :

> **Überwachungskapitalismus, der** -- **1.** Neue Marktform, die menschliche Erfahrung als kostenlosen Rohstoff für ihre versteckten kommerziellen Operationen der Extraktion, Vorhersage und des Verkaufs reklamiert; **2.** eine parasitäre ökonomische Logik, bei der die Produktion von Gütern und Dienstleistungen einer neuen globalen Architektur zur Verhaltensmodifikation untergeordnet ist; **3.** eine aus der Art geschlagene Form des Kapitalismus, die sich durch eine Konzentration von Reichtum, Wissen und Macht auszeichnet, die in der Menschheitsgeschichte beispiellos ist; **4.** Fundament und Rahmen einer Überwachungsökonomie; **5.** so bedeutend für die menschliche Natur im 21. Jh. wie der Industriekapitalismus des 19. und 20. Jhs. für die Natur an sich; **6.** der Ursprung einer neuen instrumentären Macht, die Anspruch auf die Herrschaft über die Gesellschaft erhebt und die Marktdemokratie vor bestürzende Herausforderungen stellt; **7.** zielt auf eine neue kollektive Ordnung auf der Basis totaler Gewissheit ab; **8.** eine Enteignung kritischer Menschenrechte, die am besten als Putsch von oben zu verstehen ist – als Sturz der Volkssouveränität.
> 
> **Surveillance Capitalism, n.** --  **1.** A new economic order that claims human experience as free raw material for hidden commercial practices of extraction, prediction, and sales; **2.** A parasitic economic logic in which the production of goods and services is subordinated to a new global architecture of behavioral modification; **3.** A rogue mutation of capitalism marked by concentrations of wealth, knowledge, and power unprecedented in human history; **4.** The foundational framework of a surveillance economy; **5.** As significant a threat to human nature in the twenty-first century as industrial capitalism was to the natural world in the nineteenth and twentieth; **6.** The origin of a new instrumentarian power that asserts dominance over society and presents startling challenges to market democracy; **7.** A movement that aims to impose a new collective order based on total certainty; **8.** An expropriation of critical human rights that is best understood as a coup from above: an overthrow of the people’s sovereignty.
> 
> **Capitalisme de surveillance, le** --   **1.** un nouvel ordre économique qui revendique le vécu humain comme matière première gratuite pour des pratiques commerciales occultes d'extraction, de prévision et de vente ; **2.** une logique économique parasitaire dans laquelle la production de biens et de services est subordonnée à une nouvelle architecture globale de modification des comportements ; **3.** un type de capitalisme malhonnête, sans précédent dans l'histoire humaine, caractérisé par une concentration des richesses, des connaissances et du pouvoir ; **4.** le cadre fondateur d'une économie de surveillance ; **5.** une menace aussi grave pour la nature humaine au XXI<sup>e</sup> siècle que le capitalisme industriel des XIX<sup>e</sup> et XX<sup>e</sup> siècles l'était pour le monde naturel ; **6.** la source d'un nouveau pouvoir instrumental qui affirme sa domination sur la société et impose des défis déconcertants à la démocratie de marché ; **7.** un mouvement qui vise à imposer un nouvel ordre collectif qui repose sur une certitude absolue ; **8.** une spoliation des droits humains essentiels, que l'on peut comprendre au mieux comme un putsch venu d'en haut, un renversement de la souveraineté populaire.



Étant donné la fraîcheur des travaux de S. Zuboff et la nouveauté du concept de capitalisme de surveillance (2014), il est difficile de proposer une lecture critique qui ne soit pas biaisée, d'une manière ou d'une autre, par la dimension opératoire que l'on souhaite lui donner. L'utilise-t-on pour expliquer les modèles de l'économie des plateformes&nbsp;? en quoi joue-t-il un rôle dans les politiques de régulation des États&nbsp;? interfère-t'il avec le politique et comment&nbsp;? Est-ce une manifestation d'une volonté de pouvoir (de la part de qui&nbsp;?) ou une émergence plus ou moins spontanée issue de plusieurs circonstances ou idéologies&nbsp;? etc.



Toutes ces questions doivent être posées et c'est aussi pourquoi j'ai entrepris un ouvrage dont l'ambition est de proposer des pistes de réflexion. La première est justement de se demander en quoi le capitalisme de surveillance est tangible (et si une approche matérialiste peut en venir à bout). J'en suis assez vite arrivé à la conclusion que si l'on souhaite en faire l'histoire, il faut partir d'une définition qui utilise les concepts auxquels nous sommes déjà habitués.

Mon projet est donc très différent de celui de S. Zuboff, qui va jusqu'à inventer deux figures : celle de Big Other (l’altérite à laquelle nous renvoient nos doubles numériques) et les «&nbsp;coup des gens&nbsp;», qui, par opposition à un «&nbsp;coup d'État&nbsp;», est sorte d'accaparement offensif de nos vies privées et de notre quotidien par les GAFAM.

Cependant, d'un point de vue purement intellectuel, je ne peux m'empêcher de faire deux remarques à propos de la définition de S. Zuboff. La première, c'est qu'elle est surtout un programme d'analyse et présume largement que les pratiques du capitalisme de surveillance sont la traduction d'intentions, de jeux de domination et de pouvoirs. La seconde, c'est que les différents points de définitions peuvent en fait correspondre à des constructions proposées par d'autres auteurs du mouvement des *surveillance studies* depuis les années 1970. Une liste apparaît dans [un article de Gary T. Marx](http://web.mit.edu/gtmarx/www/surv_studies.pdf)[^7], par exemple : la société disciplinaire, la société du dossier, la société du contrôle, le post-panoptisme, la dataveillance, la société transparente, l'informatisation ubiquitaire, l'uberveillance. Il s'agit d'autant d'approches différentes du capitalisme de surveillance. Dans cette mesure, on peut remercier S. Zuboff de faire l'éclatante démonstration, en une seule analyse, de ce que les *surveillance studies* avaient jusqu'à présent quelques difficultés à cerner, sans doute trop concentrées sur les rapports entre technique et société, d'un point de vue sociologique ou anthropologique. Mais S. Zuboff va-t-elle au bout de son projet, qui appelle une critique du capitalisme&nbsp;? Rien n'est moins sûr.

[^7]: MARX, Gary T. «&nbsp;Surveillance studies&nbsp;». Dans : James Wright (éd.), *International Encyclopedia of the Social and Behavioral Sciences*, Amsterdam: Elsevier, 2015, p. 733‑41.


Pour ma part, comme je compte faire une histoire, il me faut aussi satisfaire à l'exigence d'une définition. Or, mon problème est d'intégrer aussi bien l'historiographie que les témoignages (par exemple des rapports) ou des documents probants (comme des articles), sans pour autant y voir des manifestions de ce que, aujourd'hui, nous nommons le capitalisme de surveillance mais qui, sur une cinquantaine d'années, répond à bien d'autres noms. N'ayant pas le talent de S. Zuboff, je ne peux pas non plus entreprendre une analyse globale, voire holiste, en y intégrant les multiples dimensions historiques des sciences et des technologies.



Je préfère donc procéder en deux temps pour trouver une manière synthétique d'exprimer (sur la période 1960 à nos jours) ce qu'est le capitalisme de surveillance *a)* par ses pratiques et *b)* par ses mécanismes. C'est une définition qui me permet, dans cette *archéologie* que je propose, de procéder par étapes, de nommer ce que je cherche, comprendre  la naissance d'une économie de la surveillance et la logique progressive, non linéaire, du capitalisme de surveillance.



Je pose ainsi que *les pratiques de surveillance* sont :


> les procédés techniques les plus automatisés possible qui consistent à récolter et stocker, à partir des individus, de leurs comportements et de leurs environnements, des données individuelles ou collectives à des fins d'analyse, d'inférence, de quantification, de prévision et d'influence.



Et qu'il faut tâcher de situer le capitalisme de surveillance dans son parcours historique.


> L'histoire du capitalisme de surveillance est celle des transformations sociales que l'informatisation a rendu possibles depuis les années 1960 à nos jours en créant des modèles économiques basés sur le traitement et la valorisation des données personnelles. La prégnance de ces modèles ajoutée à une croissance de l'industrie et des services informatiques[^8] a fini par créer un marché hégémonique de la surveillance. Cette hégémonie associée à une culture consumériste se traduit dans plusieurs contextes : l'influence des consommateurs par le marketing, l'automatisation de la décision, la réduction de la vie privée, la segmentation sociale, un affaiblissement des politiques, un assujettissement du droit, une tendance idéologique au solutionnisme technologique. Il fait entrer en crise les institutions et le contrat social.
> 
> Du point de vue de ses mécanismes, le capitalisme de surveillance mobilise les pratiques d'appropriation et de capitalisation des informations pour mettre en œuvre le régime disciplinaire de son développement industriel (surveillance des travailleurs) et l'ordonnancement de la consommation (surveillance et influence des consommateurs). Des bases de données d'hier aux *big datas* d'aujourd'hui, il permet la concentration de l'information, des capitaux financiers et des technologies par un petit nombre d'acteurs tout en procédant à l'expropriation mercantile de la vie privée du plus grand nombre d'individus et de leurs savoirs[^9].

[^8]: Il s'agit aussi d'une corrélation entre une baisse de régime de l'industrie manufacturière (par exemple l'industrie automobile) et le déplacement des capitaux vers la surfinanciarisation de l'économie numérique.

[^9]: Nous pouvons ajouter, comme illustration du dernier point, que le capitalisme de surveillance exproprie l'individu de sa vie privée et de ses savoirs comme le capitalisme industriel a exproprié les travailleurs de leur maîtrise du temps en disciplinant au travail le salarié qui vivait, dans le monde rural, selon un tout autre rythme que celui des machines (les premières conquêtes sociales du monde ouvrier ont porté sur le temps de travail, sa rémunération, et les temps de repos). Voir à ce sujet Edward P. Thompson, *Temps, discipline du travail et capitalisme industriel*, Paris, La Fabrique, 2004.


Voilà pour ce qui me concerne. La suite se trouvera dans l'ouvrage à paraître. J'ajoute cependant que cette définition s'adresse aussi à certains (ils se reconnaîtront) qui me l'ont demandée à des fins pédagogiques (et sans doute lassés par mon style d'écriture qui rappelle souvent que les fontes d'imprimerie sont  en plomb).
