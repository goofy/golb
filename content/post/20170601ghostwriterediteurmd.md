
---
title: "Ghostwriter, un bon éditeur Markdown"
date: 2017-06-01
author: "Christophe Masutti"
image: "/images/postimages/terminal.png"
description: "Si vous cherchez un bon éditeur markdown, celui-ci rempli bien des attentes."
tags: ["Markdown", "bidouille", "pandoc", "éditeur"]
categories:
- Logiciel libre
---



Après plusieurs essais pour trouver l'éditeur Markdown qui convienne à mes besoins, je pense avoir trouvé avec [Ghostwriter](https://wereturtle.github.io/ghostwriter/) une réponse plutôt pertinente.



[Ghostwriter](https://wereturtle.github.io/ghostwriter/) mise avant tout sur la simplicité. C'est ce qu'on appelle un éditeur « distraction free ». Il pousse même le concept jusqu'à proposer trois mode d'édition&nbsp;:

- un mode normal,
- un mode « focus » qui permet de mettre en surbrillance la phrase (et non la ligne) que l'on est en train d'écrire (jusqu'à la ponctuation, en fait),
- un mode « Hemingway », qui désactive les touches ``backspace`` et ``delete`` pour se forcer à écrire comme avec une antique machine à écrire mécanique.


Mais cela, c'est du détail. Dans les fonctions plus opérationnelles, on note&nbsp;:


- une interface en panneaux, qui permet de passer le panneau principal où l'on écrit en mode plein écran, vraiment « distraction free »,
- la possibilité d'insérer des images en glisser-déposer directement dans l'interface,
- des fenêtres HUD (affichage tête haute, en anglais&nbsp;: Head-up display) qui permettent de sortir notamment l'affichage du sommaire du document et les statistiques. 
- Parmi les statistiques, on note aussi quatre types d'estimation&nbsp;: la durée de lecture, le pourcentage de mots complexes, et de manière corrélée la facilité de lecture et le niveau (facile, université, très difficile…). Tiens en écrivant le terme « corrélé » ci-dessus, je suis passé à « difficile », j'espère que cela ira…
- enfin, un autre panneau permet d'afficher le rendu HTML du document.


Concernant le rendu, une fonction très appréciable est le choix des rendus en fonction des moteurs de transcription de la saveur Markdown utilisée. Ainsi le rendu intègre le choix d'un rendu à la sauce Pandoc, Sundow, Multimarkdown, etc. Une autre fonction est le choix de la CSS qui servira à la fois pour le rendu en temps réel mais aussi pour une éventuelle exportation en HTML.

À propos d'export, le choix est très large et on doit souligner la grande propreté des fichiers produits&nbsp;: html, html5, odt, xml, rtf, docx, pdf (LaTeX), pdf (ConTeXt), epub2, epub3, Groff, LaTeX… Pour ce qui me concerne, j'exporte très souvent en LaTeX, en choisissant le moteur Pandoc, et c'est vraiment efficace. Évidemment l'autre solution est d'enregistrer le .md et le convertir avec Pandoc en ligne de commande.

Le reste est à l'avenant : correction orthographique, interface en plusieurs langues, personnalisation assez poussée de l'apparence de l'interface (couleurs,  transparence, image d'arrière plan). Ces sont autant de petits détails qui rendent l'utilisation de Ghostwriter fort appréciable.

Pour conclure, Ghostwriter répond à mes attentes, parce qu'il situe une bonne interface de rédaction à l'intersection entre l'éditeur de texte et les sorties souhaitées pour la finalisation (LaTeX et HTML, surtout pour ce qui me concerne).

Lien vers le site officiel de Ghostwriter : [wereturtle.github.io/ghostwriter/](https://wereturtle.github.io/ghostwriter/)

![Ghostwriter capture 1](/images/ghostwriter1.png)
![Ghostwriter capture 2](/images/ghostwriter2.png)
![Ghostwriter capture 3](/images/ghostwriter3.png)
