---
title: "Trail du Haut Koenigsbourg 2015"
date: 2015-09-07
author: "Christophe Masutti"
image: "/images/postimages/hikebike.png"
tags: ["Sport", "Trail", "Course", "Parcours"]
description: "Et c'est reparti pour le THK 2015"
categories:
- Sport
---

J'attendais avec une certaine impatience ce second rendez-vous à Kintzheim pour le trail court du Haut Koenigsbourg. L'engouement ressenti lors de la session de l'année dernière allait-il être encore au rendez-vous ? Et surtout, il faut bien reconnaître que, un an plus tard avec des entraînements réguliers, j'espérais bien diminuer significativement le chrono précédent.

## La course

Le nombre de participants n'en fini pas de croître pour ce trail ! De 1664 coureurs en 2014, on est passé à 1900 pour cette session 2015&#46; En voyant les objectifs d'inscrits, j'avais un peu peur que l'équipe d'organisation se laisse dépasser… que nenni ! Ils ont assuré, les bougres. Non seulement l'organisation fut de nouveau exemplaire mais ce n'est plus le seul château du Haut Koenigsbourg qui fut traversé (par le chemin de ronde s'il vous plaît) mais c'est aussi celui de Kintzheim que les coureurs ont pu admirer en fin de parcours.

![Profil et Roadbook 201. Source&nbsp;: trail-hk.com](/images/athk_roadbook_25_km_2015_v1.png)

Dans la fraîcheur matinale et dans les senteurs très caractéristiques à l'approche des vendanges, la ville de Kintzheim avait connu une nuit plutôt agitée, avec les départs des trails de 84&nbsp;km et 55&nbsp;km, respectivement à 2:00 et 7:00 du matin. C'est le trail court (25&nbsp;km) qui, fort de ses 900 participants, contribua sans doute au réveil définitif. Un quart d'heure de retard sur l'horaire prévue s'explique par le succès de l'épreuve. Sur le bitume, les semelles chauffent, on a peur de se refroidir (il fait un peu frais quand même), on se presse, on échange quelques banalités, puis c'est avec un certain soulagement que le départ est finalement donné.

La traversée de la ville n'est pas si rapide, ce qui laisse le temps de se mettre en jambe. On longe la vigne en direction de Châtenois, un point de vue remarquable au soleil levant ferait presque ralentir la cadence pour admirer les belles couleurs d'un paysage typiquement alsacien.

J'aperçois alors Céline, une sympathique coureuse de Kintzheim placée à côté de moi dans la foule du départ et avec qui j'avais échangé quelques mots. Son rythme est régulier, elle connaît le terrain… je me décide à la suivre. Sans le savoir elle me servira de lièvre dans les 5 premiers kilomètres, décisifs sur ce parcours. Si elle passe par ce blog, je lui adresse (de nouveaux) mes chaleureuses salutations ! Nous nous retrouverons au château du Haut Koenigsbourg, kilomètre 16, pour terminer la course presque ensemble.

Sur ce trail, outre les longues montées, il y a deux endroits à appréhender&nbsp;: le premier sentier qui monte au Hahnenberg, où le dépassement est très difficile, et la montée continue aux pieds du Haut Koenigsbourg. J'arrive au premier single avec le net avantage de ne pas être dans le gros du peloton, contrairement à l'année passée. Si bien que la montée se négocie finalement bien avec un rythme agréable. La première descente est très rapide, heureusement, les coureurs sont très espacés et j'en profite pour faire quelques pointes.

Puis tout s'enchaîne sur un rythme plutôt ronflant. Il faut dire – et c'est le principal grief du parcours – que les chemins sont très larges et presque sans aucun obstacle, ce qui instaure une sorte de monotonie. On s'y laisse facilement prendre au risque de ralentir l'allure, ou du moins ne pas adopter celle que l'on aurait dû avoir, dans les limites de ses capacités. Regarder sa vitesse en temps réel sur sa montre est un réflexe qu'il faut parfois savoir prendre. Bref, je n'hésite pas à accélérer un peu en me disant que je pourrai récupérer plus tard, quitte à marcher un peu plus que prévu dans la grande montée.

Ce fut une bonne stratégie, finalement, car c'est lors de cette montée que j'ai du réaliser le meilleur temps par rapport à l'année dernière, tout en modérant l'effort. Je récolte les fruits de mon entraînement estival dans les Alpes. Passé le dernier obstacle de la descente du Haut Koenigsbourg, assez technique par endroits, le reste du parcours s'est déroulé de manière assez rapide. Le grand replat entre les deux montagnes doit se négocier sur la vitesse, sans quoi on y perd vite tous les avantages glanés auparavant.

Le dernier kilomètre, cette année, nous faisait passer au château de Kintzheim. L'intérêt touristique est indéniable, mais l'autre avantage est de nous épargner la longue descente betonnée du parcours précédent. Grâce en soit rendue aux organisateurs&nbsp;: bien que le parcours soit légèrement plus long, c'est bien plus confortable comme cela.

De manière générale, le Trail du Haut koenigsbourg, dans sa version 25&nbsp;km, est un parcours très rapide. Le premier arrivé est à 1:49 et les derniers à presque 4 heures. Pour ma part, je me situe dans la première moitié (2:46), ce qui fait tout de même 20 minutes de moins que ma performance de 2014, avec 1&nbsp;km de plus ! Une différence franchement inattendue, mais qui révèle, outre l'entraînement, toute l'importance de faire les bons choix au fil de la course.

## Résultats


Classement&nbsp;:

- 32<sup>e</sup> dans la catégorie Vétérans 1 (sur 51)
- 398<sup>e</sup> au classement général (sur 839)
- Temps&nbsp;: 2:46:27
- Team&nbsp;: Framasoft

