---
title: "Entraînement trail : Rocher de Mutzig"
date: 2014-11-15
author: "Christophe Masutti"
image: "/images/postimages/hikebike.png"
tags: ["Sport", "Trail", "Entraînement", "Parcours"]
description: "Voici un parcours de trail du côté du rocher de Mutzig, incontournable pour le sportif alsacien."
categories:
- Sport
---


Qui a dit que le mois de novembre ne se prêtait pas aux longues promenades dans notre belle forêt vosgienne&nbsp;? Évidemment, on n'y rencontre pas beaucoup de monde : c'est l'occasion de prendre des sentiers habituellement fréquentés par les randonneurs et mesurer leur potentiel en entraînement trail&nbsp;! Le parcours présenté ici est une découverte de la forêt alsacienne du côté de [Lutzelhouse](http://fr.wikipedia.org/wiki/Lutzelhouse) (vallée de la Bruche) à une trentaine de kilomètres de Strasbourg.


## Description

La plupart des randonnées au départ de Lutzelhouse impliquent de rouler jusqu'aux parkings déjà fort avancés dans la forêt. En effet, les points remarquables (le Jardin des fées, le Rocher de Mutzig, la Porte de Pierre) sont accessibles pour les randonneurs sur des parcours assez longs, que tout le monde n'est pas prêt à assumer, en particulier en famille. En trail, cependant, la question des distances est toute différente. Nous partons donc depuis le village de Lutzelhouse.

Allez garer votre véhicule sur le parking de l'école (et du cimetière) de Lutzelhouse&nbsp;: vous y trouverez une fontaine qui sera fort utile à votre retour, pour nettoyer vos chaussures… Le départ et l'arrivée se situeront ici.

L'essentiel du parcours se concentre sur les deux montagnes dominant le village&nbsp;: le Langenberg et le Katzenberg. Voici les étapes et les points remarquables (voir les photos en fin de billet) :


- Les Deux Chênes
- Kappelbronn (en passant par le séquoïa)
- L'Enceinte du jardin des fées (ancien fort celtique)
- Col du Narion
- Rocher de Mutzig
- La Porte de Pierre
- Col du Wildberg
- Schmeerbuchs
- Waltersbach (village disparu)
- La Grotte du Loup
- Les deux Chênes (retour)


Il s'agit d'un massif très fréquenté par la section alsacienne du Club Vosgien. Le travail remarquable du Club Vosgien dans le balisage et l'entretien des parcours de randonnées trouve ici sa pleine mesure. Une pléthore d'indicateurs, directement visibles sur une carte IGN Top25, donnent une idée des solutions possibles pour atteindre ses objectifs. En trail, se repérer sur les balises du Club Vosgien est assez facile. Sur ce parcours, il faudra toutefois porter son attention aux différents changements de tracé et ne pas suivre inconsidérément les mêmes balises.

## Description du parcours

![Balises du Club Vosgien](/images/cheminbalisecv.png)

Le parcours fait environ **21 km** pour **850m D+**. Il est de niveau technique moyen-supérieur pour une durée moyenne de 2h45 (moins ou beaucoup moins selon le niveau, bien entendu). L'idée générale fut d'éviter les grands chemins (routes forestières quasi inévitables à cet endroit) et préférer les « single ». Plusieurs variantes sont possibles, selon votre humeur et votre forme du moment.

Le départ s'effectue à alt. 273 m. Un échauffement sérieux est recquis au départ de manière à affronter la première montée assez raide. Après le 1<sup>er</sup> kilomètre, la montée s'adoucit de manière durable jusqu'au Kappelbronn (504 m alt.). Le principal passage technique vous attend au lieu bien-nommé « La Grande Côte », et vous mènera jusqu'au Jardin des Fées. Ensuite, jusqu'au Rocher de Mutzig (alt. 1010 m), la montée est continuelle et se termine par un raidillon. La descente est ensuite amorcée, d'abord sur sentiers (dont les bords sont parfois instables, donc prudence) puis sur chemin large en fin de parcours.

Dans l'ordre, voici les signes suivis (nomenclature du Club Vosgien) :

(Le tracé du parcours est [par ici](http://umap.openstreetmap.fr/fr/map/parcours-trail-rocher-de-mutzig_21796))

- Triangle rouge + Rectangle rouge barré de blanc
- Anneau rouge + Rectangle rouge barré de blanc
- Anneau vert
- Anneau jaune
- Rectangle rouge + anneau rouge
- Coix droite jaune
- Triangle rouge + rectangle jaune
- Triangle rouge
- Triangle rouge + Rectangle rouge barré de blanc

![Le profil altimétrique](profilaltimrochermutzig.png)

![Le jardin des fées](/images/aajardindesfees.png)
![La porte de pierre](/images/portesdepierre.png)
![Le Rocher de Mutzig](/images/rocherdemutzig.png)


