---
title: "Questions de sécurité démocratique"
date: 2014-07-17
author: "Christophe Masutti"
image: "/images/postimages/ai.png"
tags: ["Libres propos", "Informatique", "Sciences", "Démocratie", "Capitalisme de surveillance"]
description: "À propos des discours sécuritaires et de notre rapport aux technologies de communication. Une analyse brillante de Liora Lazarus, de l'Université d'Oxford, mérite d'être mentionnée"
categories:
- Libres propos
---

Le jour même de la [série d'assassinats terroristes](http://fr.wikipedia.org/wiki/Attentats_de_janvier_2015_en_France) à Charlie Hebdo et Porte de Vincennes en ce mois de janvier 2015, plusieurs politiciens ont proposé, au nom de la sécurité collective, de revoir l'équilibre entre les libertés individuelles et la sécurité. Sur ce blog, j'ai eu l'occasion, le lendemain des faits, de signaler le paradoxe qu'il y avait à former, au nom de la liberté d'expression, une union politique autour des questions de sécurité collective dans une guerre contre le terrorisme. Dès lors il me semble important de se pencher sur les discours politiques qui ont et qui vont scander la vie publique cette année 2015.

Dans tous les cas où la sécurité publique est abordée de manière politique, le choix est imposé entre une définition de la démocratie et l'équilibre entre les droits individuels et la sécurité. Or, de tous les concepts, celui de sécurité est au mieux défini de manière floue (ce qui n'empêche pas la bienveillance, lorsque par exemple la santé ou la défense des salariés sont reconnues comme des formes de sécurité) et au pire complètement dévoyé pour servir des intérêts divergents. 

Les études de cas ne manquent pas autour de la notion de *sécurité démocratique*, telle cette étude de C. Da Agra intitulée «&nbsp;[De la sécurité démocratique à la démocratie de sécurité&nbsp;: le cas Portugais](http://www.cairn.info/revue-deviance-et-societe-2001-4-page-499.htm)&nbsp;» (dans *Déviance et Société*, 25, 2001), ou encore cette étude sur le [cas colombien](http://www.cairn.info/resume.php?ID_ARTICLE=PAL_083_0081). L'Unesco défend l'idée que la question de la sécurité démocratique devrait être conçue «&nbsp;comme une matrice au sein de laquelle les questions relevant de la sécurité pourraient être abordées de façon permanente par l'ensemble des acteurs de la société&nbsp;» (voir [ici](http://www.unesco.org/cpp/fr/paix/chantier.htm)), de manière à éviter d'être accaparée par quelques uns et les conflits d'intérêts. Telle est la problématique soulevée par [cet article](http://www.alterinfos.org/spip.php?article1215) sur le cas colombien, justement.

L'annonce d'une [conférence](http://www.usias.fr/actualites-agenda/agenda/evenement/?tx_ttnews%5Btt_news%5D=9948&amp;cHash=162063dbafba98b5b788744d477c0bb2) qui se tiendra prochainement à l'Université de Strasbourg (voir [le programme](http://www.usias.fr/evenements/usias-symposium-democratic-security/)) m'a fait connaître une partie des travaux de Liora Lazarus, professeure de droit à l'Université d'Oxford. Cette dernière travaille depuis longtemps sur les questions relatives au droit et à la sécurité. [Un texte de sa part](http://papers.ssrn.com/sol3/papers.cfm?abstract_id=2207936), rédigé en 2010 et publié en 2012[^1], intitulé «&nbsp;[The Right to Security – Securing Rights or Securitising Rights](http://papers.ssrn.com/sol3/papers.cfm?abstract_id=2207936)&nbsp;» (Le droit à la sécurité – sécuriser les droits ou sécuritariser les droits) m'a semblé tout particulièrement visionnaire, lorsqu'on le lit à la lumière des récents événements terroristes sur le sol français.

Je ne résiste pas à vous livrer ici la traduction personnelle (et donc perfectible) d'une section de son article, intitulée *Rhetorical expressions of the right to security* (les expressions rhétoriques du droit à la sécurité).

> **Rhetorical expressions of the right to security**
> 
> Les lecteurs ne seront pas surpris d'apprendre que le droit à la sécurité est souvent mentionné dans les discussions relatives à la «&nbsp;guerre contre le terrorisme&nbsp;». On recourt au droit pour justifier les mesures coercitives anti-terroristes, tant à l'intérieur qu'à l'extérieur des États-Unis, ou même pour justifier l'invasion militaire ou les réponses en Afghanistan[^2], au Kosovo[^3], au Pakistan[^4], et dans le cas de la Colombie, en Équateur[^5]. Il y a beaucoup d'exemples, mais deux d'entre eux démontrent particulièrement bien ce point. Lors de l'exposé de la politique anti-terroriste de l'Union Européenne, Franco Frattini déclara&nbsp;: «&nbsp;Notre objectif politique reste à trouver le juste équilibre entre le droit fondamental à la sécurité des citoyens, qui est en premier lieu le droit à la vie, et les autres droits des individus, y compris le droit à la vie privée et les droits procéduraux&nbsp;»[^6]. De même au Royaume-Uni, à propos de politique anti-terroriste en 2006, John Ried, ancien ministre de l'Intérieur, déclara&nbsp;: «&nbsp;Dans la mesure où nous sommes confrontés à une menace d'assassinat de masse, nous devons accepter que les droits individuels dont nous jouissons devront s'équilibrer avec le droit collectif à la sécurité, à la protection de la vie et de l'intégrité physique que nos citoyens exigent.&nbsp;»[^7]
> 
> Les hommes politiques ne sont pas les seuls à tenir ce langage. Les citoyens aussi articulent le droit à la sécurité par rapport à la «&nbsp;guerre contre la terreur&nbsp;». Comme l'écrit Emily Cochrane dans le *Carstairs Courier* en Alberta&nbsp;:
> 
> «&nbsp;Le Canadian Charter of Rights and Freedoms…, déclare le droit à la sécurité des personnes. Lorsque des membres d'une organisation terroriste Taliban firent s'écraser des avions dans une attaque stratégique contre l'Amérique le 9/11, en tuant 2,973 personnes, ceux qui ont soutenu ces actions et ceux qui les ont abrité, ont perdu leur droit à la sécurité tout comme ils ont causé la perte de la vie de tant d'autres personnes. Le peuple des États-Unis a un droit à la sécurité – de vivre sans peur, et le seul moyen d'y parvenir était de neutraliser la menace à la source.&nbsp;»[^8]
> 
> Ces références nous apprennent deux choses importantes&nbsp;: premièrement que le droit à la sécurité est mentionné pour renforcer la rhétorique politique dans une situation de conflit militaire et de guerre contre le terrorisme, d'une manière subtile et importante. Ce processus de légitimation (et peut-être de désinfection) par référence au discours sur les droits est ce que nous pourrions appeler une «&nbsp;conformation de la sécurité au droit&nbsp;» (*righting security*). Le cadrage du droit sur la sécurité permet aux politiques de faire passer leurs actions coercitives comme le corrélat nécessaire d'un droit. En d'autres termes, la recherche de la sécurité n'est pas seulement un choix politique en vertu d'un bien public, c'est l'accomplissement d'un devoir imposé à l'État par le droit fondamental de chaque individu à la sécurité.
> 
> De manière toute aussi cruciale, présenter de telles actions de l'État comme ayant été motivées par notre droit fondamental est au cœur de la rhétorique du «&nbsp;rééquilibrage&nbsp;» entre la sécurité et les droits de l'homme. Cette langue du rééquilibrage oppose de manière générale le droit à la sécurité de la majorité aux droits des minorités qui pourraient être violés.
> 
> L'«&nbsp;altérité&nbsp;» intrinsèque dans ce rééquilibrage rhétorique est bien illustré par l'ancien Procureur général Lord Goldsmith, qui a fait valoir qu'il est difficile de trouver un «&nbsp;calcul utilitaire simple pour trouver l'équilibre entre le droit à la sécurité du plus grand nombre et les droits de quelques uns&nbsp;»[^9]. Néanmoins, les politiques sont en désaccord quant à l'importance du droit à la sécurité, et donc savoir où situer l'équilibre entre la sécurité et les droits de la défense qui lui sont rivaux. Alors que John Reid croit que «&nbsp;le droit à la sécurité, à la protection de la vie et de la liberté, est et doit être le droit fondamental sur lequel tous les autres doivent s'appuyer&nbsp;»[^10], Sir Menzies Campbell[^11] note que tandis que le public «&nbsp;a un droit à la sécurité&nbsp;», il «&nbsp;a aussi un droit à la sécurité contre la puissance de l'État&nbsp;»[^12]. Pourtant, une telle définition de la sécurité comme un droit de la défense contre l'intervention d'État est, dans la rhétorique politique, moins couramment utilisée que la dimension positive du droit issue des devoirs coercitifs de l'État.
> 
> Ces idées divergentes à propos du droit à la sécurité, et son poids dans la balance entre sécurité et liberté, joue directement sur la manière dont les gouvernements renforcent les pouvoirs de police et évaluent l'activité militaire devant une menace sécuritaire. Il y a très peu de clarté ou de conseil sur la manière d'équilibrer le droit à la sécurité là où il est invoqué pour légitimer la force de l'État aussi bien dans le contexte national qu'international. Cette situation est problématique parce que la portée du droit à la sécurité, son poids par rapport à d'autres droits, ses limites admissibles, et les devoirs corrélatifs qu'il impose à l'État, sont toutes des questions auxquelles il faut répondre avant de savoir comment les «&nbsp;équilibres&nbsp;» pourraient être atteints.




[^1]: Voir Liora Lazarus, «&nbsp;The Right to Security – Securing Rights or Securitising Rights&nbsp;», dans&nbsp;: Rob Dickinson et al., *Examining Critical Perspectives On Human Rights*, Cambridge&nbsp;: Cambridge Univ. Press, 2012, p. 87-106.

[^2]: E. Cochrane, *Troops deserve our support*, Carstairs Courier (Alberta), 6 November 2007.

[^3]: *The Vancouver Sun*, &quot;Yeltsin's final fling: The Russian leader, often portrayed in the West as a boorish drunk, had substance that belied his unvarnished style&quot;, 27 January 2001: &quot;The Kosovo conflict demonstrated the worst political tendencies and double standards of modern Europe. It was claimed, for example, that human rights were more important than the rights of a single state. But when you violate the rights of a state, you automatically and egregiously violate the rights of its citizens, including their rights to security&quot;.

[^4]: *The Press Trust of India*, &quot;Pak should give firm assurance against abetting terrorism&quot;, 30 December 2001: &quot;Stating that terrorism had crossed the lakshman rekha (the limit of patience) with the December 13 attack on Indian Parliament, Advani said, *no sovereign nation which is conscious of its right to security can sit silent. It has to think as to what steps need to be taken to check this menace*.&quot;&quot; (Quoting India's Federal Home Minister L. Advani in a programme on national broadcaster, Doordarshan).

[^5]: *BBC Worldwide Monitoring*, &quot;Colombia defends its incursion into Ecuador&quot; 23 March 2008: Communique issued by the Presidency of the Republic in Bogota on 22 March. &quot;The Colombian Government hereby expresses:. 1/ Its full observance of the decisions adopted by the OAS. 2/ Reminds the world that the camp of alias *Raul Reyes* was a site of terrorists who acted.

[^6]: European Commissioner responsible for Justice, Freedom and Security &quot;EU counter-terrorism strategy&quot; *European Parliament*, 5 September 2007, Speech/07/505.

[^7]: J. Reid, &quot;Rights, security must be balanced&quot;, *Associated Press Online*, 16 August 2006.

[^8]: Cochrane, *Troops deserve our support*.

[^9]: Full text of speech reported by BBC News "Lord Goldsmith's speech in Full" 25 June 2004, available at [news.bbc.co.uk](http://news.bbc.co.uk/2/hi/uk_news/politics/3839153.stm) (accessed 14 September 2010).

[^10]: 10. Full text of speech reported by BBC News "Reid urges human rights shake-up", 12 May 2007, available at [news.bbc.co.uk](http://news.bbc.co.uk/2/hi/uk_news/politics/6648849.stm) (accessed 14 September 2010).

[^11]: (NdT.:) député au parlement du Royaume-Uni, voir [notice Wikipedia](http://en.wikipedia.org/wiki/Menzies_Campbell).

[^12]: Speech made in the House of Commons debates into extending the limits of pre-charge detention, 25 July 2007, HC Deb., vol. 463, col. 851. Ironically, this framing of security as a defensive right against state action was part of the rationale behind the Second Amendment of the US Constitution which allowed for an armed citizenry to defend against abuses by undemocratic government (L. Emery, &quot;The Constitutional right to keep and bear arms&quot; 28(5) *Harvard Law Review* (1915) 473, 476).

