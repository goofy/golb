
---
title: "Sortie trail au Taennchel"
date: 2015-11-27
author: "Christophe Masutti"
image: "/images/postimages/hikebike.png"
tags: ["Sport", "Trail", "Entraînement", "Parcours"]
description: "Une sortie hivernale dans un massif vosgien méconnu"
categories:
- Sport
---


C'est déjà vendredi et demain c'est reparti pour un autre tour de trail dans la brume fraîche et humide des Vosges alsaciennes. Il me fallait néanmoins témoigner de ce parcours au Taennchel effectué récemment car il présente de nombreux avantages techniques.


Au dessus de Ribeauvillé, surveillé par ses trois châteaux, on voit poindre le massif du Taennchel, un endroit magnifique avec des rochers aux étranges formes : Rocher des Titans, Rocher des Géants, Rocher des Reptiles... Si l'on sait se laisser bercer par l'atmosphère des lieux, on ne revient pas effrontément d'une visite au Taennchel. La particularité géomorphologique du massif est de présenter une longue crête d'environ 6 kilomètres de la forme d'un «&nbsp;U&nbsp;» entourant le hameau de la Grande Verrerie. Sous la partie Est du massif, on peut aussi admirer le village de Thannenkirsch et, au delà et en face, le château du Haut Koenigsbourg. Quelle que soit la face par laquelle vous abordez ce massif, il faudra compter sur un dénivelé important en peu de kilomètres, ce qui permet d'avoir quelques perspectives intéressantes en matière de technique de montée et de descente sur des sentiers aux morphologies très différentes. Ce massif est d'ailleurs le terrain de jeu de l'[Association Sportive Ribeauvillé Athléroute](http://www.asraribeauville.com) qui organise annuellement les Courses du Taennchel, dont un trail court.

Avec les collègues de l'[ASCPA](http://www.ascpa.eu/), la sortie du samedi matin nous a conduit en cette contrée. En partant de Ribeauvillé (on peut se garer au niveau du lyçée au nord de la ville), le parcours fait environ 20&nbsp;km. Il commence et se termine par la montée / descente vers les châteaux S<sup>t</sup>&nbsp;Ulrich et Ribeaupierre (310&nbsp;m&nbsp;D+). Lors du retour, cette descente est très rocheuse par endroits, presque alpine. La plus grande prudence est de mise surtout avec la fatigue d'une fin de parcours et en particulier sur sol humide. Cependant, c'est un excellent entraînement à renouveler par exemple plusieurs fois sur une boucle plus courte.

Après le sentier des châteaux et un replat aux pieds du Taennchel sur chemin large, c'est vers la Grande Verrerie que l'on se dirige par un sentier en descente régulière et pierreux par endroits, pour remonter en face vers la crête du Taennchel en passant par le Schelmenkopf (380&nbsp;m&nbsp;D+). C'est le second «&nbsp;coup de cul&nbsp;» à sérieux à supporter sur ce parcours&nbsp;: tout le reste de la crête du Taennchel alterne entre faux plat montant et descendant, ce qui laisse largement le temps d'admirer les lieux (lorsque la météo s'y prête). Une fois dépassé le Rocher des Géants, une longue descente sur sentier régulier s'amorçe jusqu'à l'Abri du Taennchel. Rien de spécialement technique mais attention toutefois aux animaux sauvages : un chevreuil a carrément faille me renverser en passant à 1&nbsp;m de moi (tous deux lancés en pleine course)&nbsp;! Magnifique souvenir mais très impressionnant sur le moment.

Les photographies çi-dessous montreront qu'en choisissant ce parcours en plein mois de novembre, ce n'est pas forcément la meilleure saison pour faire un trail touristique. Disons que nous ne nous sommes pas trop attardés à des endroits qui nous auraient incités à ralentir le rythme avec une météo plus clémente.


Comme d'habitude, ci-dessous, voici la trace du parcours&nbsp;:

<iframe width="100%" height="500px" frameBorder="0" src="https://umap.openstreetmap.fr/fr/map/sortie-trail-au-taennchel_64000?scaleControl=false&miniMap=false&scrollWheelZoom=false&zoomControl=true&allowEdit=false&moreControl=true&datalayersControl=true&onLoadPanel=undefined&captionBar=false"></iframe>

<a href="https://umap.openstreetmap.fr/fr/map/sortie-trail-au-taennchel_64000">Voir en plein écran</a>


![Taennchel 1](/images/taennchel3.jpg)
![Taennchel 2](/images/taennchel5.jpg)
![Taennchel 3](/images/taennchel6.jpg)



