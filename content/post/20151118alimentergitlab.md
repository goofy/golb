---
title: "Alimenter mon dépôt Gitlab"
date: 2015-11-18
author: "Christophe Masutti"
image: "/images/postimages/terminal.png"
description: "Petit tutoriel rapide pour initier et alimenter un dépôt Gitlab."
tags: ["Logiciel libre", "Git", "Informatique", "Bidouille"]
categories:
- Logiciel libre
---


Framasoft a ouvert un service Gitlab très facile d’utilisation. Pour qui ne serait pas encore à l'aise avec Git, voici un petit tutoriel rapide pour alimenter son propre dépôt.


## Créer un nouveau dépôt

Un fois son compte créé et configuré (tout se fait en ligne), on peut créer un nouveau dépôt. Ce dernier doit être alimenté en premier par un fichier ``readme.md`` c'est à dire un fichier écrit en Markdown. Il est censé décrire le projet, et communiquer régulièrement les principaux changements.

On peut préparer ce fichier ``readme.md`` localement en prévision des premières manipulations (ci-dessous).

### Générer sa clé SSH

Toutes les connexions sur votre compte Gitlab se feront de préférence de manière chiffrée et authentifiée avec une clé SSH. L'idée est simple : il faut générer une clé SSH, la stocker localement et la recopier dans les paramètres SSH de son compte Gitlab. On peut ajouter autant de clé SSH que l'on souhaite.

### Pour générer et stocker une clé SSH

Entrer la commande suivante :

```
ssh-keygen -t rsa -C "xxxxxxxx"
```

(remplacer xxxxxxxx par son adresse courriel renseignée dans Gitlab)

La création d'un mot de passe n'est pas obligatoire, mais elle est préférable en tant que sécurité supplémentaire.

Une fois la clé créée, il faut la placer dans son dossier ``~\.ssh`` et la recopier sur son compte Gitlab.


## Installer et configurer Git

Après avoir installé Git (``sudo apt-get install git``), il faut le configurer comme suit :

```
git config --global user.name "nomutilisateur"
git config --global user.email "adressecourriel"
```

## Premier dépôt

Une fois le dépôt créé via l'interface de Gitlab, on peut travailler localement.

Commencer par cloner le dépôt en local :

```
git clone git@git.framasoft.org:nomutilisateur/nomdudepot.git
```

Se rendre dans le dossier du dépôt :

```
cd nomdudepot
```

Coller dans ce dossier le readme préparé auparavant (cf. ci-dessus).

Puis ajouter ce fichier dans la file des fichiers que nous allons remonter dans le dépôt :

```
git add README.md
```

Annoncer un commit en écrivant un message explicatif :

```
git commit -m "ajout du fichier readme"
```

Pousser le tout sur le dépôt :

```
git push -u origin master
```

Précéder de la même manière pour tous les autres fichiers. Si l'on souhaite ajouter d'un coup plusieurs fichiers on peut écrire ``git add fichier1 fichier2 fichier3`` etc.

Évidemment il ne s'agit ici que de remonter dans la file principale du projet.

## Problème en https

Si l'on veut pousser des gros fichiers en https, on peut tomber sur une erreur de type

```
error: RPC failed; result=22, HTTP code = 411
fatal: The remote end hung up unexpectedly
```

C'est en fait parce que la configuration par défaut de Git en http limite à 1Mo la taille maximale des fichiers qu'on peut pousser. Il faut donc configurer Git pour accepter une taille plus importante en entrant une commande idoine&nbsp;:

```
git config http.postBuffer nombredebytes
```

où nombredebytes est la taille maximale des fichiers qu'on veut envoyer.

On peut configurer Git de manière globale, ainsi&nbsp;:

```
git config --global http.postBuffer 524288000
```
