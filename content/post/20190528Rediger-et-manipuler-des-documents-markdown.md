﻿---
title: "Rédiger et manipuler des documents avec Markdown"
date: 2019-05-28
author: "Christophe Masutti"
image: "/images/postimages/renaissance.jpg"
description: "Que voulez-vous faire avec votre document ? À qui le destinez-vous ? Pour quels usages ? Après avoir énoncé quelques règles relatives à la stratégie nous verrons comment organiser un travail avec Markdown vers ODT ou LaTeX."
draft: false
tags: ["Markdown", "bidouille", "pandoc", "éditeur", "traitement de texte"]
categories:
- Logiciel libre
---



Rédiger des documents et élaborer un flux de production implique d'envisager clairement le résultat final. Formulé ainsi, cela pourra toujours sembler évident&nbsp;: que voulez-vous faire avec votre document&nbsp;? À qui le destinez-vous&nbsp;? Pour quels usages&nbsp;? Après avoir livré quelques considérations générales relatives à la stratégie j'expliquerai comment j'envisage la rédaction de documents (souvent longs) avec Markdown vers ODT ou LaTeX.

<!--more--> 



# Quelques considérations préalables

Rédiger un document avec LibreOffice, MSWord, LaTeX (ou d'autres logiciels de traitement de texte) c'est  circonscrire le flux de production aux capacités de ces logiciels. Attention, c'est déjà beaucoup&nbsp;! Les sorties offrent bien souvent un large choix de formats selon les besoins. Cela dit, une question peut se poser&nbsp;: ne serait-il pas plus logique de séparer radicalement le contenu du contenant&nbsp;? 

Après tout, lorsqu'on rédige un document avec un logiciel de traitement de texte, chercher à produire un format compatible consiste à produire un format qui sera non seulement lisible par d'autres logiciel et sur d'autres systèmes, mais aussi que sa mise en page soit la même ou quasi-similaire. Finalement, on veut deux choses&nbsp;: communiquer un format *et* une mise en page.

Voici quelques années que je fréquente différents logiciels dans le cadre de la rédaction et de l'édition de documents, à commencer par LibreOffice et LaTeX. Compte-tenu des pratiques actuelles de stockage dans les nuages et la sempiternelle question du partage de documents et de la collaboration en ligne, j'en suis arrivé à une conclusion&nbsp;: écrire, manipuler, partager, modifier un document déjà mis en forme par un logiciel de traitement de texte est une erreur. Les formats utilisés ne sont que très rarement faits pour cela (il y a bien sûr des exceptions, comme le ``XML``, par exemple, mais nous parlons ici des pratiques les plus courantes). 

Tous les formats propriétaires sont à bannir&nbsp;: leur manipulation est bien trop souvent source d'erreur, et suppose un enfermement non seulement de la chaîne de production mais aussi des utilisateurs dans un écosystème logiciel exclusif. Sitôt qu'un écart minime se forme dans cette chaîne, c'est fichu.

Les formats ouverts sont bien plus appropriés. Justement parce qu'ils sont ouverts, leur compatibilité pose d'autant moins de souci. Il subsiste néanmoins toujours des risques lorsque des logiciels propriétaire entrent dans la chaîne de production parce que tous les utilisateurs n'ont pas les mêmes outils.

La faiblesse ne concerne pas uniquement ces formats ou ces logiciels mais elle réside aussi dans leur manipulation. Même si on accepte de travailler (au prix de nos libertés et de notre confidentialité) avec des services comme ceux de Google ou Microsoft, et même si on utilise des solutions libres et ouvertes comme OnlyOffice ou LibreOffice Online, l'enjeu auquel nous faisons toujours face est le suivant&nbsp;: en ligne ou en local, choisir un logiciel de traitement de texte revient à restreindre d'emblée l'usage futur de la production en choisissant par avance son format final et sa mise en page.

Faire ce choix n'est pas interdit (et heureusement&nbsp;!). C'est d'ailleurs celui qui prévaut dans la plupart des situations. Il faut seulement l'assumer jusqu'au bout et manœuvrer de manière assez efficace pour ne pas se retrouver «&nbsp;coincé&nbsp;» par le format et la mise en page choisis. Là aussi, il faut réfléchir à une bonne stratégie de production et conformer toute la chaîne au choix de départ. 

D'autres solutions peuvent être néanmoins adoptées. Au même titre que la règle «&nbsp;faites votre mise en page en dernier&nbsp;» doit prévaloir pour des logiciels *wysiwyg*, je pense qu'il faudrait ajouter un autre adage&nbsp;: «&nbsp;choisissez le format le plus basique en premier&nbsp;». Cela permet en effet de produire du contenu très en amont de la phase finale de production et inclure dans cette dernière la production du format à la demande.

# Quels choix ?

En d'autre termes, si votre éditeur, votre directeur de thèse, vos collègues ou vos amis, vous demandent un ``.odt``, ``.doc``, un ``PDF``, un ``.epub``, un ``.tex``ou un .``html``, l'idéal serait que vous puissiez le produire à la demande et avec une mise en page (ou une mise en forme s'il s'agit d'un document ``.tex``, par exemple) différente et néanmoins appropriée. Pour cela, il faut impérativement&nbsp;:

- connaître une méthode sûre pour convertir votre document,
- connaître les possibilités de mise en forme automatiques à configurer d'avance pour produire le document final.

Autrement dit, avant même de commencer à produire votre contenu (que vous allez détacher absolument de la forme finale), vous devez élaborer tout un ensemble de solutions de conversion et de mise en forme automatiques.

Rien de bien nouveau là-dedans. C'est exactement la même stratégie que l'on emprunte lorsqu'on rédige un contenu le formatant à l'aide d'un langage balisé comme le HTML ou (La)TeX. On ne fait alors que produire un document texte formaté et, parallèlement, on crée des modèles de mise en page (``.css`` pour le HTML, ou des classes ``.cls`` pour LaTeX). La sortie finale emprunte alors les instructions et les modèles pour afficher un résultat.

L'idée est d'adopter cette démarche pour presque n'importe quel format. C'est-à-dire&nbsp;:

- créer du texte à l'aide d'un éditeur de texte et choisir un langage de balisage facile à l'utilisation et le plus léger possible&nbsp;: le [Markdown](https://fr.wikipedia.org/wiki/Markdown) est à ce jour le meilleur candidat&nbsp;;
- convertir le document en fonction de la demande et pour cela il faut un convertisseur puissant et multi-formats&nbsp;: [Pandoc](https://pandoc.org/)&nbsp;;
- créer des modèles de mise en page (pour les réutiliser régulièrement si possible).


# Avantages

Les avantages de Markdown sont à la fois techniques et logistiques.

D'un point de vue technique, c'est sur la syntaxe que tout se joue. Comme il s'agit d'un balisage léger, le temps d'apprentissage de la syntaxe est très rapide. Mais aussi, alors qu'on pourrait considérer comme une faiblesse le fait que Markdown ne soit pas standardisé, il reste néanmoins possible d'adapter cette syntaxe en lui donnant un saveur particulière. C'est le cas de Multimarkdown, une variante de Markdown permettant des exports plus spécialisés. Ainsi, plusieurs fonctionnalités sont ouvertes lorsqu'on destine un document Markdown à  la conversion avec Pandoc. On retient surtout les tables, les notes de bas de page et la gestion bibliographique ou d'index. Enfin, il est toujours possible d'inclure du code HTML ou même LaTeX dans le document source (parfois même les deux, en prévision d'une conversion multiple).

Pour ce qui concerne le travail Markdown + Pandoc + LaTeX, on peut se reporter à l'article que j'avais traduit sur ce blog&nbsp;: «&nbsp;[un aperçu de Pandoc](https://golb.statium.link/post/20150724aproposdepandoc/)&nbsp;». 

Pour ce qui concerne la pérennité d'un fichier markdown, tout comme n'importe quel fichier texte, il sera toujours lisible et modifiable par un éditeur de texte, ce qui n'est pas le cas des formats plus complexes (exceptés les formats ouverts).

D'un point de vue logistique, la manipulation a plusieurs avantages, bien qu'ils ne soient pas réservés au seul format Markdown&nbsp;:

- les éditeurs de texte sont des logiciels très légers par rapport aux «&nbsp;gros&nbsp;» logiciels de traitement de texte, si bien que travailler sur un document Markdown ne nécessite que peu de ressources (et il y a un grand choix d'éditeurs de texte libres),
- on peut aisément travailler à plusieurs sur un document texte en utilisant des services en ligne légers comme des pads (par exemple sur [framapad.org](https://framapad.org) ou [cryptpad](https://cryptpad.fr)),
- ce qui n'empêche pas l'utilisation de logiciels en ligne plus classiques, pourvu que l'export puisse se faire en texte ou dans un format qui permet la conversion «&nbsp;inverse&nbsp;» vers le Markdown (ou alors de simples copier/coller),
- on peut utiliser facilement un dépôt Git pour synchroniser ses fichiers et retrouver son travail où que l'on soit (ou collaborer avec d'autres utilisateurs),
- le fait de séparer le contenu de la forme permet de se concentrer sur le contenu sans être distrait par les questions de mise en page ou une interface trop chargée (c'est aussi un argument pour LateX, sauf que la syntaxe plus complexe contraint en fait le rédacteur à organiser son texte selon les critères «&nbsp;logiques&nbsp;» de la syntaxe, ce qui est aussi respectable).

# Mon système à moi

J'en avais déjà parlé dans un autre billet sur [Markdown, Pandoc HTML et LaTeX](https://golb.statium.link/post/20150630partiecareemkdownpandoc/), aussi il y aura des répétitions...

J'utilise LibreOffice ou LaTeX ou la HTML en seconde instance. Dans la mesure du possible j'essaie de rédiger d'abord en Markdown, puis de convertir. 

Je procède ainsi dans les cas de figure suivants (liste non exhaustive)&nbsp;:

- la rédaction d'un billet de blog (inutile de convertir car mon moteur de blog utilise le Markdown et converti tout seul en HTML),
- la rédaction de notes (pour une réunion ou un texte court) que je converti en PDF en vue d'une diffusion immédiate,
- la rédaction d'un document long que je produirai en PDF via LaTeX,
- la rédaction d'un document long que je produirai en HTML et/ou epub,
- la rédaction d'un document long que convertirai en ODT dans le cadre d'un projet éditorial,
- la rédaction d'un article que je destine à une diffusion en HTML et/ou epub.

Pour convertir avec Pandoc, la moulinette de base est la suivante (ici pour du ``.odt``)&nbsp;:

<pre>
pandoc fichier-de-depart.md -o fichier-de-sortie.html
</pre>

Ensuite tout est une question d'ajout d'options / fonctions dans la ligne de commande.


Les difficultés sont les suivantes (je ne reviens pas sur le mode d'emploi de Pandoc, l'usage des templates, etc.).



## Les styles

La gestion des styles dans la conversion en ``.odt`` dans LibreOffice n'est pas évidente. Le mieux est encore d'utiliser un modèle qui prenne en compte les styles que Pandoc produit lors de la conversion. En effet, la grande force de LibreOffice, ce sont les styles (voir [cet ouvrage](https://framabook.org/libreoffice-cest-style/)). Dès lors il est important que vous puissiez opérer dessus une fois votre document au format ODT. 

Pour faire simple, il n'est pas utile de spécifier un modèle lors de la conversion avec Pandoc. Il faut la plupart du temps se contenter de produire le document en ``.odt`` puis appliquer les styles voulus. Pour aller vite, voici ce que je conseille de faire&nbsp;:

- rédigez une fois un document Markdown avec toutes les marques de formatage que vous voulez,
- convertissez-le en ``.odt``,
- ouvrez le ``.odt`` avec LibreOffice et modifiez les styles que Pandoc a créé,
- sauvegardez ce document,
- puis à chaque fois que vous devrez produire un document similaire, effectuez un simple transfert de style en suivant la méthode expliquée pages&nbsp;64-66 de [cet ouvrage](https://framabook.org/libreoffice-cest-style/).

Pour gérer les styles en vue d'une production de document HTML/epub ou LaTeX/PDF, le mieux est encore de créer ses classes ``.css`` et/ou ``.cls`` et/ou ``.sty`` et d'y faire appel au moment de la conversion. On peut aussi intégrer l'appel à ces fichiers directement dans le template, mais faut dans ce cas donner leur nom, ce qui n'est pas pratique si l'on souhaite avoir plusieurs styles de mise en page à disposition et changer à la volée.

Par exemple, pour une sortie HTML&nbsp;:

<pre>
pandoc --template=modele.html -H monstyle.css fichier-de-depart.md -o fichier-de-sortie.html
</pre>

(notez que l'appel au fichier ``.css`` peut très bien être déjà inclus dans le modèle)


## La bibliographie

J'ai indiqué dans [cet article](https://golb.statium.link/post/20150630partiecareemkdownpandoc/) comment utiliser Pandoc avec une bibliographie. L'essentiel est de comprendre que, quel que soit le format de sortie choisi, il faut que votre bibliographie soit enregistrée dans *le* format de bibliographie par excellence, *bibtex* (``.bib``).  Puis&nbsp;:

- si vous voulez sortir un fichier pour LaTeX, la bibliographie se gérera avec Biblatex (on ajoutera l'option ``--biblatex`` dans la ligne de commande),
- si vous voulez sortir des fichiers HTML, ou ODT, avec une mise en forme de la bibliographie, il faut aussi installer [Citeproc](https://en.wikipedia.org/wiki/CiteProc), un jeu de programmes auxquels Pandoc fera appel pour appliquer des instructions de configuration en CSL [Citation Style Language](https://citationstyles.org/).

Concernant les styles de bibliographie obtenus en CSL, le mieux est encore d'utiliser le logiciel [Zotero](https://www.zotero.org/). Il peut vous servir à créer votre bibliographie (il est très pratique grâce à son extension pour Firefox) ou simplement ouvrir votre ``.bib`` obtenu par un autre logiciel, peu importe. Comme Zotero permet d'exporter des éléments bibliographiques formatés, il permet aussi de jouer avec les styles CSL disponibles sur [ce dépôt](https://www.zotero.org/styles). Si vous trouvez votre bonheur, conservez le fichier ``.csl`` de votre style ou bien utilisez l'[éditeur en ligne](https://editor.citationstyles.org/about/) pour en créer un de toutes pièces ou modifier un modèle existant.

Notez que la conversion vers le format ``.odt`` avec une bibliographie formatée avec un modèle ``.csl`` ne vous produira pas une bibliographie comme si vous l'aviez obtenu avec l'extension Zotero dans LibreOffice et des références dynamiques. Au contraire, selon la mise en forme choisie vous aurez vos éléments bibliographiques dans votre document (y compris avec une bibliographie à la fin) mais il faudra re-compiler si vous changez des éléments bibliographiques. N'oubliez pas que vous devez prendre en compte toute la chaîne depuis votre format de départ (c'est pareil avec LaTeX&nbsp;: il faut recompiler si on met la biblio ou l'index à jour).

## Git&nbsp;: travailler dans l'cloude

Comme il m'arrive tous les jours d'utiliser au moins deux ordinateurs différents, j'ai choisi d'utiliser Git lorsque je dois rédiger des documents longs sur une longue période de temps. Là encore, l'avantage d'utiliser de simples fichiers texte, permet de jouer sur la rapidité et la flexibilité de Git. On peut aussi se payer le luxe (selon les programmes disponibles sur le serveur) de configurer  des *jobs* qui permettent, une fois uploadé(s) le(s) fichier(s) Markdown, de créer automatiquement les sorties voulues. Si Pandoc est installé, vous pouvez une fois pour toute configurer votre dépôt pour créer l'artefact à chaque fois.

il est donc inutile de s'encombrer localement (et encombrer le dépôt avec) des fichiers de sortie à moins de les tracer avec Git LFS (cf. ci-dessous).

Concernant les fichiers binaires, trop volumineux, les illustrations, etc. , il faut utiliser [GIT LFS](https://docs.gitlab.com/ee/workflow/lfs/manage_large_binaries_with_git_lfs.html) qui permet de trier et tracer les fichiers qu'il est inutile de recopier en doublon à chaque fois sur le serveur.

## Choisir un bon éditeur

Non, il ne s'agit pas de l'éditeur de la collection où vous allez publier votre livre. Il s'agit de l'éditeur de texte.

Pour ceux qui sont habitués à utiliser à chaque fois un logiciel de traitement de texte *wysiwyg* pour écrire, il pourrait à première vue sembler bien austère de choisir un logiciel avec une interface moins fournie (en apparence, en tout cas) pour écrire. Et pourtant, que de tranquillité ainsi gagnée&nbsp;!

Tout d'abord un bon éditeur est un éditeur avec coloration syntaxique. Avec cela, les éléments importants de la syntaxe Markdown, ainsi que les titres, les mots en italique, etc., apparaissent en évidence. Ensuite, un bon éditeur de texte est presque entièrement configurable&nbsp;: il vous sera très bénéfique de configurer selon vos goûts le thème de la coloration syntaxique et de l'interface, ainsi que la police. Cela vous assurera surtout un confort d'écriture.

Certains éditeurs sont spécialisés en Markdown, tel [Ghostwriter](https://golb.statium.link/post/20170601ghostwriterediteurmd/). Mais vous pouvez toujours utiliser des éditeurs destinés à la programmation mais disposant de tout ce qu'il faut pour écrire en Markdown. Certains sont de véritable machines à tout faire (et intègrent souvent une extension pour gérer un dépôt Git), tel [Atom](https://atom.io/) ou [Geany](https://www.geany.org/), d'autres sont des éditeurs de textes historiques du Libre, plus difficiles au premier abord, comme Vi ou Emacs (et avec un grand choix d'extensions). Et il y a ceux qui sont beaucoup plus simples (Gedit, Kate, Notepad++).

Inutile ici de s'étendre sur les centaines d'éditeurs de texte libres et disponibles pour tout un chacun. L'essentiel est de comprendre qu'un bon éditeur est d'abord celui qui conviendra à votre usage et qui vous permettra d'écrire confortablement (esthétique et ergonomie).

# Quels outils de production&nbsp;?

Pour terminer ce billet, voici les éléments pour créer mon flux de production (du moins celui auquel je me conforme le plus possible).

|      Format          |       Outils          |           Fichiers  |      Résultats     |
|----------------------|-----------------------|---------------------|--------------------|
|  Markdown            |   Pandoc + biblatex   |  ``.md``, ``.bib``,  template ``.tex``, ``.cls``, ``.sty`` | TeX (et PDF)   |
| Markdown + .bibtex   |   Pandoc + CSL        | ``.md``, ``.bib``, ``.csl``, ``.css``, template ``.html``|  HTML  |
| Markdown + .bibtex   |   Pandoc + CSL        | ``.md``, ``.bib``, ``.csl`` |   ODT                    |


Logiciels utilisés&nbsp;:

- éditeurs&nbsp;: Gedit, Ghostwriter, Atom, Zettlr (cela dépend de mon humeur)&nbsp;;
- Pour synchroniser et stocker en ligne&nbsp;: Git et Gitlab, ou bien une instance Nextcloud&nbsp;;
- LaTeX (éditeur de texte classique ou Texmaker)&nbsp;;
- LibreOffice pour un traitement de texte autre que LaTeX&nbsp;;
- JabRef et Zotero pour la gestion bibliographique (parfois un simple éditeur de texte).


Concernant la rédaction d'un ouvrage à paraître bientôt, voici comment j'ai procédé tout au long du processus d'écriture&nbsp;:

- rédaction des parties (un fichier par partie) en Markdown avec Ghostwriter la plupart du temps (il dispose d'un outil de visualisation de table des matières fort pratique)&nbsp;;
- Zotero ouvert sans interruption pour entrer les références bibliographiques au fur et à mesure et insérer leurs IDs dans le document en référence&nbsp;;
- synchronisation à chaque fin de session de travail avec Git (une instance Gitlab), et une requête de récupération à chaque début de session&nbsp;;
- pour donner à l'éditeur un  «&nbsp;manuscrit&nbsp;» en ODT suivant ses prescriptions&nbsp;: conversion avec Pandoc et un fichier CSL pour la bibliographie&nbsp;: la collaboration avec l'éditeur se fera sur Libreoffice (mise en forme, suivi de corrections).
- à venir&nbsp;: récupération du travail fini en Markdown et export HTML/epub pour la postérité.


Ajoutons à cela que l'usage du Markdown tend à se prêter à bien d'autres situations. Ainsi ce blog est écrit entièrement en Markdown, mes courriels le sont parfois, le logiciel qui me permet de prendre des notes et de les synchroniser sur un serveur utilise le Markdown (j'utilise alternativement Joplin ou Notes de Nextcloud, et de temps à autre Firefox Notes). En somme, autant de situations qui me permettent d'écrire, synchroniser et exporter&nbsp;: le Markdown est omniprésent.







