
---
title: "Scott Spark 950 : prise en main"
date: 2017-03-04
author: "Christophe Masutti"
image: "/images/postimages/hikebike.png"
description: "Rouler pour la première fois avec son nouveau VTT, c'est toujours un événement."
tags: ["Sport", "VTT", "Entraînement", "Technique"]
categories:
- Sport
---




L'hiver ne doit pas être un obstacle pour rouler et tant pis si le terrain est gras. La sortie du jour avait pour thème  : comment prendre en main un VTT tout juste sorti du carton ? En fait c'est très facile, pourvu que le vélo en question soit de bonne qualité.

## L'engin en test

Oui, parce qu'à ce niveau-là, ce n'est ni un vélo ni un simple VTT mais un engin, bourré de technologies plus avancées les unes que les autres. Lorsque j'ai vu le monteur régler les suspensions pour mon gabarit, je me suis demandé si je n'aurais pas dû choisir maths-physique à la sortie du lycée. Heureusement, une fois réglées ces suspensions, normalement on ne devrait pas avoir à y bricoler sans arrêt.

Le test a porté assez simplement sur la base de ma pratique du cross-country : 40 km environ dans les Vosges pour 1000&nbsp;m de dénivelé (ben oui, au-delà il y avait de la neige). Le VTT concerné : un [Scott Spark 950](https://www.scott-sports.com/fr/fr/products/249548008/V%C3%A9lo-SCOTT-Spark-950).

![Scott Spark 950](/images/sspark950pied.jpg)

## Alors qu'est-ce qu'il y a dans le paquet ?

D'abord une transmission Shimano XT pour le dérailleur arrière et Deore pour l'avant. D'accord, c'est pas du full-XT mais franchement pour passer les deux plateaux avant, c'est largement suffisant (et Deore reste une référence).

Dans l'ensemble, la transmission est franchement au poil. Si comme moi vous sortez direct d'un VTT avec trois plateaux avant, l'apprentissage met environ… 5 minutes, le temps de trouver une montée pour essayer tout cela. Évidemment, sur ce VTT, je conseille de prendre grand soin de cette transmission : l'équipement convient parfaitement pour une pratique cross-country mais si on tire un peu long et selon le kilométrage m'est d'avis que la longévité est proportionnelle au soin avec lequel vous passez les vitesses.

Ensuite viennent les super gadgets de chez Scott.

![Mécanisme suspension](/images/poigneesusp.jpg)

Le système Twinlock, d'abord, qui permet en trois positions, de régler le débattements des suspensions avant et arrière : relâche totale pour la descente, mode traction (semi-blocage) pour les montées un peu techniques, et blocage total pour montée régulière ou la route (bêrk). Simple gadget ou vraiment utile ? la question n'est pas sérieuse : c'est l'une des clés de la polyvalence de ce VTT. Sans ce système, l'intérêt pour le cross-country serait bien moindre. Bien sûr, il faut adapter ses réglages en fonction du terrain sur lequel on évolue, mais comme la commande est située au guidon, c'est une simple formalité.

![Mécanisme tige selle](/images/levier_tigeselle.jpg)

La commande de la tige télescopique au guidon. Là encore, l'intérêt est évident… la tige s'abaisse avec le poids du cycliste, histoire d'engager des descentes dans de bonnes conditions, puis remonte très vite une fois que l'on souhaite revenir à une position de pédalage. Petit bémol toutefois : comme on le voit sur la photo, la commande est située après la commande de transmission, et à moins d'avoir de grandes mains, il faut aller la chercher en lâchant la prise du guidon.
C'est un problème de montage et non de conception ! Je vais rapidement corriger ce problème car il est important de pouvoir manipuler les différentes commandes au guidon sans lâcher la prise. C'est normalement étudié pour cela :) Cette commande doit en fait être située entre la poignée et le levier de frein (pour affirmer cela, j'ai demandé au service après vente de chez Scott qui a mis moins de deux heures pour me répondre par courriel, merci à eux.)

De manière générale, vous avez tout intérêt à penser à baisser la selle avant d'engager la descente, mais parfois les terrains s'enchaînent plus vite que prévu.  Pour la reprise, le problème est moins difficile, sauf que généralement la main est déjà occupée à changer de vitesse. Bref, la commande de la tige au guidon, c'est génial, mais attention au montage.

Quant aux suspensions, rien à dire de particulier : fourche Fox 32 à l'avant (débattement de 100 pour le cross country) et amortisseur Fox Float à la l'arrière. Trois positions (cf. ci-dessus). Dans le domaine, Fox fait des petites merveilles.

La taille des roues est un choix… personnel. Le Spark 950 est dotée de roues en 29 pouces. Ceux qui ne jurent que par le 27.5 devront prendre le Spark 750.

Le 29 pouces et moi, c'est une longue histoire. Pour la faire courte, mon premier VTT était un Gary Fisher type «&nbsp;semi-descente&nbsp;» avec des 29 pouces, sans aucune suspension. Le fait d'avoir roulé avec cela vous fait comprendre la raison pour laquelle, si on pratique le cross-country, le 29 pouces me semble un excellent atout (mais pas le seul, j'en conviens). L'entraînement et l'inertie de grandes roues est un confort dont j'ai eu des difficultés à me passer avec mon précédent VTT qui, lui, est doté de 27,5 pouces. Passer de l'un à l'autre vous fait vraiment apprécier la différence.

Mais le bémol des 29 pouces, je l'ai rencontré aujourd'hui même : sur la neige, il y a quand même moins de répondant (trop de surface au contact). Certes, je pense que c'est aussi lié aux pneus fournis avec le Spark : des Maxxis Forecaster dont je ne suis pas un grand fan (on verra bien à l'usage). D'un autre côté, de la neige, il n'y en a pas toute l'année, hein&nbsp;?

## Conclusion

Ce VTT m'a été conseillé de toutes parts lorsque je recherchais le meilleur compromis pour ma pratique du VTT. Aucun regret. Le vélo correspond parfaitement à mes attentes et il n'y a pas de surprise. Du point de vue des performances, il est à l'image de la gamme Spark de chez Scott qui bénéficie maintenant de quelques années d'ajustements. C'est à l'usage qu'on verra ce que vaut vraiment le cadre, dans un alliage d'aluminium 6011 (avantage du nickel pour la corrosion, à envisager sur le long terme). Quant à l'entretien, il devrait être minimal.

Avec un bon réglage de départ, une étude posturale assurée par un spécialiste du magasin, ce Spark 950 est un bon investissement et s'adaptera à une pratique engagée ou non. Il se manie cependant avec discernement et nécessite tout de même un certain niveau de pratique pour en tirer tous les avantages. Je déconseillerais donc ce VTT pour un premier achat, mais une fois de bonne habitudes prises, il fera très certainement votre affaire, pour un budget qui ne grimpe pas pour autant vers les cadres carbone-super-chers :)

![Vers les hauteurs](/images/paysagespark.jpg)

