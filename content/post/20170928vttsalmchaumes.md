
---
title: "Petit tour sur les hautes chaumes"
date: 2017-09-28
author: "Christophe Masutti"
image: "/images/postimages/hikebike.png"
description: "Une balade VTT du côté du pays de Salm"
tags: ["Sport", "VTT", "Entraînement", "Parcours"]
categories:
- Sport
---


Profitons du soleil d'automne pour une sortie VTT sur les Hautes Chaumes de la région de Schirmeck / pays de Salm. Ce parcours n'est pas d'une grande difficulté technique mais nécessite à la fois de l'endurance et de la patience… (44&nbsp;km&nbsp;/&nbsp;1068&nbsp;m&nbsp;D+)

## En résumé

Au départ de Schirmeck, prenez la direction du joli petit village de Fréconrupt par le GR&nbsp;532 La montée piquera un peu, pour commencer, avec quelques efforts à fournir en guise d'échauffement. Plusieurs grimpettes de ce style vont jalonner le parcours, alors... gardez des réserves !

On prendra ensuite le temps d'admirer les frondaisons sur le chemin qui mène à Salm puis l'ascension du relief du château de Salm et de la Chatte Pendue prendra un peu de temps. Repérez, au début, le «&nbsp;chemin médiéval&nbsp;» qui longe le flanc de la montagne. On coupera ensuite au niveau du replat entre les deux sommets pour rejoindre les Hautes Chaumes. À ce niveau, il faut emprunter le chemin des passeurs puis faire un petit détour jusquà la Haute Loge (un petit chemin très amusant en VTT), où on pourra admirer la superbe vue sur les monts Vosgiens (Climont, Champ du feu, Donon...).

Il est alors temps de poursuivre au long des chaumes pour rejoindre le Lac de la Meix. Attention, le parcours emprunte le chemin «&nbsp;piéton&nbsp;», qui est très technique à la descente et que je déconseille fortement en cas de temps humide ou forte fréquentation de marcheurs. Si vous n'êtes pas à l'aise avec ce genre de pilotage, prenez plutôt le chemin forestier en amont.

Depuis le Lac il faut alors remonter et rejoindre le Col de Prayé. La descente sera à la fois technique et rapide jusqu'à la route de l'Etang du Coucou. Le retour s'effectue enfin via Malplaquet-Fréconrupt.

L'ensemble est faisable en 4 heures (le temps d'admirer les vues), pour <strong>44&nbsp;km</strong> et <strong>1068&nbsp;m&nbsp;D+</strong>.

## La carte et le tracé

<iframe width="100%" height="300px" frameBorder="0" src="https://framacarte.org/fr/map/les-hautes-chaumes_13219?scaleControl=false&miniMap=false&scrollWheelZoom=false&zoomControl=true&allowEdit=false&moreControl=true&searchControl=null&tilelayersControl=null&embedControl=null&datalayersControl=true&onLoadPanel=none&captionBar=false"></iframe>

<a href="https://framacarte.org/fr/map/les-hautes-chaumes_13219">Voir en plein écran</a>
