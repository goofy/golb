
---
title: "Misère numérique"
date: 2019-08-06
author: "Christophe Masutti"
image: "/images/postimages/hommemachine.jpg"
description: "Le couple ordinateur / Internet est à certaines administrations ce que l'hygiaphone était au guichet de La Poste il y a quarante ans."
tags: ["Libres propos", "Politique", "Libertés", "société"]
categories:
- Libres propos
---



Les techniques ont toujours créé des formes de dépendances. C'est leur rôle, leur définition. Dans la lutte pour la survie dans un environnement hostile ou pour pallier les caprices du corps, la technologie a toujours été pour l'homme la source créative des supports cognitifs, mécaniques, biologiques. Mais par-dessus tout, fruits des apprentissages, elle suscite d'autres apprentissages encore&nbsp;: apprendre à utiliser des béquilles, apprendre à gérer ses doses d'insuline avec une pompe, apprendre à conduire une voiture, apprendre à utiliser un ordinateur, apprendre à utiliser Internet.

Les apprentissages fluctuent. Dans les années 1990, tous les utilisateurs ou presque de l'Internet grand public utilisaient le courrier électronique inventé 20 ans auparavant. «&nbsp;J'ai une adresse electronique&nbsp;», lançait-on à qui voulait l'entendre&hellip; et s'en fichait pas mal. L'époque a changé. Face aux grands chambardements de la «&nbsp;digitalization disruptive&nbsp;», des plateformes «&nbsp;ouine-ouine&nbsp;» et à la «&nbsp;start-up nation&nbsp;» qui prétend achever la grande transformation des services publics en services numériques (tout en inféodant le tout à des entreprises privées), le public ne retrouve plus ses billes.

Aujourd'hui, renforçant l'idée que le courrier électronique est d'abord un outil de travail, une pléthore d'utilisateurs ne connaissent d'Internet que les applications de médias sociaux et leurs messageries. Surtout chez les plus jeunes dont on nous faisait croire, pour mieux avaler la pilule, qu'ils savaient bien mieux se débrouiller dans un monde «&nbsp;digital&nbsp;» et qu'il ne fallait donc pas craindre pour leur avenir.

Quelle connerie, cette génération Y. Construction inique de marketeux malades soumis aux GAFAM. Le résultat&nbsp;: une dépendance de plus en plus grande vis-à-vis des monopoles, la centralisation de services, la concentration des capitaux et le pompage général des données. Même les plus grands scandales, des révélations de Snowden à Cambridge Analytica, ne semblent pas changer nos rapports quotidiens à ces plateformes.

Il faut l'intégrer, regarder la réalité en face&nbsp;: les usages et les pratiques d'Internet ne sont pas (et ne seront jamais) ce que les pouvoirs publics et le marketing nous en disent. Il faut lire l'étude de Dominique Pasquier, *[L'internet des familles modestes](https://www.pressesdesmines.com/produit/linternet-des-familles-modestes/)* (2018) pour se faire une idée à peu près lucide des usages.

Zones rurales, quartiers pauvres, hyper centres urbains, les inégalités face aux usages des services numériques ne se résument pas aux inégalités d'accès aux réseaux. D'autres aspects doivent être pris en compte de manière prioritaire&nbsp;:

- compréhension des enjeux de la confidentialité et de la sécurité des données, 
- apprentissage des protocoles de communication (et destination des usages)&nbsp;: courrier électronique, médias sociaux, messageries instantanées,
- rapports cognitifs aux contenus (savoir estimer la fiabilité d'un contenu, et donc la confiance à accorder et le degré d'attention qu'on peut lui prêter),
- etc.

Le modèle publicitaire des plateformes est un modèle de sectorisation et donc d'exclusion. Les bulles de filtres et autres usages qu'imposent Facebook, Twitter ou Google, par exemple, ne sont en fait qu'un aspect parmi d'autres où l'efficacité de l'usage des outils est conditionné par les choix permanents des utilisateurs. Ceux qui s'en sortent sont ceux qui ont les meilleures capacités à trier les contenus et gérer leur attention. Il se génère ainsi une charge mentale qui, avant cette numérisation généralisée des services, n'était pas nécessaire.

Faire croire une seconde que la connexion à un service public comme celui des impôts est indépendant de ces conditions cognitives et pratiques est un mensonge. Quels que soient les avantages qu'une partie de la population pourra trouver à pouvoir échanger à distance avec une institution publique, si on ne change pas d'urgence le modèle, les grandes inégalités qui existent déjà vont s'agraver.

Quel est ce modèle des services publics «&nbsp;numérisés&nbsp;»&nbsp;? il se résume finalement à un principe simpliste&nbsp;: faire la même chose qu'avant mais par ordinateurs et applications interposés. Aucune remise en cause des organisations, des pratiques, du rapport au public.

Le couple ordinateur / Internet est à certaines administrations ce que l'hygiaphone était au guichet de La Poste il y a quarante ans. L'hygiaphone avait une fonction&nbsp;: instaurer plus de distance entre le préposé et l'usager. Au sommet&nbsp;: l'hygiaphone avec micro et haut-parleur, où même la voix humaine était devenue méconnaissable. 

Désormais on a trouvé un moyen plus efficace de balancer une partie du travail administratif sur les usagers en rendant impossibles des demandes effectuées en présence. Invariablement, toute personne se rendant dans un centre des impôts ou une mairie en ressort avec une injonction&nbsp;: «&nbsp;rendez-vous sur internet pour faire votre démarche&nbsp;». Et le pire, dans ces histoires, c'est que les préposés ne se posent jamais la question de savoir si la personne qu'ils renvoient ainsi dans les cordes numériques, sans ménagement, sont bel et bien en mesure de remplir les bonnes conditions d'usage du service numérique concerné. Corollaire&nbsp;: ces mêmes préposés scient aussi la branche de l'emploi dont ils dépendent.

Vous avez 80 ans avec une déclaration d'impôts un peu complexe&nbsp;? Vous devez refaire votre carte d'identitié&nbsp;? Un rendez-vous chez le médecin&nbsp;? Remplissez des formulaires sur Internet, demandez des rendez-vous depuis une application&hellip; et si vous n'y parvenez pas, soit vous avez des enfants et des petits-enfants capables de vous aider, soit&hellip; rien.

Indiscutablement, les personnes âgées sont les premières victimes de cette misère numérique. C'était d'ailleurs l'objet du rapport des Petits Frères des Pauvres en 2018, intitulé «&nbsp;[Exclusion numérique des peronnes âgées](https://www.petitsfreresdespauvres.fr/informer/prises-de-positions/contre-l-exclusion-numerique-de-4-millions-de-personnes-agees)&nbsp;». La misère numérique est sociale. Elle n'est plus seulement un  clivage géographique entre zones rurales et zones urbaines, entre pays riches et pays pauvres. 

La faute à la politique&nbsp;? à l'économie&nbsp;? On pourra invoquer toutes sorte de raisons pour lesquelles nos aînés aussi bien que les pauvres ou les jeunes précaires se trouvent tous doublement exclus. L'exclusion sociale et l'exclusion numérique vont de pair. On ne peut pas décorréler la question des usages numériques d'une lutte contre les inégalités sociales et économiques.


La transformation numérique des services publics s'est effectuée en excluant les utilisateurs. Tout changement technologique suppose une révision des organisations. Ici, il s'agit de l'ensemble de la société et pas seulement la réduction des effectifs des services. Imposer de tels changements dans les usages tout en conservant de vieux modèles de gestion est une mauvaise stratégie. Elle ne peut déboucher que sur l'exclusion.

Nos responsabilités individuelles ne sont pas pour autant exemptées. Renvoyer une personne âgée sur Internet lorsqu'elle vient effectuer une démarche administrative est un comportement inacceptable. Il est la preuve d'un service public mal rendu et le signe d'une indigence morale. 

Quant à l'avenir, il se dessine déjà. D'aucuns pourraient penser que, habitués comme nous le sommes, nous n'aurons aucun mal à interagir avec ces services numériques lorsque nous aurons l'âge de nos aînés. C'est faire preuve de naïveté. D'abord, croire que la situation restera la même, avec les mêmes technologies et les mêmes usages. Ensuite, croire que nos capacités physiques et cognitives restent les mêmes qu'à vingt ans. Rien n'est plus faux. En entretenant ce genre de croyances nous construisons tous ensemble les cercueils numériques de nos vieux jours. 








