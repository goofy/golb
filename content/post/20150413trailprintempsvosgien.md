---
title: "Trail de printemps... vosgien"
date: 2015-04-13
author: "Christophe Masutti"
image: "/images/postimages/hikebike.png"
tags: ["Sport", "Trail", "Entraînement", "Parcours", "Vosges"]
description: "Encore une balade autour de la Perle des Vosges"
categories:
- Sport
---


On le sent qui arrive ce fameux printemps, mais ce n'est pas toujours évident. J'avais déjà rangé mes mitaines et mes collants doublés, ressorti ma casquette fétiche et mes shorts. Et puis voilà, dans un dernier élan de témérité, je me suis lancé dans un dernier trail hivernal en ce début de mois d'avril. En bon vosgien qui se respecte, néanmoins, la méfiance l'a emporté. Voyant qu'il restait tout de même pas mal de neige sur les pistes de ski gérômoises, je m'attendais à progresser contre le climat hostile et rude. Ils étaient tous là au rendez-vous, ces fidèles amis : la neige sur laquelle on s'enfonce une foulée sur 3, la pluie froide et intermittente, le granit glissant, la boue provoquée par l'eau des fontes… Un dernier petit plaisir pour un nouveau parcours autour de Gérardmer avec trois points de vue différents.

## Description

C'est un parcours d'une durée de 2h45 (moins si la météo est clémente) avec 945m D+ autour de la Perle des Vosges. En partie, ce parcours recouvre certains tracés de la station de trail mais le principal avantage, à mes yeux, est qu'il constitue un terrain d'entraînement figurant les principales difficultés du parcours du [trail court de la Vallée des lacs](http://trailvalleedeslacs.com/) (qui aura lieu en juin). Comme ce tracé était encore enneigé, le circuit n'étant pas ouvert, il fallait trouver une solution alternative. Par ailleurs, cela fait un parcours de rechange pour éviter de voir toujours le même paysage.

Les deux principaux avantages touristiques du parcours&nbsp;:

- Circuit autour de Gérardmer avec les points remarquables suivants : Le lac de Gérardmer, la Roche du Rain, les pistes de ski alpin (la Mauselaine, la Chaume Francis, le Poli), le Saut des Cuves, la Roches des Bruyeres, la ferme de Vologne, la Gorge des Roitelets, Kichompré (hameau), Chapelle de la Trinité, Miselle, Les Xettes, le Lac de Gérardmer.
- À chaque sommet, les points de vue porteront invariablement sur la vallée de Gérardmer avec vues Ouest, Nord et Est.

Les difficultés du parcours&nbsp;:

- Deux raides montées en sentier "single", mais relativement courtes,
- Des montées et descentes assez longues en chemins forestiers larges mais pierreux,
- Une descente très fun mais délicate dans une gorge,
- Pour boucler en 20km, la fin est bitumeuse (sur 2km de descente) mais il est possible de faire une variante forestière en rallongeant un peu.


En quelques mots, le début du parcours reprend le tracé du trail de la Vallée des Lacs, mais s'en sépare du côté de Saint Jacques pour bifurquer sur les pistes du Poli. On remarquera au passage que la première montée du célèbre trail vosgien est particulièrement amusante mais un peu décevante car elle s'achève sur du bitume jusque la Mauselaine. On accusera au passage la bitumisation effrénée du site à des fins de locations touristiques. On admirera alors la vue de Gérardmer depuis les pistes de ski, en particulier celle du Tetras. La descente du Poli jusqu'au Saut des Cuves n'a rien d'exceptionnel mais la montée vers la Roche des Bruyères est un beau sentier pierreux et vraiment montagnard. La Roche présente une vue de toute la longueur de la vallée de Gérardmer jusqu'au lac. On pourra alors apprécier sur la droite, le prochain sommet à atteindre et, à gauche, un aperçu du parcours déjà accompli. La descente s'achèvera par les gorges du Roitelet, un petit ruisseau nerveux qui se jette ensuite dans la Vologne encore naissante à ce niveau. Le petit hameau de Kichompré et sa tranquillité reposeront un peu les jambes pour remonter ensuite assez sèchement jusque la Trinité puis Miselle. Le retour se fait dans la clarté d'un coteau ensoleillé (enfin... selon la météo).


![Profil du parcours](/images/profiltrailtourgerardmer.png)

[Carte disponible ici](http://umap.openstreetmap.fr/fr/map/trail-tour-de-gerardmer_36113)


## Quelques photos

Précision : ces photos ont été prises rapidement :)

![La Gorge des Roitelets](/images/gorgeroitelet1.jpg)
![La Vologne](/images/lavologne.jpg)
![Neige sur le chemin](/images/neigechemin.jpg)
![Roche des Bruyères](/images/rochebruyeres.jpg)
![Sentier](/images/sentier.jpg)



