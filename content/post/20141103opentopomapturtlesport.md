---
title: "Utiliser OpenTopoMap avec Turtlesport"
date: 2014-11-03
author: "Christophe Masutti"
image: "/images/postimages/terminal.png"
description: "Utiliser OpenTopoMap avec Turtlesport"
tags: ["Logiciel libre", "Sport", "Cartographie"]
categories:
- Logiciel libre
- Sport
---

Petit truc :</strong> voici la manière d'utiliser les fonds de carte <a href="http://opentopomap.org">OpenTopoMap</a> avec <a href="http://turtlesport.sourceforge.net/FR/home.html">Turtlesport</a>.

<!--more-->

Il suffit pour cela de se rendre dans ``Aide > Préférences > Carte / Fournisseur``. À cet endroit, il est possible d'indiquer le fond de carte que l'on souhaite utiliser. Pour OpenTopoMap, il faut entrer l'adresse suivante :

```
http://a.tile.opentopomap.org/#zoom#/#x#/#y#.png
```

```
Pour le zoom, le minimum est à 1 et le maximum à 17.
```

(Merci [@Turtlesport](https://twitter.com/turtlesport) qui m'a confié la bonne adresse)

