---
title: "Affaires privées : les coulisses"
date: 2020-06-02
author: "Christophe Masutti"
image: "/images/postimages/easyoldstyle.jpg"
description: "À la suite d'un échange sur une plateforme de microblogage, j'ai promis de décrire ma méthode de production de manu-(tapu)scrit en vue de l'édition d'un livre. Ce billet vise à montrer à quel point les formats ouverts sont importants dans un processus d'écriture. Il témoigne de mes préférences et n'est en aucun cas une méthode universelle. Cependant j'ose espérer qu'il puisse donner quelques pistes profitables à tous !"
draft: false
tags: ["Markdown", "bidouille", "pandoc", "éditeur", "traitement de texte"]
categories:
- Logiciel libre
---

À la suite d'un échange sur une plateforme de microblogage, j'ai promis de décrire ma méthode de production de manu-(tapu)scrit en vue de l'édition d'un livre. Ce billet vise à montrer à quel point les formats ouverts sont importants dans un processus d'écriture. Il témoigne de mes préférences et n'est en aucun cas une méthode universelle. Cependant j'ose espérer qu'il puisse donner quelques pistes profitables à tous !

<!--more-->

# Identification des besoins

Le livre dont il est question ici (il est bien, [achetez-le](https://cfeditions.com/masutti) !) s'intitule *[Affaires privées. Aux sources du capitalisme de surveillance](https://cfeditions.com/masutti)*, publié chez C&F Éditions, mars 2020. Il fait un peu moins de 500 pages. La production du manuscrit proprement dit (le fichier qu'on envoie à l'éditeur) m'a pris environ 5 mois. Sur cette base, l'équipe éditoriale (qu'elle en soit ici remerciée) s'est livrée au travail de relecture, avec plusieurs va-et-vient avec moi pour les corrections, et enfin un gros travail de mise en page.

Ce dont je vais traiter ici concerne la production du tapuscrit, et donc le processus d'écriture proprement dit, qui regarde essentiellement l'auteur. Pour le reste, il s'agit d'un travail éditorial avec d'autres contraintes.

Au risque d'être un peu caricatural il va me falloir décrire rapidement ce qu'est un travail de recherche en histoire et philosophie des sciences. On comprendra mieux ainsi pourquoi le choix des formats de fichiers est important dans l'élaboration d'un flux de production d'écriture pour un document long, comme un livre, ou même lorsqu'on s'apprête à écrire un article.

Dans ce type de travail, il ne suffit pas de récolter des documents historiques (pour ce qui me concerne, il s'agit de textes, photos, vidéos). Ces sources sont stockées dans différents formats, surtout des PDF (par exemple des fac-similés, des copies d'articles), des transcriptions sous forme de texte (le plus souvent dans un format texte de base), etc. Étant donnée cette diversité, un premier impératif consiste à ne pas sur-ajouter des formats et tâcher de faire en sorte que les notes de consultation soient accessibles dans un format unique et le plus malléable possible. Par ailleurs, se pose aussi la question du stockage de ces sources : les avoir à disposition, où que je me trouve, et m'assurer de la pérennité de leur sauvegarde.

Outre les sources, je dispose de travaux d'écriture préalables. Écrire un manuscrit en 5 mois n'est possible qu'à la condition d'avoir aussi à disposition une matière déjà travaillée en amont, comme un terrain préparatoire à l'écriture proprement dite. Il faut considérer ces travaux préparatoires comme des sources avec un statut particulier.

Il en est de même pour les notes de lectures qui sont aussi des réflexions sur les enjeux philosophiques, historiques et politiques, la méthodologie, les idées critiques, bref des sources particulières puisque produites par moi et toujours soumises à modifications au fur et à mesure de la maturité d'une pensée et de l'acquisition de connaissances.

Se pose ensuite la question du recensement des sources et, de manière générale, toute la bibliographie qu'il faudra utiliser dans le manuscrit. Dans ce genre de discipline, au risque de barber sérieusement le lecteur, une forme d'obligation consiste à citer la bibliographie « à la page » chaque fois que cela est possible. Afin de pouvoir mobiliser ces éléments qui se chiffrent par centaines, il faut bien entendu avoir un système capable de trier, classer, et sortir des références bibliographiques exploitables.

Un autre impératif consiste à élaborer un flux d'écriture qui soit à la fois rapide et efficace tout en l'édulcorant au maximum des contraintes logicielles. Ces contraintes peuvent être liées à la « lourdeur » du logiciel en question, par exemple le temps passé à saisir la souris pour cliquer sur des icônes est autant de temps en moins consacré à l'écriture. Se libérer de ces contraintes va de pair avec une certaine libération des contraintes matérielles : l'idéal consiste à pouvoir écrire sur différents supports c'est-à-dire un ou plusieurs ordinateurs, un smartphone ou une tablette. Pour ce qui me concerne, se trouver coincé par l'accès à un logiciel sur un seul type de support m'est proprement insupportable.

En résumé, les besoins peuvent s'énumérer ainsi : écrire, utiliser différentes machines, synchroniser et sauvegarder, gérer et exploiter une bibliographie, produire un résultat. Allons-y.




# Écrire

C'est de plus en plus répandu : les utilisateurs qui produisent du contenu en écriture n'utilisent plus uniquement les logiciels de traitement de texte tels MSWord ou LibreOffice Writer en première intention. Demandez-vous par exemple ce qu'utilisent tous les blogueurs via des interfaces d'écriture en ligne, ou regardez de près le logiciel de prise de notes que vous avez sur votre smartphone : la mise en page est assurée ultérieurement, tout ce que vous avez à faire est simplement d'écrire.

Si j'ai traduit et adapté [un manuel sur LibreOffice Writer](https://framabook.org/libreoffice-cest-style/), c'est parce que je considère ce logiciel comme l'un des plus aboutis en traitement de texte mais aussi parce que le format Open Document est un format ouvert qui permet l'interopérabilité autant qu'un travail post-production. Il en est de même pour LaTeX (qui est un *système* de composition) et dont les fonctionnalités sont quasi infinies. J'adore LaTeX.

Alors… pourquoi ne pas utiliser d'emblée l'un de ces deux logiciels ? Parce que ce dont j'aimerais vous parler ici, ce n'est pas de la production de document, mais de l'écriture elle-même. Mélanger les deux signifie  anticiper la production finale (au risque de se tromper). Quoi qu'on en dise, c'est en tout cas mon cas, on se laisse facilement distraire par les fonctionnalités du logiciel.

Pour écrire, et simplement écrire, je n'ai besoin que d'un éditeur de texte. C'est-à-dire un logiciel qui me permet d'entrer du texte et d'enregistrer sans information de formatage (on parle de *plain text*) ou alors le strict minimum. C'est sur ce strict minimum qu'il faut réfléchir en allant un peu plus loin dans le raisonnement.

En effet, lors de la production finale, je n'ai pas très envie de revenir sur chaque partie du texte nécessitant un traitement typographique au cours de l'écriture. Par exemple certains mots en emphase, le signalement des titres de parties, sections et sous-sections. Idem, j'ai aussi besoin de pouvoir formater mon texte de manière à ce qu'il se prête ultérieurement à toutes les transformations envisageables nécessitant un processus de traitement comme la génération des références bibliographiques ou l'ajout de liens (URLs). Ces choses-là sont toujours possibles en *plain text* mais il y a des solutions plus pratiques.

Il existe un format très intéressant : le [markdown](https://fr.wikipedia.org/wiki/Markdown). C'est du texte, mais avec quelques ajouts d'informations très simples qui permettent un traitement ultérieur lors de la production finale. Ce langage de balisage est léger. Il nécessite un apprentissage de quelques minutes et il s'applique facilement au cours de l'écriture sans la freiner outre mesure. C'est dans ce format que j'écris, y compris par exemple le texte de ce billet de blog. Ce format est par définition ouvert (cela reste du texte).

En pratique, il ne reste plus qu'à choisir l'éditeur de texte ou un éditeur de texte spécialisé en markdown. Sur ce point j'ai déjà eu l'occasion de [me prononcer](https://golb.statium.link/tags/markdown/) sur ce blog. Par aillieurs, j'accorde beaucoup d'importance au confort visuel de l'écriture. La possibilité de configurer l'affichage l'éditeur (couleurs du fond, style de la police, de la syntaxe, raccourcis, etc.), en jouant notamment sur les contrastes, me permet d'écrire plusieurs heures durant sans avoir trop de gêne visuelle. Il s'agit d'un confort personnel. Il y a d'autres fonctionnalités que l'on trouve dans les éditeurs de texte[^1], par exemple :

- éditer le texte à plusieurs endroits à la fois avec un *split* de fenêtres,
- faire des recherches sur des [expressions régulières](https://fr.wikipedia.org/wiki/Expression_r%C3%A9guli%C3%A8re),
- un panneau latéral ouvrant le contenu du dossier sur lequel on travaille,
- le surlignage d'occurrences,
- la possibilité d'avoir accès à un terminal sans quitter l'éditeur[^2],
- etc.

Enfin, j'y reviendrai plus tard, l'utilisation de fichiers non binaires se prête admirablement à l'utilisation de [Git](https://fr.wikipedia.org/wiki/Git), un outil de gestion de version qui, dans mon cas, me sert surtout à sauvegarder et synchroniser mon travail d'écriture. Et il se prête tout aussi admirablement à la conversion vers d'autres formats à leur tour exploitables tels ``odt``, ``html`` et ``tex``.

Mes éditeurs de texte préférés du moment sont les suivants : [Gedit](https://wiki.gnome.org/Apps/Gedit), [Atom](https://atom.io/), [Ghostwriter](https://wereturtle.github.io/ghostwriter/). Depuis peu, j'utilise aussi Zettlr pour mes notes, voyez [ici](https://golb.statium.link/post/20200218zettlr/).


# Utiliser différentes machines

Au fur et à mesure de l'avancée des travaux, on se retrouve généralement à utiliser différents dispositifs. La question se pose : doit-on réserver uniquement la tâche d'écriture à l'ordinateur de son bureau ? Pour moi, la réponse est non.

Sur tous les ordiphones et tablettes, il existe des logiciels de prise de notes qui utilisent le format texte et, comme c'est souvent le cas, le markdown. L'application Nextcloud, par exemple, permet aussi d'utiliser Nextcloud Notes, un logiciel de prise de notes qui synchronise automatiquement les notes sur un serveur Nextcloud (on peut le faire aussi avec n'importe quelle application de notes pourvu que l'on sauvegarde ses fichiers dans le dossier synchronisé). Cela est utile dans les cas suivants :

- le processus d'écriture n'est pas toujours « sur commande ». Même si les meilleures idées ne surgissent pas toujours inopinément, et même si bien souvent ce qu'on pensait être une bonne idée s'avère être un désastre le lendemain, il est utile de pouvoir développer quelques passages sans pour autant être assis à son bureau. J'utilise pour cela Nextcloud Notes, soit en tapant directement la note, soit par dictée vocale.
- recopier tout un texte lorsqu'on est en train de consulter des documents ? que nenni, soit on utilise un scanner et un logiciel de reconnaissance de caractères, soit on peut carrément utiliser son ordiphone (avec [ce logiciel](https://f-droid.org/fr/packages/org.atai.TessUI/) par exemple), pour produire un texte à placer dans ses notes.

L'essentiel est de ne pas dépendre d'un dispositif unique présent dans un lieu unique. On peut utiliser plusieurs ordinateurs et des lieux différents, utiliser l'ordiphone qu'on a toujours avec soi, la tablette sur laquelle on lit un livre et exporte des notes de lecture…

Tout ceci permet de générer rapidement des ressources exploitables et de les organiser en provenance de différents outils. Pour cela d'autres logiciels doivent encore être utilisés afin de sauvegarder et de synchroniser.


# Synchroniser et sauvegarder

Comme je viens d'en discuter, l'organisation est primordiale mais s'en préoccuper sans cesse est  plutôt lassant. Mieux vaut automatiser le plus possible.

Séparons dans un premier temps ce qui a trait à la gestion des sources documentaires et la production écrite.

Pour la gestion documentaire, l'ensemble est stocké sur un serveur Nextcloud (hébergé sur un serveur de confiance[^3]). Sur chaque machine utilisée, une synchronisation est paramétrée avec un client Nextcloud. On travaille directement dans le dossier qui se synchronise automatiquement.

Pour la production écrite, c'est Git qui sera utilisé avec un serveur Gitlab ([Framagit](https://framagit.org) en l’occurrence). Sont concernés les fichiers suivants :

- l'écriture des chapitres (fichiers ``.md``),
- les notes (autres que les notes de lecture) (fichiers ``.md``),
- le fichier bibtex de la bibliographie (contenant aussi le référencement descriptif de chaque source documentaire ainsi que les notes de lecture) (fichiers ``.bib``).


Pour terminer, il faut aussi penser à faire une sauvegarde régulière de l'ensemble sur le second disque dur de la machine principale sur laquelle je travaille, doublée d'une sauvegarde sur un disque externe. La première sauvegarde est automatisée avec ``rsync`` et tâche ``cron`` exactement comme c'est décrit dans [ce billet](https://blog.dorian-depriester.fr/linux/cron-rsync-lassurance-de-la-sauvegarde-de-vos-donnees). La seconde tâche pourrait aussi être automatisée, mais je la fais manuellement pour éviter de toujours laisser branché le disque externe.



# Gérer la bibliographie

Gérer sa bibliographie ne s'improvise pas. Il faut d'abord trouver le logiciel qui répond à ses attentes. Le choix est assez large pourvu que le logiciel en question puisse être en mesure d'exporter une bibliographie dans un format ouvert, exploitable par n'importe quel autre logiciel. 

En la matière, selon moi, les deux formats intéressants sont le ``.bib`` et le ``.xml``. Au stade de l'écriture, l'essentiel est donc de choisir un logiciel avec lequel on se sent à l'aise et dont les fonctionnalités d'export sont les plus étendues possibles. J'ai choisi [Zotero](https://www.zotero.org/) et [JabRef](https://www.jabref.org/). 

Pourquoi ces deux logiciels ? Dans les deux cas, des extensions permettent de les intégrer au navigateur Firefox (ainsi qu'à LibreOffice). Dans les deux cas on peut synchroniser la biblio en ouvrant un compte Zotero ou en plaçant son fichier ``.bib`` sur Nextcloud dans le cas de JabRef.

Mon usage est un peu exotique, je l'avoue : j'utilise Zotero pour engranger un peu tout et n'importe quoi et j'utilise JaBref pour mon travail proprement dit en utilisant le format ``.bib``et souvent en éditant directement le fichier avec un éditeur de texte. Disons que le ``.bib`` final est un fichier très « propre » contenant pour l'essentiel la bibliographie citée alors que Zotero contient un peu de tout au gré des récupérations avec l'extension Firefox. Ce n'est pas l'essentiel : quoi qu'il en soit, lors de la production finale, on configurera ce qui doit apparaître ou non dans la bibliographie.

Enfin, je préfère utiliser le ``.bib`` car il est fait au départ pour le logiciel Bibtex lui-même destiné à une utilisation avec LaTeX. Si je veux pouvoir avoir le choix de production finale, autant utiliser ce format. En outre, sa lisibilité lorsqu'on l'édite avec un éditeur de texte permet de retravailler les informations, surtout en cas de problème d'encodage.


# Produire un résultat

Nous voici arrivés à la production du manuscrit (on dit plutôt un tapuscrit dans ce cas). Il faut alors se demander ce que veut l'éditeur. Dans notre cas, il s'agissait de produire un fichier ``.odt`` avec une présentation complète de la bibliographie.

Nous avons à disposition des fichiers markdown et un fichier bibtex. On peut en faire exactement ce qu'on veut, à commencer par les éditer ad vitam æternam en *plain text*.

On notera que pour entrer des références bibliographique dans un document en markdown, la syntaxe est très simple et l'utilisation est décrite dans [plusieurs billets](https://golb.statium.link/tags/markdown/) de ce blog.

On utilisera pour cela un logiciel libre du nom de [Pandoc](https://pandoc.org/) qui permet non seulement de convertir du markdown en plusieurs types de formats mais aussi d'effectuer simultanément des opérations de compilation pour la bibliographie avec un processeur CSL nommé [citeproc](https://en.wikipedia.org/wiki/CiteProc).

En effet, pour présenter la bibliographique  il faut utiliser un format de style bibliographique. On utilise le [Citation Style Language](https://fr.wikipedia.org/wiki/Citation_Style_Language) (CSL) qui est basé sur un agencement de champs en XML. Pour créer un modèle CSL, le mieux est encore de partir sur la base de modèles existants et adapter le sien. Il existe par exemple [cet éditeur CSL](https://editor.citationstyles.org/visualEditor/).

En d'autres termes il s'agit d'ordonner ceci à Pandoc :

> S'il te plaît, transforme mon fichier markdown en fichier au format ``.odt`` tout en utilisant la bibliographie que voici au format ``.bib`` et le modèle de présentation de bibliographie  ``.csl`` que voici.

La ligne de commande est alors la suivante :
 

	pandoc -s --bibliography mabiblio.bib --filter pandoc-citeproc --csl modif-monmodele.csl fichier-entree.md -o fichier-sortie.odt

Cela c'est pour l'éditeur ! pour ce qui me concerne, ce sera plutôt une transformation en ``.tex`` et une bonne vieille compilation avec XeLaTex + Biber avec un style de mise en page sympathique. De même… pour du ``epub``.

Le seul ennui est qu'à ce stade il fallait encore travailler le texte et tout le processus de relecture qui s'est déroulé exclusivement avec LibreOffice Writer et la fonction de suivi de modification. On a donc quitté la quiétude des fichiers texte pour entrer dans un processus d'échange de fichiers ``.odt``. Ce travail aurait tout aussi bien pu s'effectuer avec Git, évidemment. Mais au bout du compte il aurait tout de même fallu créer un ``.odt`` avec les styles de l'éditeur afin d'entrer le tout dans la moulinette de son processus éditorial. Mais cela ne nous… regarde pas.


# Conclusion

Comme on peut le voir, tout le processus considère le fichier ``.odt`` comme  un format obtenu en fin de production et non pas un format sur lequel on travaille directement. C'est une posture qui se justifie par la volonté de conserver la plus grande malléabilité possible sur les résultats finaux et sur la manipulation des fichiers de travail.

Par ailleurs concernant le flux de production d'écriture, une fois l'habitude prise, il est extrêmement confortable d'écrire « au kilomètre » dans un éditeur de texte bien configuré et qui, par sa légèreté n'a besoin que de très peu de ressources système.

Toute l'importance des formats ouverts est là, tant au niveau de la lecture avec différents logiciels qu'au niveau de la production finale et l'interopérabilité qu'ils permettent. De la même manière, on peut voir que tout le travail s'est effectué avec des logiciels libres (et sur des systèmes d'exploitation libres). Le grand choix dans ce domaine permet à chacun de travailler selon ses envies et ses petites manies.

[^1]: Certaines de ces fonctionnalités se retrouvent aussi sur LibreOffice Writer, bien entendu, mais je trouve que leur accès est plus rapide dans un éditeur de texte simple.


[^2]: Certains me diront d'utiliser Emacs ou Vim. C'est ce que je fais de temps en temps mais l'apport vis-à-vis de mon travail n'est pas significatif.



[^3]: Il s'agit de mon serveur à moi et aussi du serveur destiné à l'usage interne des membres de l'association Framasoft. J'utilise les deux mais c'est un détail.
