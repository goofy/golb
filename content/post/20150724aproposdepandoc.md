---
title: "Un aperçu de Pandoc"
date: 2015-07-24
author: "Christophe Masutti"
image: "/images/postimages/terminal.png"
tags: ["Logiciel libre", "Bidouillage", "Pandoc", "Markdown", "LaTeX"]
description: "Traduction d'un article remarquable de Massimiliano Dominici"
categories:
- Logiciel libre
---

Dans un billet précédent, je parlais des avantages du Markdown dans les processus de conversion vers des formats de sortie comme HTML et LaTeX. Il se trouve que **Massimiliano Dominici**, passé maître dans l'usage de Pandoc, a publié en 2013 un article remarquable focalisant justement sur les fonctionnalités de Pandoc du point de vue des objectifs de publication HTML et LaTeX (PDF).


## Présentation

Avec l'autorisation de l'auteur, je propose une traduction française de cet article qui ne manquera pas d'intéresser ceux qui hésitent encore à franchir le pas de la ligne de commande avec Pandoc ou tout simplement ceux qui ne connaissent pas encore ce logiciel.

Cet article de Massimiliano Dominici est paru sous le titre «&nbsp;[An overview of Pandoc](https://tug.org/TUGboat/tb35-1/tb109dominici.pdf)&nbsp;», *TUGboat* 35:1, 2014, pp. 44-50. Originellement publié sous «&nbsp;[Una panoramica su Pandoc](http://www.guitex.org/home/images/ArsTeXnica/AT015/pandoc.pdf)&nbsp;», *ArsTEXnica* 15, avril 2013, pp. 31-38. Traduction de l’anglais par Christophe Masutti, avec l’aimable autorisation de l’auteur. Licence du document : [CC By-Sa](http://creativecommons.org/licenses/by-sa/4.0/).


Vous trouverez sur [ce dépôt GIT](https://git.framasoft.org/Framatophe/ApercudePandoc) les sources qui ont servi à la composition et à la compilation du document, en utilisant Pandoc's Markdown. Il est possible de télécharger directement la version PDF. Le fichier ``README.md`` contient les lignes de commandes destinées aux sorties voulues.


## Comment compiler ce document ?



Pour une sortie LaTeX, avec le template LaTeX (ne pas oublier d'installer ``pandoc-citeproc``) :

      $pandoc --listings --bibliography=biblio.bib  --biblatex metadata.yaml --number-sections -s --template=modele.latex --toc source.markdown -o sortie.tex


Pour la sortie PDF :

      $pdflatex sortie.tex
      $biber sortie
      $pdflatex sortie.tex


Pour la sortie HTML :

      $pandoc -s -S --toc --template=modele.html  -H style.css --number-sections --bibliography=biblio.bib  metadata.yaml source.markdown -o sortie.html


