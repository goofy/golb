---
title: "TVL 2015 : attention, ça glisse !"
date: 2015-06-22
author: "Christophe Masutti"
image: "/images/postimages/hikebike.png"
tags: ["Sport", "Trail", "Entraînement", "Parcours"]
description: "Récit du Trail de la Vallée des Lacs"
categories:
- Sport
---

Le [Trail de la Vallée des Lacs](http://trailvalleedeslacs.com/) est sans doute l'un des plus fun des trails du massif des Vosges, tant par son parcours que par les aléas climatiques qui caractérisent si bien nos vallées. Je ne pouvais repousser à une date ultérieure ma participation à ce trail. Pour cette première fois, je n'ai pas été déçu, ni par l'organisation ni par le parcours.



## Descriptif

Ce fut un week-end assez particulier que ces 20 et 21 juin 2015. Le premier jour, dédié aux grands parcours des 55 et 85 km fut très difficile pour les coureurs, avec du vent et des pluies glaciales sur les crêtes vosgiennes. En les voyant redescendre, et même si la perspective de la ligne d'arrivée dessinait sur leurs visages un soulagement méritoire, on remarquait les stigmates que la course n'avait pas manqué d'imprimer sur les corps fatigués. Ce n'est pas tant la longueur ou les dénivelés qui sont les plus durs sur ce trail, mais bel et bien le terrain et le climat.

Le trail court du lendemain, avec ses 29 km (27 annoncés) et ses 1300 m D+ n'offrait qu'une petite partie des difficultés de la veille. Sous le portail du départ, avec la pluie et la fraîcheur revigorante du matin, tout le monde savait à quoi s'en tenir : des pistes glissantes et une boue bien grasse savamment préparées par les coureurs de la veille, ce qui ne devait pas aller en s'améliorant, surtout en fin de peloton. Ceux qui comme moi, avaient déjà reconnu le tracé, savaient aussi quels endroits plus techniques devaient retenir toute l'attention du traileur pour ne pas risquer la blessure.

L'organisation fut exemplaire : profitant chaque fois de l'expérience des sessions ultérieures, le balisage et les indications des bénévoles (forts courageux pour attendre des heures sous la pluie) furent largement appréciés. Aucune place à l'erreur.

Le départ fut donc donné avec les mises en garde de rigueur et la foule (plus de 500 coureurs) se mit en mouvement. Dans l'ensemble, le démarrage fut plutôt serein pour une mise en jambe profitable. Dès le premier single, le bouchonnage fut prévisible, avec presque aucune possibilité de dépassement. La situation devra se renouveler deux autres fois, dans les descentes vers le lac de Longemer, à la fois à cause du ravinage et de l'instabilité des sentiers. S'il fallait prendre de l'avance, c'est sur la première montée des pistes de la Mauselaine qu'il fallait le faire : un chemin bien large au dénivelé assez régulier permettait de prendre un rythme de croisière confortable.

Les montées les plus difficiles du parcours se situaient sur sa première moitié et juste un peu après le ravitaillement de mi-parcours. Des raidillons pas franchement longs mais assez éprouvants pour les jambes en particulier avec un terrain glissant. La difficulté principale de la seconde moitié du parcours tenait selon moi au terrain des chemins, avec à certains endroits des mares de boues qui mettent à l'épreuve nos capacités de saut en longueur. Au bout d'un moment, tout le monde abandonne l'idée de se salir le moins possible : on est là pour cela, après tout. Cependant, les rotules en prennent pour leur grade à chaque fois que, sous la boue, une pierre ou une racine fait perdre l'équilibre.

Les 5 derniers kilomètres ne furent toutefois pas aussi difficiles que prévu. Quelques rayons de soleil firent leur apparition à notre grande joie. Arrivés au camp de base, beaucoup de coureur ne résistèrent pas bien longtemps avant de plonger les jambes dans le lac pour ôter le plus gros des souvenirs forestiers incrustés sur les mollets... Vivement l'année prochaine !

![Christophe Masutti, Trail de la vallée des lacs](/images/cmasutti_trail_vallee_des_lacs_20150621.jpg)

<h2>Résultats et tracé</h2>


- Les résultats du trail sont consultables [à cette adresse](https://www.chronorace.be/Classements/Classement.aspx?eventId=1137419009160923&amp;mode=large&amp;IdClassement=11909).
- Mon temps : 4:11:16</li>
- Classement : Scratch 306/546 (90 V1H / 130 VEM)</li>


Contrairement à ma reconnaissance du parcours, cette fois, le tracé est bel et bien exact. Vous pouvez aussi télécharger le fichier GPX (sur [cette carte](http://umap.openstreetmap.fr/fr/map/trail-de-la-vallee-des-lacs-gerardmer-juin-2015_44774), cliquez sur « légende » puis « visualiser les données »).




