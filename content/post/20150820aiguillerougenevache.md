---
title: "L'Aiguille Rouge, Névache"
date: 2015-08-20
author: "Christophe Masutti"
image: "/images/postimages/hikebike.png"
tags: ["Sport", "Trail", "Entraînement", "Parcours"]
description: "Améliorer sa vitesse ascensionnelle"
categories:
- Sport
---


Les Alpes, c'est vaste. Si vous cherchez un endroit propice à la randonnée en famille et particulièrement bien situé, je conseille la petite [vallée de la Clarée](http://www.nevache-tourisme.fr/fr/vallee-de-la-claree/) à quelques kilomètres de Briançon, juste à côté de la frontière italienne. Le petit village de Névache est très accueillant et s'organise l'été autour des activités pédestres, lieu de rendez-vous de nombreux randonneurs. On peut notamment saluer le système des navettes qui permettent de ne plus se soucier de sa voiture et emmènent les randonneurs au bout de la vallée, de manière à amorcer de multiples circuits aux paysages admirables.



## Entraînement à la vitesse ascensionnelle

Pour le trail, la vallée offre beaucoup d'opportunités et les moyens de se créer des boucles sympathiques avec plusieurs pics, crêtes et lacs facilement accessibles. Le conseil est évidemment de partir de très bonne heure le matin. Inutile de préciser que le soleil a tendance à plomber en journée et que les variantes du GR 5 autour de la vallée se peuplent relativement vite à partir de 9h00.

Pour la quatrième fois que je séjourne dans cette vallée, ce fut une première pour l'entraînement au trail, habitué que je suis aux chemins des Vosges. Alternant entre randonnée et repos estival, je me suis réservé 2 à 2,5 heures de course un jour sur deux, au départ du hameau de Roubion, commune de Névache. Un rythme tout à fait ronflant mais néanmoins régulier qui m'a permit de travailler sérieusement ma technique de montée sans me fatiguer outre mesure (c'est les vacances, après tout).

Le circuit de l'Aiguille Rouge, que je présente ci-dessous, est à la fois touristique et technique. Premièrement, le fait de monter sur une aiguille n'est selon moi pas vraiment intéressant du point de vue de l'entraînement au trail&nbsp;: le paysage est magnifique mais les derniers mètres de dénivelés en montée comme en descente se font avant tout dans le calme en faisant bien attention où l'on pose ses pieds. Néanmoins, on cumule ainsi quelques 1000 m D+ en un minimum de kilomètres, là où, dans les Vosges, il faudrait systématiquement allonger le parcours d'au moins 5 kilomètres pour obtenir le même dénivelé positif. C'est l'avantage de la haute montagne ; par ailleurs, n'importe quel parcours peut se faire sur des très jolis sentiers en évitant les larges et longs chemins forestiers parfois interminables des Vosges. Deuxièmement, ce parcours concentre en un seul coup tous les paysages que l'on trouve dans le briançonnais&nbsp;: si vous n'êtes que rapidement de passage dans le coin, sachez que ce circuit peut se faire en famille pour une randonnée de niveau facile (le sommet de l'Aiguille Rouge est accessible pour des enfants à partir de 12 ans, mais attention à la fréquentation&nbsp;: il peut y avoir beaucoup de monde en journée).

## Le parcours



Tout le parcours peut se faire sans carte&nbsp;: les chemins sont très bien indiqués (c'est le cas partout dans la région) et la navigation peut se faire à vue dans ce cas précis. Si, à un moment donné, vous apercevez des pancartes écrites en italien, c'est que vous êtes descendu du mauvais côté !

Au départ du hameau de Roubion, il faut emprunter le GR en direction du col des Thures. On profite ainsi de la fraîcheur (ou de l'abri, en cas de pluie) de la forêt de mélèzes sur toute la combe que l'on remonte à côté du ruisseau (ou torrent, selon la météo !). Cette fraîcheur sera particulièrement appréciable car le chemin monte de manière très régulière, d'abord faiblement puis de plus en plus rapidement sans toutefois offrir de raidillon obligeant à casser le rythme. Il est dès lors possible de moduler sa vitesse pour ne pas avoir à marcher. C'est la partie la plus intéressante du point de vue de l'endurance et, techniquement, elle offre deux à trois petits replats qui permettent de reprendre son souffle.

Arrivé à la fin de cette première partie d'environ 3.5&nbsp;km, beaucoup de sueur a déjà coulé mais on a encore de la réserve pour amorcer la prairie du magnifique vallon des Thures qui s'offre alors, avec la cabane du berger des Thures et sa fontaine. Sur un peu moins de 2&nbsp;km on longe l'Aiguille Rouge à droite pour arriver au lac Chavillon à travers le pré. On ne manquera pas de regarder attentivement à droite, à mi-côte de l'Aiguille Rouge, le chemin qui part du lac, c'est lui qu'il faut alors emprunter pour amorcer la montée de l'Aiguille, sans toutefois le confondre avec quelques traces qui longent la crête, plus en amont&nbsp;: elle mènent au même endroit mais sont beaucoup moins confortables pour le trail.

Arrivé à peu près en amont de la cabane du berger, on rejoint le chemin «&nbsp;normal&nbsp;» qui monte à l'Aiguille en provenance du col de l'Échelle au niveau de la courbe de dénivelé des 2340 m. Il suffit ensuite de poursuivre jusqu'au sommet où seuls les derniers mètres se négocient avec prudence.

En haut, il est temps de stopper sa montre et apprécier le paysage, avec, respectivement&nbsp;:


- Au Nord-nord-Ouest, le majestueux Mont Thabor,
- Au Nord, les trois pointes italiennes Balthazard, Melchior et Gaspard,
- Au Nord-Est, une vue sur les pistes de ski alpin de Bardonecchia
- À l'Est, en bas, le col de l'Échelle,
- Au Sud-Est, la vallée de la Clarée vers Plampinet,
- Au Sud, en bas, le hameau de Roubion.


La descente se fera dans le prolongement vers le col de l'Échelle jusqu'à croiser le GR (une pancarte rustique en bois indique la direction des Granges de la Vallée Étroite, qu'il faut suivre jusqu'au-dessus du Roubion). La descente vers le Roubion se fait par le chemin auparavant emprunté à ceci près que, environ 300 m après avoir franchi le ruisseau, un petit sentier (ouvrez l’œil) vers la droite permet, en quittant le GR, d'aller dans la combe du Roubion pour rejoindre le circuit dit des «&nbsp;balcons de Névache&nbsp;». C'est une variante que l'on peut emprunter jusque Névache Ville Haute (à environ 4&nbsp;km par ce chemin) ou couper à quelques endroits (indiqués) pour rejoindre le hameau de Sallé ou Névache Ville Basse. Le parcours ci-dessous coupe au plus court pour rejoindre le Roubion, ce qui porte ce tracé à 14,5&nbsp;km pour environ 1000 m D+.

![Profil altimétrique](/images/profilaiguillerouge.png)

<iframe src="https://umap.openstreetmap.fr/fr/map/aiguille-rouge_50801?scaleControl=false&amp;miniMap=false&amp;scrollWheelZoom=false&amp;zoomControl=true&amp;allowEdit=false&amp;moreControl=true&amp;datalayersControl=true&amp;onLoadPanel=undefined&amp;captionBar=false" frameborder="0" height="400px" width="100%"></iframe>



![](/images/arnev1)

![](/images/arnev2)

![](/images/arnev3)

![](/images/arnev6)

![](/images/arnev7)






