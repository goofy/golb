
---
title: "Paul Magnette, l'Europe et le CETA"
date: 2016-10-21
author: "Christophe Masutti"
image: "/images/postimages/typewriter.png"
description: "Un discours général et universel à propos du CETA et de ses implications politiques et démocratiques"
tags: ["Politique", "Commerce", "CETA", "Libres propos"]
categories:
- Libres propos
---



J'interviens peu sur ce blog à propos de politiques européennes, alors qu'on pourrait s'y attendre étant donné que j'y suis plongé régulièrement dans le cadre de mes fonctions professionnelles. Je suis néanmoins particulièrement touché par le discours récent de [Paul Magnette](https://fr.wikipedia.org/wiki/Paul_Magnette), le 16 octobre 2016, devant le parlement Wallon.

Dans ce discours (vidéo en bas de ce billet) d'une durée approximative d'une vingtaine de minutes, [Paul Magnette](https://fr.wikipedia.org/wiki/Paul_Magnette) explique de manière claire et précise les raisons pour lesquelles la Wallonie refuse(ra), dans son état actuel, l’accord économique et commercial global (AECG), mieux connu sous son acronyme anglais Comprehensive Economic and Trade Agreement (CETA). Ce dernier, pour être adopté, doit être accepté par toutes les parties prenantes, c'est à dire les 10 provinces canadiennes et les 28 États-membres de l'UE. Or, pour que la Belgique fédérale puisse valider son accord, il faut auparavant que le gouvernement Wallon puisse donner le sien, au même titre que les autres régions Belges (région Flamande et région Bruxelloise).

Et voilà que la Wallonie refuse, ce qui implique l'impossibilité pour la Belgique de donner son accord. Pour en comprendre rapidement le contexte on peut lire [l'article de Pierre Kohler et Servaas Storm](https://www.monde-diplomatique.fr/carnet/2016-10-14-Rejet-du-Ceta-accroc-libre-echange) paru dans le Moinde Diplomatique le 14 octobre 2016. Je ne m'étendrai pas davantage : il est clair que les grands accords commerciaux dont il est question depuis ces dernières années posent des problèmes de souveraineté des États, en particulier du côté européen, et que les modèles d'équilibres sociaux, sanitaires, environnementaux et normatifs de ce côté de l'Atlantique risquent de basculer dans une foire au moins-disant néo-libéral.

Ce n'est pourtant pas dans ce registre (uniquement) qu'il faut écouter le discours de Paul Magnette. Ce spécialiste de la construction européenne (titulaire d'un doctorat en science politique sur ce sujet) est à la fois un expert et un élu. C'est un européen convaincu mais surtout éclairé. Et en tant que tel, il confronte les grands principes de la construction européenne (en particulier tout l'intérêt du [principe de subsidiarité](https://fr.wikipedia.org/wiki/Principe_de_subsidiarit%C3%A9_en_droit_de_l%27Union_europ%C3%A9enne)) avec la contradiction politique des accords des autres pays européens (à tendance centralisatrice) vis-à-vis du CETA. Il cite d'ailleurs en passant les divergences entre les élus allemands et la Cour constitutionnelle allemande, ainsi que les hésitations de fond au sein du gouvernement des Pays-Bas. Et tout ceci sans oublier de souligner avec force et vigueur tout l'intérêt qu'il y a **à écouter la société civile** wallone (mutualités, syndicats, experts, universitaires…) qui s'est sérieusement penchée sur le CETA avec forcerapport, avis, et déclarations. Une approche dont devraient s'inspirer plus d'un élu en France.

En somme, le discours de Paul Magnette est un discours **de démocratie**. Et il est assez rare aujourd'hui d'entendre de tels discours **dans le cadre d'un parlement en plein processus de décision** (c'est-à-dire qu'il ne s'agit pas d'un discours électoral bourré de bonnes intentions). C'est sans doute aussi la raison pour laquelle, afin d'appuyer son discours, Paul Magnette cite deux auteurs, et pas des moindre : Albert Einstein et Emmanuel Kant (à partir de 11'55 dans la vidéo). Car en effet son discours a une double portée : générale, d'abord, puisque le processus d'adoption du CETA ne saurait logiquement pas convenir aux principes de souveraineté des États ; universelle, ensuite, puisque en vertu du principe de l'[impératif catégorique kantien](https://fr.wikipedia.org/wiki/Philosophie_pratique_de_Kant), qui, aux yeux de Paul Magnette, vaut aussi bien pour les individus que pour les États, on ne saurait espérer d'accord harmonieux avec les modalités discutables du CETA et, au-delà, de tous les accords censés définir aujourd'hui des processus de mondialisation économique.

<iframe width="560" height="315" src="https://www.youtube.com/embed/B5GhqxWeqzQ" frameborder="0" allowfullscreen></iframe>

