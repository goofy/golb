---
title: "Pour libérer les sciences"
subtitle: ""
date: 2010-12-30
image: "/images/postimages/say.png"
author: "Christophe Masutti"
tags: ["Libres propos", "Sciences", "Communs", "Droit d'auteur", "Édition"]
description: "Comment libérer au mieux les connaissances scientifiques ?"
categories:
- Libres propos
---


L'objectif de ce texte est de faire valoir l'intérêt d'une diffusion décentralisée et libre des connaissances scientifiques. En partant de l'idée selon laquelle l'information scientifique n'a d'autre but que d'être diffusée au plus grand nombre et sans entraves, je montrerai les limites du système classique de publication à l'ère du format numérique, ainsi que les insuffisances des systèmes d'archives «&nbsp;ouvertes&nbsp;». J'opposerai le principe de la priorité de la diffusion et à l'aide de quelques exemples, j'aborderai la manière dont les licences libres [Creative Commons](http://fr.creativecommons.org/) permettent de sortir de l'impasse du modèle dominant.


![Contrat Creative Commons](http://i.creativecommons.org/l/by-sa/2.0/fr/88x31.png)

«&nbsp;Pour libérer les sciences&nbsp;» by [Christophe Masutti](http://christophe.masutti.name) est mis à disposition selon les termes de la [licence Creative Commons Paternité - Partage des Conditions Initiales à l'Identique 2.0 France](http://creativecommons.org/licenses/by-sa/2.0/fr/).

(Màj) : Ce texte a été écrit en **2010**, et publié sur [le Framablog](https://framablog.org/2010/12/20/pour-liberer-les-sciences-christophe-masutti/). Disponible uniquement en PDF à l'époque, je le reproduis ici en entier.

Christophe Masutti (IRIST -- Université de Strasbourg), *Pour libérer les sciences*, 2010.

Merci à Jean-Bernard Marcon pour sa (re)lecture attentive.

# Introduction


En juillet 2010, l'Organisation européenne pour la recherche nucléaire
fit un [communiqué](http://linuxfr.org/2010/07/18/27142.html)
apparemment surprenant. Déclarant supporter officiellement l'initiative
[Creative Commons](http://creativecommons.org/weblog/entry/22736), elle
annonça que les résultats publiables des recherches menées au LHC (Large
Hadron Collider -- l'accélérateur de particules inauguré en 2008)
seraient diffusés sous les termes des licences libres Creative Commons,
c'est-à-dire[^1] :

En réalité, la surprise était en partie attendue . En effet, le CERN[^2]
fut pour beaucoup dans l'apparition de
l'[Internet](http://fr.wikipedia.org/wiki/Web#Historique) et le rôle
qu'y jouèrent Tim Berners Lee et Robert Cailliau, inventeurs du système
hypertexte en 1989, fut décisif. Le fait de diffuser les résultats du
LHC sous licence libre obéit donc à une certaine logique, celle de la
diffusion et de l'accessibilité de l'information scientifique sous
format numérique. La dématérialisation des publications scientifiques et
leur accessibilité mondiale, pour un coût négligeable grâce à Internet,
permet de se passer des mécanismes de publication par revues
interposées, avec cession exclusive de droit d'auteur, et réputés lents,
coûteux, et centralisés. Dans ce contexte, le CERN a opté pour une
diffusion décentralisée (en définissant a priori les conditions
d'exercice des droits d'exploitation favorisant le partage) tout en
garantissant la paternité et l'intégralité (les droits moraux) des
travaux scientifiques.

Plus récemment encore, en novembre 2010, M. P. Rutter et J. Sellman, de
l'Université d'Harvard, ont publié un
[article](http://opensource.com/education/10/10/uncovering-open-access)
intitulé Uncovering open access [^3], où ils défendent l'idée selon
laquelle le libre accès aux informations scientifiques permet de
resituer le lien entre savoir et bien commun. Pour amorcer leur
argumentaire, l'étude de cas -- désormais classique en histoire de la
génétique -- de la redécouverte des expérimentations de Gregor Mendel
sert à démontrer à quel point les sciences sont assujetties à la
diffusion . Là encore, il s'agit de mettre en perspective la question de
la disponibilité des informations scientifiques à travers les systèmes
centralisés de publications. L'exemple des accords difficiles entre le
Max Planck Institüt et le groupe Springer pour l'accès à quelques 1200
revues est illustratif de la tension permanente entre le coût des
abonnements, les besoins des chercheurs et l'idée que les connaissances
scientifiques devraient être accessibles pour tous.

Dans un monde où la production et la diffusion des connaissances
dépendent essentiellement de l'outil informatique (production et
communication de données dans toutes les disciplines), la maîtrise des
technologies de stockage et du web sont des conditions essentielles pour
garantir le transfert et l'accessibilité. Aujourd'hui, en profitant des
plus récentes avancées technologiques, tout un chacun est capable
d'échanger avec les membres de sa famille et ses amis un grand volume de
données en tout genre via Internet. Il devrait donc logiquement en être
de même pour les travaux scientifiques, pour lesquels l'échange
d'information est d'une importance vitale. Ce n'est toutefois pas le
cas.

# La centralisation

La production, la diffusion et l'accès à nos connaissances scientifiques
sont formalisés par les outils informatiques :

-   la production : nul chercheur ne saurait aujourd'hui travailler sans
    Internet et encore moins sans un ordinateur dans lequel il classe,
    construit et communique ;

-   la diffusion : aujourd'hui, un article scientifique peut rester, de
    sa production à sa lecture finale, dans un circuit numérique sans
    jamais en sortir ;

-   l'accès : nous profitons tous des multiples services qui nous
    permettent, notamment par Internet, d'accéder rapidement à
    l'information, beaucoup plus rapidement qu'il y a à peine vingt ans.
    Le rythme de la recherche s'accélère et la numérisation des
    connaissances n'y est pas pour rien.

Pour rendre cohérent ce système de production et de diffusion,
l'ingénierie informatique a joué un rôle fondamental dans la création
des moyens par lesquels nous ordonnons nos connaissances : le système
hypertexte, l'introduction de la logique booléenne, les outils
d'indexation de données, etc. De même, la réduction du temps de
transmission de l'information, grâce, par exemple, aux nanotechnologies
ou encore au clustering de serveurs, joue un rôle primordial dans
l'efficacité de nos systèmes d'échanges d'informations. Ces innovations
ont donc produit quelque chose de très positif dans la mesure où,
profitant de ces avancées techniques majeures, nous réduisons de plus en
plus les délais de communication des données scientifiques, ce qui
optimise la production de nouvelles connaissances.

La production, la diffusion et l'accès à nos connaissances ont donc
épousé un ordre numérique hautement performant, et l'on pourrait
redouter que la maîtrise de cet ordre soit avant tout une maîtrise des
moyens. Il s'avère que non. Excepté pour les expérimentations
nécessitant, par exemple, l'utilisation de supercalculateurs, la
maîtrise des outils informatiques en général ne préjuge pas du rendement
scientifique, elle ne fait qu'optimiser la communication des résultats.
Les maîtres du nouvel ordre numérique ont donc déployé une stratégie qui
a toujours fait ses preuves : centraliser les données et conditionner
leur accès.

C'est ce qui s'est produit dans le cas d'Internet : alors même
qu'Internet est d'abord un système décentralisé où chacun communique des
données avec tous les autres, la possession des données et le calibrage
de leur accès par les acteurs de l'économie du web a transformé Internet
en un gigantesque Minitel 2.0[^4], c'est à dire une logique d'accès
individuel à des serveurs spécialisés.

Dans le cas des connaissances scientifiques, leur stockage et leurs
conditions d'accès, c'est ce modèle qui fut repris : une optimisation du
potentiel de communication mais un accès restreint aux données. En
effet, les supports de la publication scientifique, que sont notamment
les revues, proposent un service de stockage, payant ou non. Le
problème, c'est qu'en maîtrisant ainsi le stockage et l'accès aux
productions scientifiques, le potentiel technologique censé accélérer la
diffusion et la réception de données est réduit aux contingences de
rendement et aux capacités des services qui centralisent ces
connaissances.

En fait, les revues scientifiques ont toujours fonctionné sur ce mode
centralisé qui avait, au départ, deux objectifs : a) garantir la
fiabilité de la production scientifique et la protéger par le droit
d'auteur et b) regrouper les connaissances pour en assurer la diffusion,
afin que les scientifiques puissent externaliser leur communication et
rendre visibles leurs travaux. Cette visibilité, à son tour, permettait
d'asseoir la renommée des chercheurs, de valoriser et évaluer leurs
recherches au sein d'un champ disciplinaire, et surtout de permettre une
forme de démocratie scientifique où les connaissances peuvent être
discutées et critiquées (même si un peu de sociologie des sciences
montre vite les limites de cette apparente démocratie). Aujourd'hui, la
centralisation que proposait chaque revue, dans chaque discipline, est
devenue d'abord une centralisation de moyens. Les revues se regroupent,
des consortiums naissent et proposent, cette fois, de centraliser
l'accès en plus des données. Or, le coût du support de la production
scientifique est devenu presque nul puisque chaque chercheur est en
mesure de communiquer lui-même sa production au sein de la communauté et
même au monde entier grâce à Internet.

De même, les mécanismes de diffusion, encouragés par la rigidité de
l'évaluation scientifique qui raisonne presque exclusivement en termes
de classement de revues, sont aujourd'hui centralisés et conditionnés
par le savoir-faire des organisations ou conglomérats en termes de
stockage des données de classement et d'indexation. À tel point que ces
acteurs (les regroupement de revues), conditionnent eux-mêmes les outils
d'évaluation des instances publiques (comme la bibliométrie) qui
définissent le *ranking* des chercheurs et des publications elles-mêmes.
Nous avons donc confié aux professionnels de la publication les bases
matérielles qui nous permettent de juger les productions scientifiques,
et, ainsi, de produire en retour d'autres connaissances. La
centralisation s'est alors doublée d'un enjeu de pouvoir, c'est à dire
une stratégie permettant non seulement de décider qui a accès aux
données, mais aussi comment et selon quels critères. Cette stratégie est
devenue protectionniste : alors même que l'informatique et Internet
permettraient de changer de système de diffusion et garantir le libre
accès de chacun à l'information scientifique, le système de
centralisation à créé des monopoles de moyens, et de la
commercialisation des données.

De leur côté, les chercheurs - auteurs n'ont pas récupéré la maîtrise de
la diffusion de leurs propres productions et la centralisation est
devenue de plus en plus incontournable puisque le savoir-faire technique
inhérent au stockage et à la diffusion en masse est possédé par quelques
groupements ou conglomérats. Qu'importe, après tout, si une partie de
ces moyens de diffusion et d'accès appartiennent à des conglomérats
privés comme Google, Springer ou Elsevier ? Les impératifs de rendement
et de rentabilité expliquent-t-ils à eux seuls la recherche de contenus
toujours plus imposants de Google ou faut-il envisager un élan humaniste
encore jamais rencontré dans l'histoire ? Dans quelle mesure le
classement des revues conditionne-t-il en retour la rentabilité de leur
diffusion numérique pour une firme comme Elsevier? Quel est le degré de
neutralité de ces services de diffusion de contenus scientifiques ? Ce
ne sont que quelques exemples frappants. Plus généralement, dans quelle
mesure les parts de marché et les contraintes économiques jouent-elles
un rôle dans la diffusion des connaissances, et, par conséquent, dans
leur accès et leur production ?

Je n'apporterai pas de réponse à cette question, qui nécessiterait une
étude approfondie sur les mécanismes du marché de la diffusion
électronique et des supports. Je me contenterai simplement de souligner
le hiatus entre d'un côté les multiples études sur l'économie de la
connaissance et, d'un autre côté, le manque d'analyse sur
l'appropriation concrète des moyens de diffusion par les agents. Car
l'idée selon laquelle les agents sont tous rationnels masque le fait que
les auteurs comme leurs lecteurs ne maîtrisent pas les conditions de
diffusion et s'en remettent entièrement à des tiers.

Un exemple récent : sur la pression du gouvernement américain,
Amazon.com a cessé d'héberger sur ses serveurs le site Wikileaks, suite
aux révélations de ce dernier sur la face caché de la diplomatie
internationale (Le Monde, 1er décembre 2010). Certes, le service de
diffusion et de vente de livres d'Amazon.com n'a que peu de relation
avec son service d'hébergement de sites, mais dans la mesure où un
gouvernement peut ordonner la fermeture d'un site sur ses serveurs,
comment assurer la neutralité du service de diffusion de livres de cette
même multinationale ? Un gouvernement pourrait-il faire pression sur
Elsevier-Science Direct si l'une des publications diffusées par ce
groupe divulguait des données pouvant être censurées ?

# L'accès gratuit


Pour ce qui concerne les services d'accès gratuit, comme le service
d'archives ouvertes HAL, inscrit dans le cadre de l'Open Archive
Initiative (OAI), l'accès aux productions scientifiques correspond bel
et bien à une volonté de diffusion globale et libérée des contraintes
économiques. Mais il ne s'agit que de l'accès à certaines productions,
sous réserve de l'acceptation préalable des détenteurs des droits de
diffusion que sont les revues, et uniquement si l'auteur accomplit la
démarche.

Si HAL et les principes de l'OAI permettent aussi le dépôt de textes
produits directement par leurs auteurs, et favorise en cela la diffusion
par rapport à la publication, c'est une part importante des
connaissances qui est effectivement en accès gratuit. Néanmoins, comment
comprendre cette différence de traitement ? À qui appartiennent les
connaissances ? À celui qui les produit, qui cède une partie de ses
droits d'auteur (et justement ceux qui conditionnent la diffusion), ou à
celui qui les diffuse et met à portée du public les moyens considérables
auxquels nous accédons aujourd'hui grâce aux circuits numériques ? Si
nous ajoutons à cela le besoin inconsidéré d'être évalué, d'améliorer le
*ranking* des chercheurs et de publier dans les revues cotées,
pouvons-nous raisonnablement croire que le dépôt spontané de la part des
auteurs dans un système d'archives ouvertes rétablisse cette inégalité
de traitement dans l'accès et la diffusion des connaissances ?

Le système de dépôt [ArXiv.org](http://arxiv.org/) reste pourtant un
modèle[^5], car il est inscrit dans une certaine tradition de la
communauté des chercheurs qui le fréquentent et y déposent presque
systématiquement leurs productions. Il a ses limites, tout comme
l'ensemble des initiatives d'accès gratuit aux ressources : aucun n'a de
politique véritablement claire à propos du droit d'auteur.

# Les politiques éditoriales


Le [projet Sherpa](http://www.sherpa.ac.uk) est une source d'information
très utile pour connaître les politiques éditoriales des maisons de
publications. La liste Romeo, en particulier, recense les possibilités
et les conditions de dépôt des productions dans un système d'archives
ouvertes. Selon les revues et les conditions imposées par les éditeurs,
il est possible pour un auteur d'archiver une version *preprint* ou
*postprint* de son article. Un classement des revues par couleur (blanc,
jaune, bleu et vert) signale les niveaux d'autorisation donnés aux
auteurs afin qu'ils puisse disposer de leur travail après avoir
toutefois cédé une partie de leurs droits d'auteur.

Le fait que la majorité des revues scientifiques aient des politiques de
droit d'auteur différentes n'est guère surprenant. Outre les grandes
revues classées dans les premiers rangs mondiaux, on trouve parfois des
centaines de revues spécialisées selon les disciplines, apparaissant (et
parfois disparaissant) selon la vie des communautés de chercheurs. Un
peu d'histoire des sciences nous apprend que lorsqu'une discipline ou un
champ d'étude apparaît, le fait que la communauté puisse disposer d'un
espace de publication propre marque souvent les débuts d'une forme
d'institutionnalisation de ce champ d'étude et, donc, une forme de
reconnaissance par le reste de la communauté scientifique. La longévité
et la fréquence de parution des revues peuvent être considérées comme
des indicateurs de croissance de ce champ d'étude au cours de son
histoire. Par conséquent, la multiplicité des revues, dans la mesure où
se trouvent des groupes de chercheurs assez motivés pour les maintenir
en termes de moyens techniques, humains et financiers, est plutôt un
élément positif, signe d'une bonne santé de l'activité scientifique, et
une forme de garantie démocratique de l'accès aux connaissances. Si
toutes ces revues ont des politiques de droit d'auteur différentes c'est
aussi parce qu'elles ont des moyens divers de subsistance, et, bien
souvent, la seule vente des exemplaires papier ne suffit pas à couvrir
les frais de publication.

Un autre élément qui conditionne en partie les politiques éditoriales,
c'est le flux des publications scientifiques. Paradoxalement, le nombre
actuel des revues ne saurait suffire à absorber les productions
scientifiques toujours plus nombreuses[^6]. Outre les phénomènes de
compétitions entre pays, ce nombre est principalement dû aux politiques
de recherche et à la culture de l'évaluation des chercheurs selon
laquelle le nombre de publications est devenu un indicateur de qualité
de la recherche, faute d'avoir des évaluateurs capables de (et autorisé
à) juger précisément de la pertinence et de la valeur scientifique du
contenu des publications. Or, ces politiques d'évaluation de la
recherche impliquent pour les chercheurs la nécessité de publier dans
des revues dont le classement, établi par ces mêmes évaluateurs, préjuge
de la qualité scientifique du contenu.

Toutes les revues ne sont pas classées, à commencer par les petites
revues connues dans les champs d'étude émergents et qui sont bien
souvent les principaux supports de communication des recherches les plus
novatrices et des nouvelles niches intellectuellement stimulantes.
Finalement, les revues classées, et surtout celles qui disposent d'un
classement élevé, ne peuvent absorber tout le flux des productions.
Cette pression entre le flux et les capacités concrètes de publication
implique une sélection drastique, par l'expertise (les *reviewers* ou
*referees*), des articles acceptés à la publication. Comme cette
sélection est la garantie a priori de la qualité, le classement se
maintient alors en l'état. On comprend mieux, dès lors, pourquoi ces
revues tiennent non seulement à ce que les auteurs leur cèdent leurs
droits pour exploiter les contenus, mais aussi à ce que ces contenus
soient le moins visibles ailleurs que dans leur propre système de
publication et d'archivage. Nous revenons au problème de la
centralisation.

# Propriété et pénurie


Si nous mettons en perspective la croissance des publications
scientifiques, le besoin d'évaluation, la nécessité (individuelle de la
part des chercheurs, ou concurrentielle au niveau des universités et des
pays) du *ranking* qui privilégie quelques revues identifiées au
détriment des plus petites et discrètes, et l'appropriation des moyens
de diffusion par les conglomérats du marché scientifique, nous assistons
à l'organisation d'une pénurie maîtrisée de l'information scientifique.

J'évacue aussitôt un malentendu. Cette pénurie est maîtrisée dans le
sens où l'information scientifique est en général toujours accessible,
mais ce sont les conditions de cette accessibilité qui sont discutables.

Prenons un cas concret. [Jstor](http://www.jstor.org/) (Journal Storage)
est une organisation américaine à but non lucratif, fondée en 1995, dans
le but de numériser et d'archiver les revues académiques. Créé pour
permettre aux Universités de faire face à l'augmentation des revues,
Jstor sous-traite l'accessibilité et le stockage de ces revues, assurant
ainsi un rôle de gardien de la mémoire documentaire scientifique. Le
coût de l'abonnement à Jstor est variable et ne figure certainement pas
parmi les plus chers. En revanche, dans le cadre de l'archivage et des
conditions d'accessibilité, des accords doivent se passer entre les
revues détentrices des droits de publication et Jstor. Ainsi, la
disponibilité des revues est soumise à une barrière mobile (*moving
wall*) qui détermine un délai entre le numéro en cours de la revue et le
premier numéro accessible en ligne. Quel que soit ce délai, un article
scientifique devra toujours être payé : soit en achetant la revue au
format papier, soit en achetant l'article au format électronique sur le
site de la revue en question, soit en achetant un abonnement auprès de
Jstor dans le cas où le numéro de la revue en question y est accessible.

Dans d'autres cas de figure, les articles scientifiques peuvent être
trouvés et vendus au format numérique sur plusieurs espaces à la fois :
sur le site de la revue, sur un site de rediffusion numérique (comme par
exemple le service Cat.Inist du CNRS), ou en passant par le service
d'abonnement d'une institution (Jstor, Elsevier...).

Jamais auparavant on n'avait assisté à une telle redondance dans l'offre
de publication du marché scientifique. En conséquence, surtout avec
l'arrivée du service de vente d'articles à l'unité, jamais le marché de
la publication scientifique n'a obtenu un tel chiffre d'affaire. Est-ce
synonyme d'abondance ? Pas vraiment. L'inégalité de traitement entre les
revues est toujours un obstacle qui prive les nombreuses petites revues
(qui peuvent toutefois être célèbres mais dont le marché n'a bien
souvent qu'une dimension nationale) de participer à l'offre. Dans la
plupart des cas, les articles scientifiques publiés dans ces conditions
sont donc archivés d'une manière ou d'une autre, mais sur un marché
séparé, parfois sur le site internet propre à la revue, parfois sur les
serveurs d'un regroupement non lucratif de revues, géré la plupart du
temps par des institutions publiques, comme par exemple
[Revues.org](http://www.revues.org/).

Par ailleurs, un autre obstacle est préoccupant : le temps de latence
variable d'une revue à l'autre entre la publication papier et l'accès
aux versions numériques. Ce temps de latence est dû à deux écueils qu'il
me faut maintenant longuement développer.

Le premier est la conception rigide que l'on a du format numérique, à
savoir que la version numérique d'un document est considérée comme une
copie de la version papier. Ce n'est pas le cas. Fort heureusement, la
plupart des maisons d'éditions l'ont compris : un article peut être mis
sous format HTML, avec un rendu dynamique des liens et de la
bibliographie, par exemple, ce qui lui apporte une dimension
supplémentaire par rapport à la version papier. Or, c'est cette
conception du document-copie numérique qui prime, par exemple, dans le
cas de Jstor, ou encore dans celui de certains projets de numérisation
de la BNF, car l'objectif est d'abord de stocker, centraliser et
d'ouvrir l'accès. Certes, la numérisation de fonds anciens ne peut
transformer les articles en pages dynamiques (quoique les récentes
avancées dans le domaine de la numérisation de fonds tendrait à montrer
le contraire). En revanche pour les numéros plus récents, qui de de
toute façon ont été rédigés de manière électronique par leurs auteurs,
la barrière mobile implique bien souvent que pendant quelques années un
article ne sera disponible qu'au format papier alors que rien ne l'y
contraint techniquement. Cela représente une perte considérable dans
notre monde numérique ! Dans le cas d'une revue bi-annuelle à faible
tirage, il devient très difficile de se procurer un numéro un ou deux
ans après sa parution (phénomène qui pourrait être mieux contrôlé avec
les systèmes d'impression à la demande dont il sera question plus tard).
Et il faut attendre sa mise à disposition sur Internet (gratuitement ou
non), pour que cet article touche enfin le potentiel immense du nombre
de lecteurs à travers le monde...à ceci près qu'il y a toujours un temps
de latence et qu'en moins de deux ans, un article peut voir très vite
son intérêt scientifique diminuer, et avec lui l'intérêt de la mise en
ligne, si ce n'est uniquement pour son archivage. Ce temps de latence,
cette barrière mobile , peut très facilement disparaître pour peu que
l'on s'interroge sur le réel intérêt de la cession des droits d'auteur
(de diffusion) des articles scientifiques.

C'est ce qui m'amène au second écueil : la question des droits d'auteur
et de diffusion. Pour qu'une revue papier soit rentable, ou du moins
qu'elle résiste à la pression entre l'investissement et les frais de
fonctionnement, il faut qu'elle puisse vendre un certain nombre de
copies. Pour que cette vente puisse se faire, tout le monde part du
principe que les auteurs doivent céder une partie de leurs droits à
l'éditeur (une cession exclusive). Avec l'apparition des licences libres
de type [Creative Commons](http://fr.creativecommons.org/), nous verrons
que ce n'est nullement là une condition nécessaire. L'autre aspect de
cette cession de droit est que l'auteur ne peut plus disposer de son
travail (son œuvre) comme il l'entend. La diffusion de cette œuvre est
donc soumise à la politique éditoriale de la revue qui ne s'engage pas
obligatoirement à en garantir l'accessibilité numérique. En revanche, la
cession des droits permet aux revues de réaliser une plus-value
supplémentaire dans le cadre de la diffusion numérique, par exemple en
passant des accords avec un grand distributeur de revues électroniques.
Là encore, le biais est à redouter dans la mesure où les abonnements ont
un coût bien souvent prohibitif pour les institutions qui les payent, ce
qui fait souvent l'objet de discussions serrées.

Avec l'émergence des premiers périodiques électroniques, les éditeurs
ont massivement investi dans l'économie numérique, en répercutant ces
coûts sur les abonnements. Pour donner un exemple, le prix [annoncé par l'Université de Poitiers](http://scdlre.edel.univ-poitiers.fr/document.php?id=375) pour
un abonnement à Elsevier-Science Direct est de 37556 euros pour l'année
2007, un coût apparemment raisonable mais qui ne cesse d'augmenter et
doit être multiplié par le nombre d'abonnements différents d'une même
université[^7]. Pour pallier ces frais toujours croissants les
institutions se sont organisées en consortia, de manière à mutualiser
ces coûts. Mais cela ne vaut que pour les groupements capables de faire
face aux éditeurs. En effet, certains pays se heurtent à une réduction
des fonds publics dédiés à la recherche, et d'autres pays, comme ceux en
voie de développement, n'ont bien souvent pas les moyens de payer ces
abonnements. En fait, il y a là encore une différence de traitement dans
l'accès à l'information scientifique, et sans doute la plus révoltante :
dans la mesure où l'accès aux ressources numériques représente bien
davantage, d'un point de vue technique et de traitement de
l'information, qu'un simple accès à un abonnement papier , comment
peut-on concevoir qu'une partie loin d'être négligeable des informations
scientifiques sous forme électronique puisse ne pas être accessible à
certaines parties du monde ? Et au sein d'un même pays, comment accepter
qu'il puisse exister une inégalité d'accès entre les différentes
institutions, entre celles qui ont les moyens financiers suffisants et
les autres, ou entre celles qui, faisant partie de tel consortia, n'ont
pas accès aux mêmes revues que les autres ?

Certes, on me rétorquera que les abonnements aux revues papier ont
toujours été chers eux aussi, de même que le stockage de ces revues. Ce
à quoi je réponds :

1.  le format numérique ne coûte rien à la production (les revues
    externalisent les coûts de mise en page chez les auteurs eux-mêmes,
    et ont tendance à ne jamais payer les auteurs ni les membres des
    comités de sélection des articles), c'est le stockage et la gestion
    sur les serveurs privés d'un conglomérat qui représente un coût,

2.  ce stockage en un seul endroit dont l'accès est payant n'apporte
    rien de plus à la qualité de l'information scientifique,

3.  classement et ordonnancement de l'information scientifique dépendent
    du distributeur, et ne sont donc pas neutres scientifiquement
    (certaines revues disparaissent ou apparaissent dans les catalogues
    suivant les transactions ou les intérêts du moment),

4.  les productions scientifiques devraient donc circuler librement dans
    les communautés scientifiques sans dépendre de services tiers, du
    moins non publics.

Il ressort de tout cela que l'accès à l'information scientifique souffre
gravement d'un manque d'efficacité. Les mouvements du type archives
ouvertes se contentent finalement de transformer l'information en
archive, justement, c'est à dire la forme la moins exploitable de
l'information scientifique. En effet, dans la mesure où l'accessibilité
à l'information sous forme numérique en temps et en heure dépend d'un
marché de diffusion fermé, dont sont exclus des pans entiers de la
connaissance (les revues papier et parfois numériques mais dotées de peu
de moyens ou n'appartenant pas à un conglomérat), on traite l'article
scientifique au format numérique comme une copie de sa version validée ,
parfois plusieurs années après, au titre d'archive de la connaissance.
En somme, on fait de la mémoire documentaire, au lieu d'assurer la
diffusion des connaissances au moment où elles se créent. Or, si un
groupe comme Elsevier Science Direct regroupe environ 2000 périodiques,
comment est assurée la diffusion des autres revues ? Nous avons vu leur
inégalité de traitement. La barrière mobile séparant, pour un article,
sa publication de son archivage, crée donc une grande inégalité entre
les articles pouvant être diffusés par les revues cotées ou appartenant
à des groupements de diffusion, et ceux des revues moins cotées, sachant
que la rareté induite par les mécanismes de classement (on ne peut
classer toutes les revues) est aussi une cause de cette inégalité de
traitement et de fermeture du marché. Ma conclusion, pour cette partie,
tient en une seule affirmation : il faut rendre la priorité à la
diffusion sur la publication. Mais sous quelles conditions ?

# Principe de priorité de la diffusion


Je voudrais opposer à ce modèle centralisé, privatif et inégalitaire le
principe de priorité de la diffusion, c'est-à-dire le fait de diffuser
la connaissance avant que de la publier par le moyen des revues, des
livres ou des publications numériques nécessitant une obligation
d'abonnement. En somme, diffuser avant de vendre.

Ce n'est pas une nouveauté. Ce principe est déjà agréé par tous les
chercheurs. En effet, toute production scientifique a pour but premier
d'être diffusée. Avant même sa publication, un article est diffusé à
l'intérieur du réseau de la communauté de chercheurs à laquelle
appartient l'auteur : ses collègues qui l'aident à rédiger et apportent
leurs avis, le laboratoire ou institut auquel il appartient et auquel il
demande éventuellement l'autorisation pour pouvoir publier, les membres
du comité d'évaluation de la revue qui sont censés appartenir à sa
communauté (sans quoi ils ne pourraient juger de la pertinence
scientifique de l'article), et enfin, la plupart du temps, les
spécialistes de son domaine d'étude à qui il a spontanément envoyé ses
travaux ou qui le lui ont demandé[^8]. Il s'avère pourtant que ces
pratiques vont à l'encontre du contrat de cession de droits que l'auteur
a conclu avec l'éditeur.

Il peut aussi arriver, dans certains cas, que la revue ne propose même
pas de contrat de cession exclusive, et que la diffusion par l'auteur
lui-même est considérée comme un manque de fair-play de sa part,
produisant ainsi un manque à gagner pour la revue souvent elle-même dans
un équilibre financier précaire. Dans ce cas bien particulier, courant
dans le domaine des sciences humaines, et pour de petites revues
communautaires , il importe alors de se demander quelle peut bien être
l'utilité d'une revue qui n'a, ainsi, aucun autre but que de centraliser
l'information scientifique, organiser sa faible diffusion, et confirmer
la pertinence scientifique d'un travail déjà diffusé dans une partie de
la communauté de spécialistes. En fait, l'intérêt est évident : il
s'agit d'ajouter une ligne à la liste des publications d'un chercheur et
donc de son institut ou laboratoire, même si la revue en question n'est
pas classée. La comptabilité du nombre de publications est tenue à des
fins d'évaluation et démontre la productivité en termes quantitatifs.
Les petites revues qui émergent donc des champs scientifiques ont pour
premier rôle de participer à l'amortissement du flux croissant de
publications scientifiques. Toutefois, je resterai prudent : cela
n'altère en rien leur qualité scientifique, bien au contraire, puisque
la plupart du temps elles rassemblent la production scientifique d'une
communauté établie et qui valide les recherches menées selon le principe
de l'évaluation par les pairs.

La vraie raison d'être de certaines revues serait-elle donc l'évaluation
et non la diffusion ? C'est un aspect qu'il faut prendre en compte. Les
revues scientifiques sont spécialisées et s'adressent toujours à une
communauté de chercheurs bien identifiée. La meilleure preuve est qu'il
appartient aux revues de vulgarisation scientifique, à grand tirage,
d'effectuer un travail de veille et de reformulation afin de rendre
accessibles les informations scientifiques importantes à un public de
non spécialistes, qu'il soit grand public ou un public composé de
spécialistes d'autres disciplines. À l'exception des revues
scientifiques généralistes avec un haut niveau d'évaluation (une
cohortes d'experts de disciplines différentes) comme *Nature* ou
*Science*, par exemple, toutes les revues, qu'elles aient un *ranking*
élevé ou non, s'adressent à la communauté de chercheurs qui les font
exister. Elles sont en quelque sorte les vitrines de ces communautés et
en montrent le dynamisme. Publier dans ces revues n'a donc pour autre
objectif que d'être un acte hautement individuel visant à optimiser son
évaluation par les instances et les pairs. Par conséquent, l'évaluation
et la centralisation sont les causes premières de l'existence des
revues, qui entrent d'ailleurs en concurrence (économique et
intellectuelle) lorsqu'elles appartiennent à un même champ scientifique.

Nous avons vu que la centralisation est néfaste pour la diffusion. Nous
avons vu aussi que lorsqu'un article arrive à publication, c'est qu'il a
été validé et diffusé auparavant dans la communauté ou du moins dans une
partie significative de cette communauté, au regard du niveau de
spécialisation des travaux en question. Donc, dans les *pratiques* des
chercheurs, au jour le jour, c'est la diffusion qui prime sur la
publication, et, dans la mesure où le but d'une information scientifique
est d'être diffusée, la publication devrait être considérée à sa juste
place : un acte accessoire motivé par d'autres raisons que l'avancement
des sciences.

# Trois exemples


Plusieurs revues et institutions ont choisi d'opter pour le principe de
la priorité de la diffusion de manière *formelle*. Ce type de choix est
de plus en plus courant et alterne entre l'archivage avec accès gratuit
(généralement appelé archives ouvertes ) et l'adoption de licences
libres de type Creative Commons. Les trois exemples suivants me
permettront d'illustrer la différence entre ces deux possibilités.

Un premier exemple concerne la revue *[Medical History](http://www.ucl.ac.uk/histmed/publications/med_hist)*. Cette
revue, supportée par le [Wellcome Centre for History of Medicine](http://www.ucl.ac.uk/histmed/publications/med_hist)
(University College London), est publiée de manière classique depuis
1957. Il y a quelques années, le comité éditorial, en phase avec la
[politique du Wellcome Trust](http://www.wellcome.ac.uk/About-us/Publications/Reports/Biomedical-science/WTD003181.htm)
en faveur de l'*open access*, a décidé de porter la revue en ligne (y
compris ses archives) avec un accès gratuit, l'hébergement étant assuré
par [PubMed Central](http://www.ncbi.nlm.nih.gov/sites/entrez?db=pmc)
(PMC), le service d'archives gratuites de la Bibliothèque nationale de
médecine aux États-Unis[^9]. Effort louable dans la mesure où les
abonnements, loin de s'amenuiser comme on aurait pu le craindre, ont au
contraire augmenté suite à la visibilité nouvelle de cette excellente
revue, se déclarant par la même occasion comme une référence
incontournable du champ de l'histoire de la médecine.

*Medical History* a donc privilégié la diffusion sur la publication. Les
articles sitôt évalués, acceptés et publiés sont accessibles facilement
et gratuitement pour l'ensemble de la communauté des chercheurs. Les
droits d'auteurs, eux, sont soumis à la [politique de
copyright](http://www.ncbi.nlm.nih.gov/pmc/about/copyright.html) de
PubMed Central, sous la juridiction du gouvernement américain ou des
pays étrangers d'où sont issus les articles. Ainsi, le copyright d'un
article dans *Medical History* est toujours celui de l'auteur de
l'article, protégé par le droit national.

Le principal biais de cette configuration est que l'auteur, s'il est
effectivement rassuré sur la conservation de ses droits, n'est pas en
mesure de décider a priori de la manière dont peut être utilisé son
article.

Par exemple, un utilisateur de l'article ne peut lui-même partager
l'article sans demander d'abord l'autorisation à l'ayant droit.
Qu'arrive-t-il à la mort de l'auteur ? C'est un véritable parcours du
combattant que de retrouver alors les ayants droits de l'œuvre. Et, bien
entendu, cette question ne touche pas seulement les publications
scientifiques, mais toutes sortes de productions soumises au droit
d'auteur. Si *Medical History* a pu changer sa politique éditoriale sans
se soucier de ce problème pour les articles les plus anciens, c'est
parce que les auteurs on procédé à une cession de droits à l'époque où
ils ont écrit l'article. Du moins, je l'espère\... Sinon, les ayants
droit actuels peuvent retirer l'œuvre de la collection d'archive.

Sans être aussi pessimiste pour l'auteur, projetons-nous dans un futur
proche et imaginons un instant qu'un riche mécène féru d'histoire de la
médecine dispose des moyens techniques pour transformer l'ensemble de la
collection de *Medical History* au format e-book, à destination des
chercheurs souhaitant en disposer sur leurs liseuses électroniques, et
même éventuellement en proposant de payer pour le service rendu.
Pourquoi devrait-il attendre une quelconque autorisation des ayants
droit ? Là encore on peut se demander à qui appartient la connaissance
scientifique : n'est-elle pas destinée d'abord à être diffusée ?

En fait, il en va de l'intérêt commun que de spécifier dès le départ les
conditions sous lesquelles l'œuvre peut être diffusée, partagée et même
vendue, c'est en cela que les licences Creative Commons sont une
amélioration du principe du droit d'auteur.

Un second exemple, le cas des publications du LHC, déjà mentionné dans
l'introduction, nous montre un autre point de vue sur les limites de la
notion de propriété. En effet, ce qui motive essentiellement un tel
choix, c'est avant tout une forme d'injustice. Un texte est produit et
validé par la communauté des chercheurs. Pourquoi l'éditeur, détenant
les droits d'auteurs nécessaires, devrait-il le revendre au prix fort,
sans que jamais (sauf pour de rares exceptions) les retombées
économiques soient profitables à la communauté ? Au contraire, elle est
amenée à payer pour accéder aux informations qu'elle a elle-même
produites.

Je suis caricatural. Dans plusieurs cas de figure, bien entendu,
l'éditeur fait un véritable travail éditorial, consistant à corriger le
texte et le mettre en page...quoique les corrections proposées le sont
en fait bien souvent par un relecteur appartenant à la communauté de
chercheurs, et rarement payé pour le faire (seule sa renommée y gagne).
Si bien que, dans le cas fort compréhensible où les lecteurs
préféreraient une version papier de la revue, il serait normal d'en
payer les frais d'impression et de tirage, c'est à dire un coût bien
moindre que celui proposé pour des abonnements numériques, surtout
lorsque les fournisseurs de services ne sont pas eux-mêmes les éditeurs,
mais de simple revendeurs exerçant un monopole sur le stockage des
informations.

Certes, ce ne sont pas les raisons principales du soutien actif du CERN
en faveur des licences Creative Commons, mais ces arguments sont bel et
bien présents et jouent un rôle décisif dans la volonté de choisir un
système de diffusion plus juste.

Un troisième exemple me permet de montrer de quelle manière le choix des
licences libres peut être assumé par une revue scientifique. *[Non
Linear Processes in
Geophysics](http://www.nonlin-processes-geophys.net/volumes_and_issues.html)*,
sous titrée *An open access Journal of the European Geosciences Union* a
décidé de placer chaque article sous licence
[CC-BY](http://creativecommons.org/licenses/by/3.0/) (Creative Commons
-- Paternité), c'est-à-dire que vous pouvez :

-   reproduire, distribuer et communiquer cette création au public,

-   modifier cette création (dans notre contexte : l'auteur ou
    l'utilisateur peuvent améliorer l'article et en corriger
    éventuellement certains aspects après sa publication),

-   à condition de citer le nom de l'auteur original de la manière
    indiquée par l'auteur de l'œuvre ou le titulaire des droits qui vous
    confère cette autorisation (mais pas d'une manière qui suggèrerait
    qu'ils vous soutiennent ou approuvent votre utilisation de l'œuvre).

En d'autres termes, vous pourriez aussi vendre l'œuvre. Choquant ? Pas
tant que cela. Je fais encore appel à vos capacités imaginatives.
Imaginons qu'en l'honneur d'un chercheur décédé (décidément, mes
exemples n'ont rien de réjouissant!), ses collègues désirent compiler
toutes ses œuvres dans un volume publié et vendu à l'occasion du dixième
anniversaire de sa mort. Et bien, dans le cas de la licence CC-BY, ils
le pourraient sans avoir à demander d'autorisation spécifique.

Les licences Creative Commons favorisent la création là où le droit
d'auteur sous sa forme classique a tendance à la freiner voire
l'annihiler. Cependant, certains pourront soulever aux moins deux
arguments à l'encontre de cet exemple.

*Premier argument* : le droit d'auteur (le copyright -- je rappelle plus
loin la différence entre les deux) est d'abord l'expression d'une
propriété. Pourquoi la veuve de notre camarade ne pourrait-elle pas
bénéficier des royalties générées par la vente de l'ouvrage dont nous
venons de parler, ou même s'opposer à sa publication ? Il y a plusieurs
réponses :

-   La première est que nous parlons de science[^10]. Comme nous l'avons
    vu, nous devons avant tout favoriser la diffusion de l'information
    scientifique qui ne saurait être la propriété de qui que ce soit (la
    méthode de résolution d'une équation mathématique peut-elle être la
    propriété de quelqu'un ? C'est un vaste débat qui nous mène tout
    droit à la question de la brevetabilité et de la propriété
    intellectuelle, mais ce n'est pas notre propos ici).

-   Deuxièmement, rien n'empêche les diffuseurs et vendeurs de l'ouvrage
    de reverser des royalties à la veuve (même si elle n'est pas
    l'auteur et que ce n'est pas son travail, elle peut contractualiser,
    par exemple, la mise à disposition de manuscrits appartenant à
    l'auteur). De plus, elle peut elle-même diffuser et vendre à son
    tour.

-   Troisièmement, l'auteur lui-même aurait pu, de son vivant, utiliser
    une licence n'autorisant pas la vente de ses œuvres (dans le cas des
    Creative Commons, il s'agirait de la licence CC-By-NC).

*Second argument* : si tout le monde peut diffuser et modifier un
article scientifique tiré de la revue *Non Linear Processes in
Geophysics*, le risque serait grand de nuire à l'intégrité ou à la
moralité de l'auteur, voire de distribuer des versions de l'article
néfastes à l'avancement scientifique. Là encore, il y a plusieurs
réponses :

-   les licences Creative Commons permettent à l'auteur de conserver
    quoi qu'il arrive ses droit moraux, notamment le droit de paternité
    et le droit au respect de l'intégrité de l'œuvre (c'est pourquoi les
    modifications sont toujours proposées à l'auteur et non imposées à
    l'oeuvre, au risque de contrevenir au droit d'intégrité).

-   L'obligation de citer l'auteur implique de citer aussi la source de
    l'article modifié. Étant donné que la communauté scientifique se
    reportera systématiquement à cette source (nous sommes toujours dans
    le cadre d'une revue clairement identifiée), la diffusion d'un
    article falsifié sera marginale.

-   La possibilité de modifier l'article s'adresse en premier lieu à
    l'auteur lui-même ! En cédant ses droits de diffusion, toute
    modification qu'il peut faire dans son article après une première
    publication est vouée au silence. En utilisant une licence Creative
    Commons, il peut non seulement modifier et améliorer son article,
    mais aussi le diffuser ; et la revue peut très bien intégrer un
    système de mises à jour. C'est là tout l'avantage de la diffusion
    numérique des articles scientifiques, et nous verrons plus loin
    comment le système d'impression à la demande peut aussi y trouver sa
    justification.

# Droit d'auteur


Un obstacle a donc été identifié : c'est au nom des droits d'auteur que
l'on centralise et handicape la diffusion des œuvres scientifiques. Il
semblerait bien que la pratique de cession de droits d'auteur et
l'emploi du copyright (même à titre de mise à disposition gratuite des
travaux scientifiques) soient compris en premier lieu et *exclusivement*
comme des moyens d'autoriser l'exploitation de la propriété de l'auteur.
Dans tous les cas de figure, comme les droits d'auteur ne nécessitent
aucune démarche particulière pour être automatiquement attribués à
l'auteur, c'est dans ce cadre juridique que sont exploitées les œuvres
scientifiques, mais la plupart du temps sans que soit proposé à l'auteur
de déroger aux pratiques (injustes) en vigueur.

Qu'est-ce que le droit d'auteur ? Il s'agit des droits exclusifs dont
dispose l'auteur sur ses œuvres de l'esprit originales. Ces droits sont
de deux sortes :

-   le droit moral, qui protège la paternité de l'œuvre et au nom duquel
    l'intégrité de l'œuvre est garantie,

-   et le droit patrimonial qui donne a priori l'exclusivité de
    l'exploitation (économique) de l'œuvre à l'auteur et qui varie en
    durée selon les législations nationales.

Dans certains pays qui appliquent une législation jurisprudentielle,
comme aux États-Unis, le droit d'auteur est nommé copyright.
Généralement, le copyright accentue l'importance du droit de propriété
par rapport au droit moral, ce qui explique l'apparente synonymie entre
le droit d'auteur et la propriété intellectuelle. Cette dernière
regroupe le droit d'auteur (œuvres de l'esprit), mais aussi la propriété
industrielle (y compris les brevets et les marques).

Un chercheur de la Stanford Law School, Mark Lemley décrit bien cela
dans un article de 2005[^11]. En faisant une étude sur le nombre
d'occurrences trouvées dans les textes de lois américains, Lemley
démontre que l'expression de propriété intellectuelle a peu à peu
remplacé les expressions de droit d'auteur et droit des brevets. Pour
lui, cette distorsion tient à deux choses : la première est la création
en 1967 de l'Organisation mondiale de la propriété intellectuelle (OMPI)
qui représente en une seule institution les intérêts des ayants droit
dans les trois domaines du droit d'auteur, des brevets et des marques
déposées. La seconde raison, c'est que parler de propriété
intellectuelle permet d'unifier deux domaines disciplinaires traitant
des droits exclusifs à l'information immatérielle. En d'autres termes,
si le droit des brevets vise à encourager la publication d'idées et à
imposer une limitation de monopole sur ces idées, le droit d'auteur peut
suivre la même voie et imposer la restriction de l'information au nom de
la défense de la paternité de l'œuvre ou de son intégrité. Autrement
dit, le droit d'auteur peut être amené à servir d'autres intérêts que
ceux pour lesquels il a été créé : freiner, voire empêcher la diffusion
des idées reconnues d'abord comme propriété exclusive.

C'est dans ce cas de figure que les licences libres s'opposent à l'idée
de propriété intellectuelle , notamment dans le cas des logiciels
libres. Concernant ces derniers, les licences libres proposent de
défendre le droit d'auteur pour les logiciels envisagés comme des
productions de l'esprit, mais en encadrant la diffusion et l'utilisation
d'un logiciel libre de trois manières :

-   positive (vous avez le droit de diffuser et modifier tant que vous
    respectez la paternité de l'oeuvre),

-   durable (il faut garder la même licence dans toutes les versions
    modifiées),

-   et profitable (tout le monde peut utiliser le code et produire à son
    tour du logiciel libre).

Tout cela s'oppose bien sûr à la logique des brevets, mais permet aussi
d'encourager la créativité tout en proposant un modèle économique plus
juste. Dans son livre *Internet et Création*[^12], Philippe Aigrain
s'inspire du modèle du libre dans l'économie de l'immatériel et démontre
l'intérêt d'une licence globale pour favoriser le partage des œuvres, du
moins sous format numérique.

Pour revenir à la question des droits d'auteur dans le contexte de la
diffusion de travaux scientifiques, il s'avère que la notion de
propriété est devenue la seule référence dans le cadre de leur
exploitation, numérique ou non. Deux solutions sont alors envisagées :

1.  l'auteur garde la propriété exclusive et se débrouille pour diffuser
    lui-même ses travaux. Une revue peut cependant l'y aider dans le
    cadre d'un projet d'archives ouvertes, ou en publiant les articles
    sous licence libre, tout en offrant un service d'évaluation par les
    pairs ;

2.  soit c'est la revue qui possède les droits de reproduction et
    d'exploitation, par cession (exclusive ou non) de la part de
    l'auteur. Même si l'auteur ne profite pas des éventuelles royalties
    générées par l'exploitation de son œuvre, il n'est pas pour autant
    lésé, dans la mesure où son œuvre sera bel et bien publiée à
    destination de la communauté. C'est cette dernière qui se trouve
    lésée, nous l'avons vu, par les différents aspects de la
    centralisation et de la restriction de diffusion des informations
    (même à titre gratuit).

Il faut en conclure que les droits d'auteur sont ici considérés comme un
instrument au service de la diffusion des œuvres scientifiques.
Puisqu'il s'agit d'instruments encadrant les droits patrimoniaux et
moraux, il appartient à l'auteur de choisir la manière dont son œuvre
sera exploitée et diffusée. Soit il s'en remet à un tiers, soit il s'en
remet à la communauté. Pourquoi ne pas directement autoriser la
communauté à exploiter et diffuser l'œuvre en fixant par avance les
conditions au nom, justement, du droit d'auteur ? Après tout, même les
éditeurs de revues font partie de la communauté et pourraient profiter
eux aussi des autorisations d'exploitation...mais pas de manière
exclusive.

# Universalisme et liberté


Parmi les solutions envisagées pour favoriser la diffusion et la
créativité dans le cadre du partage des biens intellectuels ou
artistiques, la plus radicale semble être la plus simple : supprimer le
droit d'auteur...ou plutôt supprimer le copyright, ce dernier étant
envisagé essentiellement sous son aspect de droit patrimonial. C'est
l'idée explorée par Joost Smiers et Marieke van Schijndel dans *Imagine
there is no copyright and no cultural conglomerate too*[^13]. Ce livre
propose un nouveau modèle économique où le copyright n'a plus de raison
d'être. Dans leurs études de cas, qui concernent la littérature, la
musique, le cinéma et les arts graphiques, la notion de droit d'auteur
est envisagée uniquement sous l'angle du droit moral. Cela implique de
nouvelles formes d'organisation des marchés dans lesquelles tout serait
fait pour que chaque acteur soit gagnant. La principale caractéristique
en est que toute forme de monopole doit être proscrite en raison même de
la centralisation des coûts et donc de la privatisation des œuvres.

Il s'agit bien sûr d'une utopie. Mais l'un de ses intérêts est
d'adresser quatre objections au système de licences Creative Commons :

1.  Les Creative Commons ne forment pas un système économique capable de
    garantir les revenus de tous les auteurs-créateurs.

2.  Elles ne remettent pas en cause le droit d'auteur, mais créent de la
    propriété qui devient libre .

3.  Rien n'oblige les conglomérats culturels à y participer.

4.  La propriété reste le pivot des licences libres, or la propriété
    n'est pas une condition nécessaire à l'appréciation et à la
    réception d'une œuvre par le public.

Il serait fastidieux ici d'exposer tous les avantages des licences
Creative Commons. Pour cela, j'invite le lecteur à parcourir *Culture
Libre*[^14], écrit par leur initiateur Lawrence Lessig, professeur de
droit à Stanford, ainsi que la page Wikipédia qui leur est consacrée.
Pour l'essentiel, elles sont le résultat d'un véritable tour de force
consistant à adapter une certaine compréhension du droit d'auteur au
modèle économique dominant, dans lequel nous vivons. Loin d'être une
utopie, les même préoccupations à l'encontre des monopoles se retrouvent
mais dans le cadre du libre choix et de la responsabilité individuelle
pour faire vivre les biens communs intellectuels et artistiques.

Un autre aspect des licences Creative Commons est leur universalisme.
Bien sûr, il faut à chaque fois adapter les termes de la licence aux
caractéristiques juridiques du pays concerné en matière de droit
d'auteur. Cependant, cela a pour effet de proposer un modèle commun
censé s'appliquer en tout lieu.

Je prends l'exemple du projet DASH à Harvard, cité par M. P. Rutter et
J. Sellman dans leur article Uncovering Open Access (cf. introduction).
[DASH](http://dash.harvard.edu) est l'acronyme de *Digital Access to
Scholarship at Harvard*. Ce système a été adopté par l'université
d'Harvard afin de mettre à disposition au public, au titre d'archives
documentaires gratuites, l'ensemble des articles écrits par les membres
de la faculté. La formulation du projet fut la suivante : chaque membre
d'une faculté accorde au président et ses collaborateurs de l'université
d'Harvard la permission de rendre disponibles ses articles
universitaires et d'exercer le droit d'auteur pour ces articles . Cela
implique que tout travail universitaire à Harvard est par défaut
gratuitement disponible pour tous. C'est un excellent exemple du
principe de priorité de la diffusion, et sur le fond, peu de critiques
peuvent être adressées, sauf les suivantes.

En premier lieu, cette disposition concerne exclusivement l'université
d'Harvard. Un article publié dans une autre université ne peut être
livré dans le dépôt central DASH. Les [termes
d'utilisation](http://osc.hul.harvard.edu/dash/termsofuse#OAP) sont
plutôt flous quant à l'éventualité d'une diffusion par un tiers (par
exemple dans le cas où plusieurs scientifiques d'Harvard et d'ailleurs
écrivent un article, déposé dans le DASH, mais qu'un des auteurs décide
de le diffuser à son tour dans son pays ou dans son université
d'origine). Qu'arrive-t-il si un jour le projet DASH cesse ses activités
? Il faudra apporter des modifications substantielles aux conditions
d'utilisation (Harvard Open Access Policy[^15]) pour qu'une autre
université reprenne la gestion du dépôt. Mais que se passerait-il en cas
d'arrêt total ?

En somme, là encore, il subsiste des contraintes qui seraient rapidement
levées avec l'adoption des licences Creative Commons. En effet, ce
serait aux auteurs, et non à l'université et ses représentants, de
définir quels sont les usages qui peuvent être faits de leurs articles.
Il est possible de rendre systématique le dépôt des travaux sur un
serveur, mais dans ce cas, il n'y a pas de relation nécessaire entre le
lieu du dépôt et la diffusion de l'œuvre. Le système DASH reste
prisonnier alors que les licences Creative Commons permettent une
diffusion décentralisée des œuvres tout en conservant leurs droits et
sans préférence pour leur provenance.

# La Déclaration de Berlin


Enfin, puisqu'il vient d'être question des systèmes d'archives ouvertes,
deux choses sont regrettables. La première est la multiplication des
dépôts. Les universités ou autres institutions qui ont décidé
d'installer à leurs frais de tels dépôts ont répondu à un besoin
croissant de la part des chercheurs à pouvoir disposer de leur mémoire
documentaire. Elles ont aussi répondu à l'augmentation croissante des
coûts d'abonnements aux revues dont nous avons vu le caractère
monopolistique. En revanche, chacun de ces dépôts possède une politique
de diffusion propre, avec des conditions d'utilisation particulières.
L'*Open Archive Initiative* tend à harmoniser cet ensemble, mais là
encore l'utilisation des licences libres, à caractère universel,
permettrait d'éviter cette tâche fastidieuse.

Le second regret concerne la traduction de open en libre , du moins dans
les exemples francophones de dépôts d'archives. Si les archives sont
déclarées ouvertes, c'est parce que de tels système s'inspirent du
modèle de développement des logiciels *open source*. Or, dans ce
contexte, libre et gratuit ne signifient pas la même chose[^16]. Nous
avons vu que les licences libres sont dites libres parce qu'elle
garantissent la liberté de l'auteur de disposer de ses droits, ainsi que
celle de l'utilisateur dans le cadre du contrat passé avec l'auteur qui
définit les conditions de cette liberté d'utilisation. Les archives
ouvertes ne sont donc pas un accès libre aux ressources , comme on peut
parfois le lire ici et là : rien n'indique dans ces archives le
caractère libre des documents mis gratuitement à disposition des
utilisateurs. La preuve en est que dans la plupart des cas, la mise à
disposition d'articles publiés dans ces archives ouvertes est soumise à
autorisation préalable de la part des revues au regard de leurs
politiques éditoriales. Tel est le prix.

Pourtant, un texte célèbre fut quelque peu oublié par le mouvement des
archives ouvertes, et aurait permis d'éviter ce flou conceptuel entre
*libre* et *ouvert*. Suite à un congrès organisé par la Société Max
Planck pour le Développement des Sciences en 2003 à Berlin, un appel fut
rédigé : la *Déclaration de Berlin sur le libre accès à la connaissance
en sciences exactes, sciences de la vie, sciences humaines et
sociales*[^17]. Cet appel a reçu à ce jour presque 300 adhésions de la
part de différentes institutions académiques. Dans ce texte, qui
encourage les enseignants-chercheurs et toutes les institutions à
favoriser la mise à disposition des publications scientifiques en libre
accès, la première[^18] condition propose une définition claire d'un
texte en libre accès :

Pour rappel, les premières versions des licences Creative Commons ont
été publiées en 2002 et on ne peut s'empêcher de corréler la définition
d'un texte en libre accès par la *Déclaration de Berlin* et la
définition d'une œuvre libre par les Creative Commons. La première fait
explicitement référence à la seconde, sans toutefois la mentionner.
Certes, le mouvement des Creative Commons était encore jeune à cette
époque, mais il n'en demeure pas moins que le texte de la *Déclaration
de Berlin*, s'inscrivant dans le mouvement des Archives Ouvertes, marque
une distance que j'ai déjà mentionné précédemment et que j'approuve
pleinement : une publication scientifique doit être *libre* et pas
seulement *ouverte*.

En qualifiant une œuvre d'ouverte, on ne peut signifier que deux choses : qu'elle peut être en accès gratuit (c'est le cas la plupart du temps) et que l'on s'est arrangé avec les politiques éditoriales. On peut alors se demander pourquoi des auteurs placent eux-mêmes leurs textes en archives ouvertes lorsqu'ils n'ont pas (ou pas encore) été publiés : c'est une manière de donner sans donner, de verser dans le bien commun en gardant la possibilité de retirer à tout moment sous la pression d'une revue. Les licences Creative Commons, elles, permettent à l'auteur de se réserver le droit de changer d'avis mais la version de l'œuvre libérée auparavant reste libre à jamais (les personnes disposant de cette œuvre sous licence libre peuvent continuer à en faire usage sous les conditions dans lesquelles ils l'ont reçue initialement).

Si la Déclaration de Berlin n'est guère citée, c'est parce que les
initiatives d'archives ouvertes disposent d'une meilleure visibilité
dans les lieux fréquentés par les chercheurs et rassurent les revues en
proposant une politique d'archivage sous autorisation. Il nous faut au
contraire défendre une certaine *éthique de la recherche* et laisser le
soin aux archives ouvertes de publier les œuvres non libres,
c'est-à-dire organiser une forme de récupération (mais ô combien
importante) en échange d'un peu de liberté. Même si rien n'empêche un
auteur de déposer un travail sous licence libre dans un dépôt d'archives
ouvertes, le mélange des genres est selon moi peu recommandé, à moins
d'ouvrir des sections spécifiques aux licences libres.

# Aspects pratiques


Pour terminer ce texte, j'aimerais aborder quelques aspects pratiques,
même si les exemples cités plus haut sont assez clairs. La question des
livres et de l'évaluation de la qualité des documents doivent néanmoins
faire l'objet de quelques précisions. J'aborderai en dernier lieu, mais
sans chercher à être exhaustif, la forme des échanges scientifiques, une
question qui mériterait d'être beaucoup plus approfondie.

## Et les livres ? 


Nous n'avons parlé jusqu'à présent que des articles scientifiques mais
les mêmes réflexions s'adaptent parfaitement à tous les domaines de
l'édition scientifique. En réalité, nous pouvons considérer que revues,
monographies, rapports et ouvrages collectifs peuvent tous utiliser les
licences Creative Commons.

Il est très courant, dans le cadre d'un projet de recherche subventionné
par une instance publique, que le groupe de chercheurs utilise une
partie des fonds pour publier un ouvrage collectif de référence. Dans ce
cas de figure, il s'adresse souvent à une maison d'édition qui non
seulement s'engage à mettre le livre ou la revue à disposition dans son
catalogue, mais demande aussi une certaine somme d'argent pour faire
imprimer un minimum d'exemplaires et éventuellement (parfois ce n'est
même pas le cas) effectuer un travail éditorial et de mise en page. Il
est anormal que des fonds dédiés à la recherche soient utilisés pour
publier à perte un nombre conséquent d'ouvrages destinés à prendre la
poussière dans un placard.

La mise à disposition et la diffusion de tous types d'ouvrage sous forme
numérique permettrait de couvrir un large public (souvent il est
difficile de se procurer un livre d'une petite maison d'édition depuis
un pays étranger). Si le livre au format papier est estimé nécessaire ou
répond à un besoin de la part des utilisateurs (car il est souvent plus
agréable de lire sous ce format), les systèmes d'impression à la demande
existent, et fonctionnent avec des machines de reproduction numérique.
Les cas de [I-kiosque](http://www.i-kiosque.fr/) et de [In Libro
Veritas](http://www.ilv-bibliotheca.net), par exemple, méritent d'être
mentionnés. Il est curieux que le monde scientifique n'utilise pas
davantage de telles solutions impliquant des coûts bien moindres.

## Et l'évaluation ? 


Si tout le monde peut publier n'importe quoi, il n'y a plus de
crédibilité . Cette affirmation est vraie, bien entendu. Mais j'ai passé
du temps à démontrer que l'essentiel de la publication scientifique, de
l'évaluation à la diffusion, est assuré en majeure partie par la
communauté des chercheurs. Qui évalue les projets d'ouvrage au profit
des maisons d'édition, si ce ne sont des pairs que l'on charge, pour
l'occasion, de la responsabilité d'une collection ?

Revues et collections d'ouvrages peuvent fonctionner sur le même mode :
une sélection d'articles ou d'ouvrages, parmi les plus pertinents pour
les thèmes choisis, et leur agrégation dans un ensemble cohérent.
Pourquoi cela devrait-il se faire obligatoirement sous l'égide d'une
maison d'édition et de son nom ? Surtout si les efforts de mise en page
et de corrections sont externalisés chez les auteurs. En fait, ce
qu'apportent aujourd'hui les maisons d'édition classiques aux
publications scientifiques se réduit bien souvent à un nom (une marque)
et un catalogue de diffusion...en échange d'un contenu sérieux et
vendable. Si l'auteur souhaite toucher des royalties, il peut très bien
le faire dans le cadre de l'impression à la demande et si la licence
Creative Commons choisie autorise la diffusion commerciale[^19].

La communauté scientifique dispose aujourd'hui des moyens nécessaires et
suffisants pour gérer ses propres revues ou collections. En fonction de
leur importance, des moyens humains peuvent être mis à disposition par
les universités, par exemple, tout en vendant des exemplaires papier
publiés à la demande. Cela représente certes un coût qui doit être
internalisé par les instances académiques, mais il sera bien moindre que
le coût engendré par les multiples abonnements. Il se trouve que, déjà,
une grande partie de des coûts de production revient aux institutions
(en temps de travail de la part des scientifiques, ou en subventionnant
indirectement des publications, ou encore en hébergeant des maisons
d'édition comme les Presses Universitaires de...).

## Et les serveurs ? 


Permettez-moi ici de revenir sur nombre de dépôts d'archives ouvertes :
il serait bien plus efficace de les transformer en autant de nœuds
accueillant indifféremment n'importe quel article, quelle que soit sa
provenance, et placé sous licence Creative Commons. Par conséquent il y
a plusieurs avantages à favoriser ainsi la décentralisation de la
distribution des œuvres :

-   la diffusion de pair à pair est possible,

-   la redondance (un même article sur plusieurs serveurs) permet
    d'optimiser la disponibilité de l'article,

-   l'accès est moins soumis aux contraintes techniques et -- parfois --
    idéologiques inhérentes aux réseaux et la couverture mondiale serait
    mieux assurée (certains pays soumis, par exemple, à une dictature,
    peuvent restreindre les accès à des serveurs d'autres pays plus
    démocratiques : la possibilité de copier et diffuser à l'intérieur
    de ces pays des œuvres libres est un pas de plus vers la
    démocratie).

## Et les formats ? 


Aujourd'hui, la plupart des articles diffusés sous forme numérique sont
accessibles en HTML et PDF. Les conglomérats du marché n'ont
généralement pas rendu totalement privateurs les formats sous lesquels
les articles peuvent être disponibles au téléchargement payant. Il
importe d'être attentif à d'éventuelles transformations dans ce domaine.
Le format HTML est de loin le meilleur rendu adapté à la fois à la
lecture et à la diffusion. Aujourd'hui, dans la mesure où tous les
articles scientifiques sont produits sous format numérique par leurs
auteurs, rien n'empêche la mise en ligne sous ce format. L'utilisation
du format PDF, s'il permet un confort différent, peut aussi être soumis
à l'emploi de DRMs et autres verrous numériques visant à empêcher la
diffusion ultérieure ou permettant la lecture pour un temps donné sous
réserve de paiement. La recherche du profit dans le cadre de l'exercice
d'un monopole peut prendre des formes variées auxquelles la communauté
scientifique se doit de rester attentive. Voyez la [page Wikipédia](http://fr.wikipedia.org/wiki/Format_ouvert) consacrée aux
formats libres garantissant l'interopérabilité.

-----

[^1]: Voir [le site
    creativecommons.org](http://fr.creativecommons.org/).

[^2]: Le Conseil Européen pour la Recherche Nucléaire fut créé en 1952
    et a changé de nom deux ans plus tard. L'acronyme fut toutefois
    conservé.

[^3]: Pour une traduction française, voir le billet du
    [Framablog](http://www.framablog.org/index.php/post/2010/12/11/decouvrir-le-libre-acces)
    (11 décembre 2010).

[^4]: Nous empruntons cette expression à Benjamin Bayart, dans cette
    [célèbre
    conférence](http://www.fdn.fr/internet-libre-ou-minitel-2.html)
    intitulée Internet libre ou minitel 2.0 ? (juillet 2007).

[^5]: On peut se reporter à la thèse de Nathalie Pignard-Cheynel, *La
    communication des sciences sur Internet. Stratégies et pratiques*,
    Université Stendhal Grenoble 3, 2004
    ([lien](http://sciences-medias.ens-lyon.fr/scs/article.php3?id_article=167)),
    en grande partie consacrée au système Arxiv.

[^6]: Voir le rapport biennal de l'[Observatoire des Sciences et des Techniques](http://www.obs-ost.fr) qui a analysé la part de
    production scientifique de la France de 1993 à 2006. En 1993, la
    France a contribué à la publication de 31618 articles, contre 39068
    en 2006, ce qui représente respectivement 5,2% et 4,4% en parts de
    publications mondiale. Il faut prendre en compte deux éléments
    importants dans cette analyse : premièrement il s'agit de
    contributions, car un article scientifique peut avoir été écrit de
    manière collaborative entre plusieurs chercheurs de pays différents.
    D'un autre côté, l'OST ne traite que des parts de publication pour 7
    champs disciplinaires : mathématiques, biologie, chimie, physique,
    sciences de l'univers, recherche médicale, sciences pour
    l'ingénieur. Il est impossible d'évaluer le nombre de publications
    scientifique au niveau mondial, car toutes les revues ne seraient
    jamais recensées. Il faudrait de même faire un travail d'analyse
    toutes disciplines confondues. En revanche si on considère que la
    France a maintenu un niveau supérieur à 4% compte tenu de la montée
    des puissances comme la Chine et l'Inde, on peut en conclure que le
    rythme de publications français n'a cessé de croître ces dernières
    années, et que ce n'est pas le seul pays dans ce cas.

[^7]: On peut consulter la réponse à la question Combien coûte un
    abonnement électronique sur le [portail documentaire](http://jubil.upmc.fr/repons/portal/) de l'UPMC.

[^8]: La plupart du temps, il est vrai, après la publication. Mais il
    n'est guère aimable (ni stratégique du point de vue de la renommée
    personnelle) de refuser à un cher collègue la communication d'un
    article dont on est l'auteur, et de le prier d'aller débourser
    quelques euros en commandant le numéro de la revue en question
    (surtout si cette revue ne profite pas du support des Elsevier et
    Springer, auquel cas, la commande peut souvent devenir un parcours
    long et pénible).

[^9]: [UK PubMed Central](http://ukpmc.ac.uk/) est un site miroir de
    PMC.

[^10]: Ou d'art, en général. C'est la question des biens communs qui est
    soulevée ici. En publiant un article sous licence Creative Commons,
    on verse les connaissances dans le bien commun, c'est un acte
    altruiste qui peut toutefois être conditionné : possibilité de
    commercer ce bien, possibilité de l'améliorer ou le modifier (sans
    que cela nuise à l'auteur ou à l'œuvre elle-même.

[^11]: Mark A. Lemley, Property, Intellectual Property, and Free Riding
    , *Texas Law Review*, 83, pp. 1031, 2005.

[^12]: Philippe Aigrain, *Internet et Création*, Paris : [In Libro
    Veritas](http://www.inlibroveritas.net/lire/oeuvre20460.html), 2008.

[^13]: Joost Smiers et Marieke van Schijndel, *Imagine there is no copyright and no cultural conglomerate too*, Amsterdam : Institute of Network Cultures, 2009. [Accès libre](http://networkcultures.org/wpmu/theoryondemand/titles/no04-imagine-there-are-is-no-copyright-and-no-cultural-conglomorates-too/).

[^14]: Lawrence Lessig, *Free Culture, How Big Media Uses Technology and
    the Law to Lock Down Culture and Control Creativity*, New York: The
    Penguin Press, 2004
    ([http://www.free-culture.cc](http://www.free-culture.cc/)). Placé
    sous licence libre, ce livre a été traduit en plusieurs langues, y
    compris [en
    français](http://fr.readwriteweb.com/wp-content/uploads/Culture_Libre-Lawrence_Lessig.pdf).

[^15]: Le guide destiné aux auteurs se trouve sur le [site du service de
    documentation](http://osc.hul.harvard.edu/authors/policy_guide). On
    peut aussi consulter cet article du Harvard Magazine ([mai-juin
    2008](http://harvardmagazine.com/2008/05/open-access.html))

[^16]: le mouvement du logiciel libre, initié par Richard M. Stallman
    propose une éthique là où le mouvement open source propose un
    principe d'efficacité. Si, pour des logiciels, la question peut-être
    discutée, je ne pense pas qu'elle puisse l'être concernant les
    connaissances sicentifiques. Pour comprendre la différence, je vous
    invite à lire : Richard Stallman, Christophe Masutti, Sam Williams,
    *Richard Stallman et la révolution du logiciel libre. Une biographie
    autorisée*, Paris: Eyrolles ([Framasoft -
    Framabook](http://www.framabook.org/stallman.html)), 2010.

[^17]: Le texte en français peut être téléchargé à cette adresse :
    [http://oa.mpg.de/...](http://oa.mpg.de/files/2010/04/BerlinDeclaration_wsis_fr.pdf)
    (ainsi que la [liste des signataires](http://oa.mpg.de/lang/en-uk/berlin-prozess/signatoren/)).

[^18]: La seconde concerne le format et l'accessibilité de l'œuvre.

[^19]: Pour un modèle de collection de livres (non académiques) sous
    licences libre, avec partage de royalties entre auteurs et éditeur,
    voyez [Framabook.org](http://www.framabook.org/).

