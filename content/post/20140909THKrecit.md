---
title: "THK: premier trail à ambiance médiévale"
date: 2014-09-09
author: "Christophe Masutti"
description: "Trail du Haut Koenigsbourg"
image: "/images/postimages/hikebike.png"
tags: ["Sport", "Trail", "Vosges", "THK"]
categories:
- Sport
---

Le trail du Haut Koenigsbourg ! Il était temps que je cesse enfin de
faire l'ours solitaire au fond des bois pour courir une épreuve
organisée. Il y en aura peu, car décidément j'aime bien être seul, mais
pour cette première, un petit récit de course s'impose, d'autant plus
que l'organisation fut extraordinaire...

### Nous étions 500 et par un prompt renfort... !


07h00 du matin le 07 septembre 2014, arrivée à Kintzheim, petite
bourgade alsacienne charmante tout juste sortie du brouillard matinal.

J'arrive pile au top départ de la version 54km du Trail du Haut
Koenigsbourg, juste à temps pour taper la claque aux courageux qui
s'élancent dans la fraîcheur des vignes. Une heure après, ce sera mon
tour, le temps d'un petit échauffement et quelques étirements dans le
quartier qui se réveille doucement.

Pour sa 4e année, le [trail du Haut Koenisgbourg](http://www.trail-hk.com/) offrait pas moins de 4 épreuves
dans la même matinée : 12km (350m+), 24km (850m+), 54km (2020m+) et 84km
(3284m+).

Si on lit un peu les témoignages ici et là sur les réseaux
sociaux, l'organisation fut exemplaire et c'est peu dire. L'accueil et
la gestion d'un total de 1664 coureurs (non, ce n'est pas le trail du
Haut Kronenbourg, relisez bien) ne fut pas une mince affaire, mais
ajoutez-y, en vrac :

-   un balisage ultra clair, avec de jolies pancartes parfaitement
    visibles
-   un parcours littéralement enchanteur, avec des points de vue variés
-   un passage aménagé **dans** le château du Haut Koenigsbourg, si!
-   et en prime des ménestrels avec de la musique médiévale, et des
    châtelains pour nous encourager,
-   un énorme camion-douche, des ravitaillements sympas et complets, et
    pour couronner le tout, une chaude ambiance au départ comme à
    l'arrivée.

Voilà pour poser le cadre de cette course. Autant dire qu'on ne peut pas
regretter de s'être inscrit. Du coup, la barre est placée très haut pour
ce premier trail : ca va être dur de s'inscrire à d'autres courses!

### C'est parti, ca s'accroche derrière


07h40 : Antoine me rejoint. Le collègue n'en est pas à son premier
trail, loin de là. Il est à l'aise, lui... Moi, un peu intimidé par tout
ce monde, je ne sais pas trop où me placer. Devant, n'y pensons même
pas. Tout derrière? pourquoi pas... ou bien dans le gros de la troupe...

Peu importe, en définitive, car les 4 premiers kilomètres devraient
servir à étirer au maximum le cortège, enfin, presque... 

08h00 : le départ des 24km est donné par Môssieur l'maire de Kintzheim en personne.
Quelques 800 coureurs s'élancent en direction des vignes. Diantre! ca
part vite! Ha zut, ma montre qui ne capte pas le satellite... bidouillage... 

« -- tu captes, toi? -- non, pas de réception! -- ha si, ca vient. Hé mais c'est quoi ce cardio de malade? Elle délire c'te
montre. »

En fait, le cardio se stabilisera en milieu de course. Il faut
dire que c'est mon principal repère (quand d'autres regardent plutôt
leur vitesse): donc soit on est vraiment parti très vite, soit le cardio
déconnait, plutôt un peu des deux... Je n'en saurai pas plus. Ca monte
dans les vignes, terrain bitume/béton pour commencer. On se réchauffe
néanmoins au soleil rasant, dans une lumière typiquement alsacienne,
juste au dessus des vignes encore un peu brumeuses, avant d'entrer dans
la forêt du Hahnenberg.

Premier passage single après le premier ravitaillement... ca bouchonne... et un peu de déception.... J'avais
fait le parcours en solo le 22 août, et à cet endroit, on peut courir...
enfin, trottiner, quoi. Tant pis, c'est sympa, on monte au Hahnenberg
file indienne, youkaïdi, youkaïda.

Je perds complètement Antoine de vue
: il me dira plus tard avoir tapé une grosse accélération pour doubler
plusieurs coureurs. Moi, je n'ai pas osé de crainte de devoir bousculer,
et puis je discutais du parcours avec mes collègues de devant... 

Arrivé
à ce premier sommet, la course commence vraiment. Pas le temps d'admirer
le paysage (et pourtant une très belle vue sur la plaine d'Alsace) : je
décide de descendre aussi vite que possible. Finalement, je n'aurai pas
gagné énormément de temps : la descente n'est pas longue et on se
retrouve ensuite sur un chemin assez roulant.

Mon dossard fait des
siennes, il joue au Sopalin avec la sueur : mauvaise qualité (beaucoup
de coureurs sont dans le même cas, cela arrive). Je le remets, il se
re-déchire... Tant pis, je le sortirai au besoin... direction mon slip.

A partir du parking de la Montagne des Singes (km 9), je commence
seulement à prendre mon rythme de croisière. Le passage dans le
sous-bois moussu est un enchantement. Un ravitaillement marrant au
Refuge Pain d'Epices (on nous distribue... du pain d'épices, donc) et on
commence l’ascension vers le château du Haut Koenigsbourg.

C'est sans
doute la partie la plus pénible du parcours : un long chemin forestier
ascendant, du genre où tu marches plus vite que tu ne cours, mais pas
systématiquement...

Et là arrive la récompense (km 18)! le sommet ? non!
le château : d'abord une châtelaine m'invite à emprunter une poterne le
long du mur d'enceinte.... des escaliers (fabriqués exprès pour nous?)
puis on se met à courir sur le chemin de ronde du château.

« Attention la tête! » Hé oui, les médiévaux étaient en moyenne plus petit que nous
autres... enfin, pour ce qui me concerne avec mon 1,68m je n'étais pas
vraiment concerné. Et là, j'ai pensé aux grands coureurs de devant, qui
avaient franchi la ligne d'arrivée depuis longtemps... Après tout, un
trail, c'est technique, et il faut courir aussi avec la tête. Ici, au
sens propre comme au sens figuré.

Après le château, la descente vers
Kintzheim. Enfin, une descente... qui comprend tout de même une assez
longue partie sur un replat entre les deux sommets. C'est la
particularité des Vosges. Donc rythme de croisière, on pense à autre
chose. Je vois pas mal de gens avec des sales crampes en train de faire
des étirements... je décide de ne pas forcer. De toute façon, l'arrivée
n'est pas loin...

### Arrivée et conclusion

10 minutes avant l'arrivée. Avant une dernière pente bétonnée (où je
préfère marcher quelques mètres), je me retrouve un peu seul, j'en
profite pour faire le point sur le temps de parcours : je mettrai
finalement le même temps qu'en solo: 3h05. Compte-tenu du fait que les 8
premiers kilomètres ont été plutôt laborieux, j'ai plutôt bien couru
dans l'ensemble, ce qui me laisse une belle marge de progression pour le
prochain trail.

Évidemment, j'ai pris un rythme plutôt ronflant : le
plus difficile aura été de courir en groupe et composer avec le rythme
des autres dans les passages étroits. Beaucoup de temps peut être gagné
si l'on est seul, par conséquent, il faut être un peu plus stratégique
sans toutefois se précipiter.

Enfin l'arrivée! Je retrouve mon collègue qui m'a précédé d'une dizaine de minutes. Bah! il a fait la photo, merci
!

[Voir le parcours](https://umap.openstreetmap.fr/fr/map/carte-sans-nom_16273)

![CM](/images/cmthk.jpg)

