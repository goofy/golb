---
title: "Faire du sport librement avec Turtle Sport"
date: 2014-07-14
author: "Christophe Masutti"
image: "/images/postimages/hikebike.png"
tags: ["Sport", "Trail", "VTT", "Logiciel libre"]
description: "Turtlesport, un logiicel qui permet de connecter des dispositifs de mesure, organiser ses entraînement, cartographier ses parcours"
categories:
- Sport
---

Après avoir cherché un cardio-fréquencemètre + GPS fiable et qui ne nécessite pas de connexion à un réseau social quelconque, il faut aussi avoir un bon logiciel capable de lire les traces et utiliser les informations récoltées durant la course. Sous GNU/Linux, un logiciel qui fait parler de lui, c'est Turtle Sport.

## Qu'est-ce que Turtlesport ?

Turtlesport est un logiciel sous licence GNU LGPL. Il n'est pas exclusivement réservé à GNU/Linux et tourne aussi bien sous MSWindows que sous MACOS. Un logiciel sympathique, donc, puisque, en profitant d'une communauté élargie, il est traduit en 9 langues.

Pour reprendre son descriptif, Turtle Sport est «&nbsp;un logiciel pour communiquer avec les produits Garmin fitness (forerunner and edge)&nbsp;». En réalité, il est capable de lire des formats de fichiers comme `.fit`, `.gpx`, `.tcx`, `.xml`. etc. Donc, si vous avez un dispositif capable de produire de tels fichiers et que vous pouvez les récupérer, il suffit ensuite de les importer. Turtlesport peut néanmoins communiquer avec les dispositifs **Garmin** de manière assez efficace, d'autant plus que les distributions GNU/Linux sont capables de le faire même nativement. Pour ce qui me concerne, je connecte ma montre Garmin Forerunner et Turtle Sport me liste les courses effectuées.

Le reste est à l'avenant : on navigue entre les courses par exemple sur le mode d'un agenda, et après avoir entré vos données personnelles (fréquence cardiaque max, taille, poids, etc) vous pouvez commencer à travailler les aspects statistiques et les performances. Les fonds de cartes, quant à eux sont au choix&nbsp;: de Google Map à OpenStreetMap…


Enfin, si vous désirez partager votre course avec un de vos correspondants, il est possible de la lui envoyer par courriel par exemple sous la forme d'un fichier GPX (exporté) ou d'un lien Googlemap en un clic directement depuis Turtle Sport. Cela présente aussi l'avantage de disposer du fichier GPX qui, lui, est lisible sur de nombreux sites dédiés au partage de tracés&nbsp;: ainsi vous choisissez ce que vous voulez partager sur Internet…
