---
date: 2019-04-14
title: "Rando et VTT : dispositifs GPS et cartographie"
author: "Christophe Masutti"
description: "Ce billet concerne tous ceux qui, dans le cadre d'activités de plein air (randonnée, VTT, trail) ont besoin d'une solution permettant de planifier et visualiser des parcours et disposer d'une solution mobile sur le terrain. Choisir ses logiciel, créer des fonds de cartes, embarquer une solution de navigation."
image: "/images/postimages/naturel.jpg"
draft: false
tags: ["Sport", "VTT", "Logiciel libre", "Parcours", "cartographie"]
categories:
- Sport
- Logiciel libre
---



Ce billet concerne tous ceux qui, dans le cadre d'activités de plein air (randonnée, VTT, trail) ont besoin d'une solution permettant de planifier et visualiser des parcours et disposer d'une solution mobile sur le terrain. Les logiciels cités ici ne sont pas tous libres&nbsp;: après avoir longuement cherché des solutions simples à mettre en œuvre, du moins qui ne nécessitent pas d'être un crack en Système d'Information Géographique, quelques compromis se sont imposés.

# De quoi avons-nous besoin&nbsp;?

- D'un smartphone Android (je n'ai pas fait de test avec d'autres OS)&nbsp;: j'ai choisi d'en recycler un sous version ancienne (mais pas trop) d'Android. Conseil&nbsp;: s'assurer que le smartphone dispose d'une puce GPS fiable (les puces d'il y a dix ans étaient réputées peu exactes, mais cela s'est largement amélioré). L'idée est de disposer d'un dispositif peu onéreux et néanmoins utile.
- d'un chargeur&nbsp;: en randonnée pédestre, il y a toujours moyen d'économiser de la batterie, voire se contenter d'enregistrer une trace ou simplement visualiser une carte embarquée à la demande. En VTT ou en course à pied, on ne peut guère manipuler le dispositif en pédalant ou en courant. Mes essais ont rarement dépassé 4 heures d'utilisation avec écran allumé en permanence et guidage GPS en action, mais au bout du compte le niveau de batterie excédait rarement 15%. Ajoutons à cela la température ambiante et la variabilité des batteries de smartphone&nbsp;: à vous de voir de quelle quantité d'énergie vous avez besoin.
- d'un ordinateur sous GNU/Linux ou MSWindows avec une bonne connexion Internet (pour télécharger les tuiles de cartes).
- être à l'aise avec la gestion de fichiers.

# Quels sont les objectifs&nbsp;?

Il s'agira d'utiliser des fonds de cartes les plus exacts possibles (en l’occurrence des fonds de carte IGN)[^1] sur le dispositif embarqué et visualiser une trace par-dessus de manière à suivre un parcours dans les meilleures conditions. On pourra néanmoins utiliser les fonds de cartes communautaires d'Open Street Map avec les logiciels permettant de planifier les parcours même si quelquefois, il vaut mieux s'assurer de la bonne correspondance avec les cartes IGN (et éventuellement contribuer à OSM pour compléter les cartes).

L'IGN a depuis peu de temps rendu disponible sur Geoportail les balisages du Club Vosgien. Étant donné que mes pérégrinations concernent les Vosges, j'ai d'autant plus d'intérêt à utiliser ces fonds de cartes.

[^1]: Les fonds de cartes OpenStreetMap sont certes très fiables et exacts mais lorsqu'il s'agit de naviguer dans des coins peu fréquentés de certains massifs, il y a tout de même des chemins non cartographiés. Une solution très importante, si vous tournez régulièrement dans les mêmes coins, est de [contribuer à OpenStreetMap](https://wiki.openstreetmap.org/wiki/FR:Guide_du_d%C3%A9butant) dès que vous le pouvez. C'est facile, notamment avec l'éditeur de carte sur le site OSM.  Un autre point, particulièrement bloquant dans les endroits que je fréquente (les Vosges), est que seul l'IGN a pour l'instant l'exclusivité du balisage du Club Vosgien. Cette histoire de droit et de propriété intellectuelle est désolante mais il faut reconnaître que, une fois sur le terrain, pouvoir visualiser sur la carte les parcours du club Vosgien est très utile, particulier pour avoir une idée de la «&nbsp;fréquentabilité&nbsp;» des sentiers.

## Petite précision rapide

Nous allons parler de «&nbsp;tuiles&nbsp;» (*tiles*), de calques, et de niveaux de zoom. Kézako&nbsp;?

Un fond de cartographie numérique est composé de plusieurs couches d'images correspondants à des puzzles rassemblés en autant de niveaux de zoom sur la carte. Ce n'est pas exactement les échelles (valables sur une carte papier) mais, pour faire court, on peut dire les choses ainsi&nbsp;: plus vous zoomez plus votre «&nbsp;échelle&nbsp;» est précise. Selon ce que vous voulez faire, par exemple pour de la randonnée, il n'y a pas besoin de composer un fond de carte qui comprend tous les niveaux de zoom. Ainsi, pour obtenir l'équivalent ce qu'on voit sur une carte papier à 1/250000 on utilisera toutes les images (les tuiles) qui correspondent au niveau voulu.

Quant aux calques, c'est un vocable que l'on retrouve partout où l'on traite d'image numérique. Là encore, pour faire simple, on peut dire qu'un fond de carte est un calque sur lequel on va superposer un autre calque qui est en fait le dessin d'une trace GPS. Ce dessin est obtenu avec un fichier (souvent portant l'extension GPX) qui contient tous les points GPS qui composent la trace. 

Un tel fichier (lorsqu'on l'ouvre avec un éditeur de texte) comprend un listing, dans l'ordre de passage, de tous les points&nbsp;: latitude et longitude, date et heure, altitude, etc.



# Quelles sont les tâches&nbsp;?

Il y a plusieurs aspects à prendre en compte&nbsp;:

1. choisir les logiciels à utiliser sur le dispositif portable (Oruxmaps, Maverick),
2. récupérer les fonds de cartes pour être utilisés sur le dispositif portable (Mobac),
3. choisir les logiciels à utiliser pour planifier les parcours sur l'ordinateur (Viking, QMapShack)
4. choisir les logiciels à utiliser pour visualiser les parcours sur l'ordinateur (et créer sa base de données avec ses stats)&nbsp;: Turtlesport, Mytourbook, partager avec Framacarte...

## Applications sur le smartphone

C'est sans doute le sujet le plus décevant&nbsp;: je n'ai pas trouvé d'application libre/open source satisfaisante. Cependant, les applications offrent la possibilité d'utiliser les cartes du projet Open Street Map (OSM).

Que faut-il&nbsp;?

- une application dans laquelle on peut importer des fonds de carte sur mesure,
- une visualisation claire du tracé avec suivi GPS et, si possible, un code couleur pour les dénivelés.

Mon choix s'est porté sur [Oruxmaps](https://oruxmaps.com/cs/en/). Outre de nombreuses fonctionnalités utiles, cette application permet un guidage GPS particulièrement efficace et reste peu gourmande en énergie, même avec la fonction «&nbsp;écran allumé&nbsp;» pendant tout le trajet. 

Une fois le smartphone connecté à l'ordinateur, pour déposer des fonds de carte et des traces GPX, il suffit de naviguer&nbsp;:

- dans ``oruxmaps > mapfiles`` et déposer les dossiers de fonds de carte. **Ou bien** (et c'est conseillé) configurer depuis Oruxmaps l'emplacement des fonds de carte («&nbsp;définir le dossier où sont stockées les cartes&nbsp;») et pointer sur un dossier de la carte SD externe. En effet, si on met toutes les Vosges du Sud, le dossier prend facilement 300&nbsp;Mo, par exemple.
- dans ``oruxmaps > tracklogs`` et déposer les fichiers GPX des traces que l'on veut suivre. Puis dans le menu Oruxmaps, «&nbsp;gérer traces / routes&nbsp;», sélectionner le tracé voulu.

Oruxmaps permet un affichage de la trace avec un code couleur bien visible indiquant les dénivelés. C'est très utile car le smartphone étant situé à plus de 30&nbsp;cm des yeux (dans mon cas sur le guidon de mon VTT, avec moult vibrations), repérer les courbes de niveaux d'une carte est plutôt difficile.


{{< figure src="/images/Oruxmaps-navi.png" title="Fig. 1 -- Navigation sur Oruxmaps" >}}





Une autre application offre peu ou prou les mêmes conditions, c'est [Maverick](https://codesector.com/maverick). L'interface est plus simple, plus facile à prendre en main, mais il y a moins de gadgets. L'essentiel porte sur la géolocalisation. De la même manière&nbsp;:

- pour déposer un fond de carte, on navigue dans le dossier ``Maverick > maps``,
- pour déposer une trace, on navigue dans ``Maverick > tracks``.

{{< figure src="/images/Maverick-navi.png" title="Fig. 2. -- Navigation avec Maverick" >}}



On peut toutefois noter une particularité intéressante pour Maverick&nbsp;: comme on va le voir plus loin, nous allons créer des fonds de carte avec un certain niveau de zoom (zoom 15 pour ce qui nous concerne). Ce qui signifie que les tuiles qui correspondent à d'autres niveaux de zoom (par exemple le fond de carte OSM présent par défaut) peut très bien chevaucher nos tuiles. Ainsi, en zoomant, on obtient alors un mélange des cartes libres Open Street Map (OSM) et nos tuiles, ce qui permet d'avoir un panel très complet.

Une autre application se nomme [RandoGPS](http://www.randogps.net/). Je ne m'étendrai pas dessus car elle ne correspond pas tout à fait au besoin exprimé plus haut. Elle mérite cependant d'être signalée&nbsp;: elle permet d'afficher «&nbsp;offline&nbsp;» le fond de carte correspondant à la trace qu'auparavant on aura pris soin d'entrer sur notre compte en ligne. Grâce à un astucieux système de numéro, on peut alors récupérer la trace directement depuis l'application. RandoGPS est néanmoins dédiée à la randonné pédestre et se prête beaucoup moins bien à d'autres activités sportives.

## Récupérer des fonds de carte

À cette étape, il faut utiliser un logiciel qui, non seulement est capable de récupérer toutes les images d'un fond de carte aux dimensions voulues, mais aussi convertir l'ensemble dans un format qui puisse être lu par l'une ou l'autre des applications sur le smartphone.

Pour cela, il y a un logiciel libre (sous licence GNU GPL) nommé [Mobile Atlas Creator](https://mobac.sourceforge.io/), alias MOBAC (avant 2010, il se nommait TrekBuddy Atlas Creator). Écrit en *java*, il est utilisable sur GNU Linux comme sous MSWindows. Il suffit de télécharger Mobac et dézipper le fichier localement pour lancer ensuite le ``.jar`` sous GNU Linux ou le ``.exe`` sous MSWindows.

Mobac propose, dès l'ouverture, une visualisation des fonds de carte téléchargeables à la demande via un système de clés (c'est pour cela qu'il faut être connecté à Internet pour l'utiliser). Si, à l'ouverture, vous avez une erreur indiquant que la cartographie choisie n'est pas disponible, sélectionnez, en haut à droite la source ``OpenStreetMap 4UMaps.eu``. C'est de loin un jeu très complet.

### Utiliser le bon jeu

Comme je l'ai dit plus haut, aussi sympathiques que soient les tuiles communautaires d'OSM, elles ne sont pas aussi complètes que celles de l'IGN[^2] et nous cherchons aussi à obtenir les balisages du Club Vosgien.

[^2]: Mais encore une fois, il suffit de contribuer&nbsp;: OSM souffre la plupart du temps de quelques chemins non complétés, assez peu pour néanmoins utiliser les cartes. De retour chez vous, si vous voyez qu'il manque quelques centaines de mètres sur un sentier, vous pouvez compléter et obtenir au bout du compte une cartographie complète des coins où vous vous rendez souvent. Cela prend peu de temps et les mises à jour d'OSM sont régulières.

Pour obtenir le fond de carte IGN sur MOBAC, on peut utiliser la *Clé pratique* que l'IGN a [rendu disponible](https://geoservices.ign.fr/blog/2017/06/28/geoportail_sans_compte.html) aux utilisateurs qui ne souhaitent pas utiliser de compte professionnel (et assez complexe). Pour cela il faut ajouter un petit fichier de configuration qui va utiliser cette fonctionnalité pratique de l'IGN. Voici comment faire ce petit bricolage facile.

Premièrement, fermez MOBAC et ouvrez un éditeur de texte. Si vous êtes sous GNU Linux, vous avez l'embarras du choix. Sous Windows, utilisez par exemple [Notepad++](https://notepad-plus-plus.org/fr/), un logiciel libre très pratique. Créez un nouveau document et copiez-collez le code ci-dessous&nbsp;:

<pre>
name = "IGN Pratique Cartes+ortho";

tileType = "jpg";
tileSize = 256;
minZoom = 0;
maxZoom = 19;
ignoreError = "True";

String getTileUrl( int zoom, int x, int y ) {
if (zoom < 17)
{return "http://wxs.ign.fr/pratique/geoportail/wmts?SERVICE=WMTS&VERSION=1.0.0&REQUEST=GetTile&LAYER=GEOGRAPHICALGRIDSYSTEMS.MAPS&STYLE=normal&FORMAT=image/jpeg&TILEMATRIXSET=PM&TILEMATRIX=" + zoom + "&TILEROW=" + y + "&TILECOL=" + x ;}
else
{return "http://wxs.ign.fr/pratique/geoportail/wmts?SERVICE=WMTS&VERSION=1.0.0&REQUEST=GetTile&LAYER=ORTHOIMAGERY.ORTHOPHOTOS&STYLE=normal&FORMAT=image/jpeg&TILEMATRIXSET=PM&TILEMATRIX=" + zoom + "&TILEROW=" + y + "&TILECOL=" + x ;}
}
</pre>


Enregistrez ensuite le document en lui donnant l'extension ``.bsh`` (mettez le nom que vous voulez devant) dans le dossier ``\mapsources`` de MOBAC (lorsque vous avez dézippé Mobac pour lancer le programme vous avez sans doute remarqué la présence de ce dossier).

Relancez ensuite MOBAC et sélectionnez en haut à droite dans les sources celle qui porte le nom «&nbsp;IGN Pratique Cartes+ortho&nbsp;» (cf. le fichier que vous venez de créer). 

Désormais, en zoomant vous reconnaîtrez les cartes IGN et, du côté des Vosges, vous verrez le balisage du Club Vosgien que l'IGN a rendu disponible sur ses fonds de carte.

### Créer un atlas

Il faut maintenant créer un atlas, c'est-à-dire un fond de carte correspondant à la zone que vous désirez de manière à l'utiliser sur le dispositif mobile GPS.

1. Sur Mobac, positionnez-vous sur la zone que vous désirez&nbsp;: clic droit pour bouger la carte et clic gauche pour sélectionner une zone.
2. Sélectionnez dans le menu ``Atlas > Nouvel atlas``.
3. Dans la fenêtre qui apparaît, donnez un nom à votre Atlas. Pour le format, si vous utilisez Oruxmaps, sélectionnez ``Oruxmaps Sqlite`` et si vous utilisez Maverick, sélectionnez ``RMaps SQlite``.
4. Dans le panneau de gauche, sélectionnez un ou plusieurs niveaux de zoom. Le niveau 15 correspond bien à l'usage habituel sur le terrain, mais vous pouvez en sélectionner plusieurs selon votre usage.
5. Sélectionnez la zone qui vous intéresse sur la carte (dézoomez pour cela). La sélection rectangulaire est la plus simple à utiliser, mais vous pouvez aller dans le menu ``Sélection`` et choisir ``Mode de sélection > polygonal``, par exemple.
6. Dans la partie «&nbsp;Contenu de l'Atlas&nbsp;», donnez un nom à votre nouveau contenu puis cliquez sur ``Ajouter la sélection``.
7. Enfin dans le panneau de gauche, cliquez sur ``Créer l'Atlas``.
8. MOBAC télécharge alors les tuiles qui correspondent à votre sélection et les niveaux de zoom choisis.


{{< figure src="/images/Mobac1.png" title="Fig. 3. -- Créer un nouvel atlas" >}}

{{< figure src="/images/Mobac2.png" title="Fig. 4. -- Sélectionner une zone" >}}

{{< figure src="/images/Mobac3.png" title="Fig. 5. -- Niveaux de zoom, ajouter la sélection et créer l'atlas" >}}



N'oubliez pas un point important&nbsp;: plus votre sélection est étendue et plus vous avez de niveaux de zoom, plus le nombre de tuiles sera important et plus votre atlas pèsera lourd. Pour un ordre de grandeur, tout le massif des Vosges du Sud (Saverne en haut) au niveau de zoom 15 est contenu dans un fichier de 300 Mo.

Une autre solution, si vous disposez déjà du fichier GPX de la trace que vous planifiez, consiste à laisser MOBAC sélectionner automatiquement les tuiles autour de la zone en question. Pour cela&nbsp;:

1. Dans le menu de droite, ``Charger GPX``,
2. Sélectionner votre fichier et l'importer,
3. Dans le Menu ``Sélection`` choisir ``Sélectionner avec une trace GPX``.

Cela dit, d'un point de vue pratique, il est à mon avis plus simple de créer une fois pour toute un gros atlas sur tout un massif et que l'on place dans le smartphone, au lieu de répéter l'opération à chaque fois que l'on planifie un parcours.

### Transférer les atlas sur le dispositif mobile

Mais où donc sont trouvables les fichiers ainsi créés&nbsp;? En fait, après que MOBAC a fini de télécharger les tuiles, vous avez la possibilité de cliquer sur ``ouvrir le dossier des atlas``. Si, pour une raison ou une autre cela vous est impossible, cherchez le dossier ``\atlases`` dans votre ``\home`` sous GNU Linux ou ailleurs sur ``C:`` sous Windows. Rassurez-vous, il ne doit pas être bien loin.

Dedans vous trouverez&nbsp;:

- Le dossier de l'atlas si vous avez utilisé le format ``Oruxmaps Sqlite``. Là, ouvrez ce dossier et vous trouverez un (ou plusieurs) sous-dossier(s) correspondant à votre (ou vos) sélection(s). C'est ce sous-dossier qu'il faut placer dans votre smartphone dans le répertoire ``oruxmaps > mapfiles`` (ou là où vous avez spécifié via Oruxmaps l'emplacement du dossier des cartes, cf. la première partie de ce tutoriel). Par exemple, si vous avez créé un atlas ``Vosges`` et dedans deux sélections ``Vosges-du-nord`` et ``Vosges-du-sud``, il y a aura donc deux dossiers à copier et coller dans Oruxmaps, ``\Vosges-du-nord`` et ``\Vosges-du-sud``.
- Ou bien un fichier ``xxxx.sqlitedb`` si vous avez utilisé le format ``RMaps SQlite`` pour Maverick. Dans ce cas, c'est ce fichier qu'il faudra copier et coller dans le dossier ``Maverick > maps`` sur votre smartphone.

Après avoir débranché votre smartphone de l'USB de votre ordinateur, vous utilisez les applications comme suit&nbsp;:

- dans Maverick, menu ``Maps > more maps``, déselectionnez toutes les étoiles et sélectionnez la map que vous venez d'entrer.
- dans Oruxmaps, cliquez sur l'icône carte du menu du haut, puis ``Nouvelle carte > offline``et vous trouverez dans ``Multicartes`` le dossier OTRK que vous avez entré.

# Préparer vos parcours

Il reste maintenant à planifier un parcours. Pour cela il existe plusieurs solutions. Beaucoup d'utilisateurs se rabattent sur des services en ligne comme [Openrunner](https://www.openrunner.com/). Ce dernier propose des cartes (dont les cartes IGN) et un système de pointage permettant de tracer un parcours et l'enregistrer en GPX. 

D'autres solutions peuvent néanmoins être tout à fait utilisables localement avec des logiciels libres. On peut citer [Viking](https://sourceforge.net/p/viking/wikiallura/Main_Page/) et [QMapShack](http://www.qlandkarte.org/). Les deux sont présents dans les dépôts des distributions GNU Linux courantes.

Dans le cas de QMapShack, une opportunité intéressante est qu'on peut obtenir de la même manière avec MOBAC un fond de carte utilisable directement pour faire des traces. Nul besoin, par conséquent, d'utiliser un service en ligne. Voici comment faire.

A l'ouverture de QMapShack vous avez certainement remarqué un message bizarre&nbsp;: «&nbsp;Au secours&nbsp;! je veux des cartes&nbsp;! je n'ai pas envie de lire la documentation&nbsp;!&nbsp;». Bon... il fallait quand même lire un petit peu alors je vous livre mes résultats.

Dans la partie haute du panneau de gauche de QMapShack, vous pouvez activer ou désactiver des fonds de carte (clic droit). Mais un clic droit dans ledit panneau vous permet d'accéder à un menu ``Configurer les répertoires des cartes``. Il vous permet de préciser un répertoire dans lequel le logiciel peut aller chercher des fonds de cartes.

Oui, mais quelles cartes&nbsp;? Hé bien celles que vous pouvez créer avec MOBAC exactement comme je l'ai décrit plus haut. Sauf que cette fois, au moment de créer un nouvel atlas, vous préciserez le format ``TwoNav (RMAP)``.

MOBAC créera alors un dossier portant le nom de l'atlas, avec, dedans, le fichier résultat du travail. Puis dans QMapShack&nbsp;:

- lancer la fonctionnalité ``Configurer les répertoires des cartes``,
- cliquer sur ``+`` et pointez le *dossier* que Mobac a créé (dans ``\atlases``) ou copier-coller le dossier dans un autre répertoire de votre choix et pointez dessus.

Vous trouverez alors, dans la partie haute du panneau de gauche de QMapShack le nom du fond de carte qu'il vous restera à activer pour le voir s'afficher.

Le reste est un jeu d'enfant&nbsp;: 

- cliquez sur l'icône ``Routage`` et donnez un nom à votre projet,
- clic droit sur la carte et ``Ajouter une trace``,
- faites votre tracé et enregistrez,
- puis ``Fichier > Enregistrer toutes les données SIG`` (et sauvegardez au format GPX).

Vous voilà avec une trace qu'il vous reste à transférer sur votre dispositif mobile pour Oruxmaps ou Maverick, dans le dossier correspondant, comme précisé plus haut.

Vous pouvez faire les mêmes opérations avec Viking. De la même manière vous importez votre trace GPX pour la visualiser.


{{< figure src="/images/qmapshackway.png" title="Fig. 6 -- Tracer un parcours avec QMapShack" >}}


# Stocker et gérer vos traces

Cette partie sera plus courte car il s'agit simplement de signaler l'existence de quelques logiciels utiles.

Le premier est [Turtlesport](http://turtlesport.sourceforge.net/), qui vous permet de stocker vos traces (y compris depuis un dispositif GPS de type Garmin directement). Mais, au-delà, il vous permet de tenir le compte de vos performances et autres informations sur votre matériel sportif. Il agit exactement comme un carnet de bord de vos sorties.

Le second est [Mytourbook](http://mytourbook.sourceforge.net/mytourbook/). Il s'apparente au précédent avec un peu plus de gadgets.

Pour partager vos traces avec vos amis, vous pouvez aussi penser à [Umap](https://umap.openstreetmap.fr/fr/) et [Framacarte](https://framacarte.org/fr/) (le second est une instance du premier).

Je vous convie à consulter [ce billet](https://golb.statium.link/post/20160617logiciellibrevtttrailrando/). Il est un peu ancien mais complétera utilement ce qui vient d'être dit ici.









