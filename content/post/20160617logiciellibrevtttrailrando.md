---
title: "Cross-country, VTT, Trail : 8 logiciels libres"
date: 2016-06-17
author: "Christophe Masutti"
image: "/images/postimages/terminal.png"
description: "Ami traileur, biker, ou randonneur, toi le geek sportif, regarde plutôt cette petite sélection de logiciels libres, rien que pour toi.
"
tags: ["Sport", "VTT", "Logiciel libre", "Parcours", "cartographie"]
categories:
- Sport
- Logiciel libre
---


En matière de logiciel libres, les sportifs ont plutôt l'embarras du choix. C'est à se demander pourquoi, lorsqu'on veut récupérer des traces, on doive naviguer entre divers sites issus des constructeurs de montres GPS (pas toujours bien intentionnés).  Et c'est sans compter la page ouèb du trail de Brouzouf-le-haut qui oblige les concurrents à se connecter sur Facebook et Google pour voir les caractéristiques du parcours. Ami traileur, biker, ou randonneur, toi le geek sportif, regarde plutôt cette petite sélection de logiciels libres, rien que pour toi.

## Quels logiciels ?

J'en ai encore fait l'expérience avec le site internet Tracedetrail.fr. Certes, le site est plutôt bien fichu, avec la possibilité de partager ses traces en les accompagnant des différentes données (altitudes, cumuls de dénivelés au différents points du parcours, etc) mais il faut ouvrir un compte pour télécharger les traces ou récupérer des données, quant à confier systématiquement mes données à un tiers, c'est un principe&nbsp;: je n'aime pas.

En réalité ce type de site ne repose que sur un principe relativement simple&nbsp;: stocker et partager des fichiers GPS. Si on a sur sa machine, en local, des logiciels fiables pour les lire, il n'y a aucune raison d'ouvrir un compte chez Monsieur Dupont, si ce n'est que pour sacrifier à la mode du moment. De plus, on ne dispose pas toujours d'une connexion internet&nbsp;: en vacances au fond d'une vallée perdue pour s'adonner à la pratique du trail, il peut toujours être utile d'analyser ses parcours sans devoir les téléverser quelque part.

Les logiciels spécialisés dans les relevés GPS ont deux caractéristiques&nbsp;: les fonds de cartes proposés (à minima, la possibilité d'utiliser ses propres fonds de cartes), et la capacité d'analyser un maximum de données issues des fichiers (GPX, KML, etc) qu'on utilise, en particulier si les fichiers contiennnent des données autres que purement géographiques comme par exemple les donnnées d'un cardiofréquencemètre. À cela s'ajoutent des fonctionnalités supplémentaires appréciables&nbsp;:


- pouvoir se connecter à un dispositif GPS (selon les marques, en particulier Garmin), même si l'on peut d'abord chercher les fichiers sur le dispositif puis les utiliser avec le logiciel,
- stocker les informations, comparer les données avec des graphiques,
- éditer des traces existantes ou en créer de nouvelles pour préparer un parcours,
- ajouter des informations en fonction de l'activité (course à pied, cyclisme, trail, randonnée, etc.)


## Des logiciels libres

Ce qui me motive dans le trail, c'est avant tout la sensation de grande liberté que procure cette discipline. Si l'on compare avec la course à pied classique ou le cross-country, les contraintes de distances et de temps sont largement repoussées, on se confronte à des terrains aux multiples caractéristiques, les épreuves mélangent le plus souvent les amateurs et les pros, et ce qui compte avant tout, c'est de pouvoir terminer un trail, et pas forcément «&nbsp;faire un temps&nbsp;».

Je trouve que la pratique du trail est complètement opposée aux contraintes des logiciels privateurs ou de services distants qui imposent une rétention de données ou dont les conditions d'utilisation impliquent l'emploi des mes informations personnelles à des fins commerciales. Les assurances et même certaines mutuelles profitent déjà de la manne providentielles des dispositifs de santé connectés (cf. les machins-fit), et l'on présage déjà très bien les inégalités sociales que cela pourra créer à l'avenir. Hors de question, donc, d'utiliser des logiciels privateurs, ou des services tiers dont les intentions ne sont pas conformes à mon éthique.

L'utilisation de [logiciels libres](https://fr.wikipedia.org/wiki/Logiciel_libre) n'implique pas pour autant de devoir galérer des heures avec des interfaces difficiles. Les logiciels présentés ci-dessous sont certes très spécialisés, et nécessitent un minimum d'apprentissage. Cependant, chacun d'entre eux permet de visualiser rapidement un profil de trace&nbsp;: le travail des données n'est ensuite qu'une question d'habitude.

Tous ces logiciels proposent des versions pour les trois systèmes d'exloitation les plus courants&nbsp;: GNU/Linux, MacOs et MSWindows. Concernant GNU/Linux, la plupart sont disponibles dans les dépôts des principales distributions, à l'exception de MyTourbook et Turtlesport.

## GPSbabel

Il serait difficile de commencer une liste de logiciels censés lire des fichiers contenant des données GPS sans mentionner [GPSBabel](http://www.gpsbabel.org/index.html). Ce n'est pas un logiciel indispensable, étant donné que, souvent, les logiciels sont capables de converser avec les dispositifs GPS courants, et que ces derniers fournissent des fichiers aux formats la plupart du temps lisibles. Cependant, pour assurer des conversions entre ces différents formats de fichiers, GPSBabel est d'une grande utilité.

## Viking

Le logiciel [Viking](https://sourceforge.net/p/viking/wikiallura/Main_Page/) est avant tout un visualiseur de données GPS. Il permet de les afficher sur un fond de carte, les archiver et les classer. Viking fonctionne par calques&nbsp;: il suffit d'afficher la carte voulue (disponible soit en local soit en ligne, via, différents [Web Map Services](https://fr.wikipedia.org/wiki/Web_Map_Service)), puis ouvrir un fichier de trace GPS pour le voir affiché sur la carte. Il est aussi possible de réaliser un tracé avec mesure de distances grâce aux outils graphiques disponibles.

![Viking](/images/viking.png)

## GPXviewer

Dans la même ligne que Viking, mais de manière encore plus simple, Gpxviewer permet d'afficher une trace GPX sur un fond de carte. Disponible dans la plupart des dépôts GNU/Linux, il s'agit d'un programme très simple, dont les équivalents ne manquent pas sur les autres systèmes d'exploitation.

![GpxViewer](/images/gpxviewer.png)

## QlandKarteGT

Bien que disponible dans les dépôts des distributions GNU/Linux, QlandKarteGT est en fait aujourd'hui obsolète, il est remplacé par son sucesseur Qmapshack (voir sur le <a href="http://www.qlandkarte.org/">site officiel</a>). Néanmoins, les fonctionnalités sont déjà très poussées pour cette version d'un projet logiciel très dynamique. QlandKarteGT est un outil très polyvalent, et dispose même d'un rendu 3D des parcours sur carte. On peut noter la présence (au moins dans les dépôts) de QlandKarteGT-Garmin qui fourni des greffons pour communiquer avec différents dispositifs Garmin. Cependant, QlandKarteGT n'est pas limité à Garmin, bien sûr.  Un autre atout de QlandKarteGT est de proposer d'emblée des fonds de cartes issus de nombreux [Web Map Services](https://fr.wikipedia.org/wiki/Web_Map_Service)) européens, et, parmi ceux-ci, les fonds IGN Topo 25. Les fonctionnalités de QlandKarteGT peuvent se limiter à l'import/export et la visualisation, mais elles peuvent être très poussées et se confondre avec un véritable outil de système d'information géographique.



## Turtlesport

On ne présente plus [Turtlesport](http://turtlesport.sourceforge.net), LE logiciel qu'il faut avoir si on fait du trail, de la course à pied, du vélo&hellip; En un coup d'oeil vous pouvez avoir un rapport d'activité des sessions d'entraînement, l'affichage de votre parcours sur un fond de carte (configurable), et le tout en personnalisant les données de l'athlète (taille, poid, équipement, vitesse, fréquence cardiaque, etc.). C'est sans doute dans cette liste le logiciel le plus polyvalent, capable de communiquer avec un grand nombre de dispositifs GPS/cardio, et disposant d'une interface agréable et facile à prendre en main (pour utiliser le fond de carte OpenTopoMap avec Turtlesport, voyez le petit tutoriel consacré sur ce blog).



## Mytourbook

Moins connu, peut-être, que Turtlesport, [MytourBook](http://mytourbook.sourceforge.net/mytourbook/) est prévu pour analyser, représenter, comparer, et documenter des traces GPS. Il est donc adapté à la randonnée, le VTT ou le trail. On peut ajouter des photos et les lier au parcours, visualiser les dénivelés et analyser les vitesses, récupérer des graphes, des analyses statistiques. Il s'agit d'un programme tournant avec java, mêlant les licences Eclipse et GPL (un peu de concession, parfois). Turtlesport et Mytourbook sont comparables, avec une mention spéciale pour le second concernant la visualisation graphique des différentes données des traces (il faut toutefois s'habituer à l'interface).

![MyTourBook](/images/Mytourbook.png)

## Framacarte et Umap

Enfin, il aurait été dommage de ne pas mentionner au moins deux possibilités de partager ses traces en ligne via des services non seulement libres mais aussi éthiques et décentralisateurs. Comme je l'ai dit précédemment, la meilleure façon de partager ses traces GPS est encore de partager les fichiers sans vouloir absolument les téléverser auprès de services tiers. Pourtant, vous pouvez avoir besoin de partager des traces en donnant directement à voir une carte, la rendre disponible sur votre site web ou tout simplement parce que vos correspondants ne disposent pas forcément d'un logiciel pour lire ce type de fichier.

[Umap](https://umap.openstreetmap.fr/fr/) est un projet OpenStreetMap (le Wikipédia de la cartographie)  qui vous permet d'éditer et partager des traces de manière très rapide, en quelques clics. Voyez ce [petit tutoriel](https://docs.framasoft.org/fr/umap/). Mais Umap est bien plus que cela&nbsp;! C'est aussi la possibilité, pour vous, votre club, votre association, de disposer de votre instance Umap sur un serveur et partager vos traces en conservant vos données là où vous le voulez. En guise de démonstration, [Framacarte](https://framacarte.org/fr/) est un service de [Framasoft](http://framasoft.org/) qui montre qu'il est possible d'installer une instance Umap et la proposer à qui vous voulez. Vous pouvez utiliser utiliser l'instance Umap d'OSM ou de Framasoft en toute sécurité pour vos données, bien sûr, mais n'oubliez pas qu'il est possible (et même souhaitable) de créer votre propre instance.

![Framacarte / Umap](/images/carte-trailframacarte.jpg)

