---
title: "Demain, tous en garde à vue?"
date: 2015-01-08
author: "Christophe Masutti"
image: "/images/postimages/say.png"
tags: ["Libres propos", "Libertés", "Terrorisme", "Politique"]
description: "Le paradoxe de la peur appliqué aux outils (communicationnels) de la liberté d'expression"
categories:
- Libres propos
---




Cet article a été publié (et remanié) sur le célèbre Framablog à  [cette adresse](http://www.framablog.org/index.php/post/2015/01/11/patriot-act-francais-pour-nous-non).

## Sale temps

Hier, c'était mon anniversaire, j'ai 40 ans. Hé bien, des jours comme ça, je m'en serais passé.

Hier, c'était la barbarie&nbsp;: c'est Cabu, Charb, Wolinksi, Tignous, Honoré, et d'autres acteurs de Charlie Hebdo (sans oublier les policiers) qui ont été lâchement assassinés dans les locaux du journal par deux gros cons, des beaufs, des salauds. Ils s'en sont pris à des dessinateurs qui ont largement contribué à la formation de ma propre pensée critique à travers la lecture régulière du journal. Des copains, nos copains.

Ce matin, j'ai la tête en vrac. J'ai à l'esprit ces mots de Ricoeur qui définissait la démocratie par le degré de maturation d'une société capable de faire face à ses propres contradictions et les intégrer dans son fonctionnement. Au centre, la liberté d'expression, outil principal de l'exercice de la démocratie. À travers nos copains assassinés, c'est cette liberté qui est en jeu aujourd'hui. Ils exerçaient cette liberté par le crayon et le papier. L'arme absolue.

Charlie Hebdo n'a pas vocation à incarner de grands symboles, au contraire, les dénoncer et faire tomber les tabous est leur principale activité. C'est justement parce que la mort de dessinateurs est aujourd'hui devenue un symbole qu'il va falloir s'en méfier, car dans cette brèche s’engouffrent les tentatives protectionnistes et liberticides.

La liberté est insupportable pour les pseudos-religieux sectaires&nbsp;— et pour tout dire, une grosse bande de connards&nbsp;— qui tentent de la faire taire à grands coups de Kalachnikov et de bombes sournoises. La liberté est insupportable pour les fachos et autres réacs qui ne manqueront pas de s'engouffrer dans le piège grossier du repli et de la haine. La liberté est insupportable pour celui qui a peur.

Nous vivons depuis des années sous le régime des plans vigipirate, des discours sécuritaires et du politiquement correct. Sous couvert de lutte contre le terrorisme, la surveillance généralisée de nos moyens de communication s'est taillé une belle part de nos libertés, sans oublier les entreprises qui font leur beurre en vendant aux États (et pas toujours les plus démocratiques) des « solutions » clé en main. Des lois liberticides au nom de l'antiterrorisme sont votées [sans réel examen approfondi](http://www.numerama.com/magazine/31270-la-loi-anti-terrorisme-promulguee-sans-controle-constitutionnel.html) par le Conseil Constitutionnel. En guise de contre-pouvoir, on nous refourgue généralement des administrations fantoches aux pouvoirs ridicules, des «&nbsp;Conseils&nbsp;» et des «&nbsp;Hauts Comités&nbsp;» de mes deux. Mais le vrai contre-pouvoir, ce sont les copains de Charlie Hebdo et tous leurs semblables, journalistes ou caricaturistes, qui l’exercent, ou plutôt qui le formalisent pour nous, à travers leurs dessins et leurs textes. Le contre-pouvoir, c'est nous tous tant que nous n'oublions pas de penser et d'exprimer nos contradictions. Et pour maintenir la démocratie, nous devons disposer intégralement de nos moyens de communication dont il revient à l'État de garantir la neutralité.

Demain, nous risquons de nous retrouver tous en garde à vue et pas seulement à cause des terroristes. C'est là tout le paradoxe. La terreur est aussi bien instrumentalisée par les assassins que par certains membres de la classe politique, et pas seulement à droite. Tous sont prêts à réprimer notre liberté pour maintenir leurs intérêts électoraux, ou d'autres intérêts financiers. Leur contrainte, c'est l'obligation du choix&nbsp;: il faudrait choisir entre la liberté et la dictature, entre la liberté et la peur, entre la liberté et l'esclavage, avec à chaque fois un peu de nos libertés qui s'envolent.

Non&nbsp;! Assez&nbsp;! Stop&nbsp;! je suis pour la liberté mais sans concession. Une liberté obligatoire, une liberté que l'on assène sans contrepartie. Je suis un radical du papier, un ayatollah de la liberté d'expression, un taliban des communications ouvertes, un nazi des protocoles informatiques libres, un facho de la révélation snowdenienne&nbsp;! Du moins je voudrais l'être, nous devrions tous l'être. Et sans avoir peur.

Je suis né il y a 40 ans, et cela fait presque autant de temps que se sont développés autour de moi des supports de communication qui sont autant de moyens d'exercices de la liberté d'expression. Comme beaucoup, j'oublie souvent que rien n'est acquis éternellement, que nos libertés sont le fruit de luttes permanentes contre ceux qui voudraient nous en priver. La boucherie Charlie nous l'a cruellement rappelé hier.
