---
title: "Clefs publiques"
date: 2012-01-01
lastmod: 2022-05-31
author: "Christophe Masutti"
tags: ["Occupations"]
description: "Des clefs pour communiquer"
categories:
- Libres propos
---

## Clefs à télécharger

- Clef publique pour affaires Framasoft et libertés numériques : [clepubliqueframasoftframatophe.asc](https://nuage.masutti.name/s/JqprnMFWpRdLfTY)

- Clef publique personnelle (pour messages personnels) : [clepubliquepersochristophe.asc](https://nuage.masutti.name/s/feLmSaQjjTCHHBJ)

## Explications

Voir [cette page Wikipedia](https://fr.wikipedia.org/wiki/Cryptographie_asym%C3%A9trique)

<!--
{{< figure src="/images/Chiffrement_asymetrique.jpg" title="Parcours simplifié d'un message chiffré" >}}
-->

