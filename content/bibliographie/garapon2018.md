---
title: "Justice digitale"
subtitle: "Révolution graphique et rupture anthropologique"
date: 2018-04-20
image: "/images/biblio/Antoine-Garapon.jpg"
author: "Christophe Masutti"
description: "Garapon, Antoine, et Jean Lassègue. Justice digitale: révolution graphique et rupture anthropologique. PUF, 2018."
type: bibliographie
categories: ["bibliographie"]
auteurs: ["Garapon", "Lassègue"]
---

Remplacement des avocats par des robots, disparition des notaires, résolution des conflits en ligne, justice prédictive, état civil tenu par la blockchain, généralisation des contrats en bitcoins échappant à tout contrôle (et à toute taxation) : le numérique n'en finit pas de bouleverser la justice en inquiétant les uns et en enthousiasmant les autres. Plutôt que de proposer un bilan de ces innovations, nécessairement prématuré, ce livre tente de situer l'épicentre anthropologique d'une déflagration provoquée par l'apparition d'une nouvelle écriture qu'il faut bien désigner comme une révolution graphique. La justice digitale alimente un nouveau mythe, celui d'organiser la coexistence des hommes sans tiers et sans loi par un seul jeu d'écritures, au risque d'oublier que l'homme est un animal politique.

----

Garapon, Antoine, et Jean Lassègue. *Justice digitale. Révolution graphique et rupture anthropologique*. PUF, 2018.


**Lien vers le site de l'éditeur :** https://www.puf.com/content/Justice_digitale

----
