---
title: "L’avenir des idées"
subtitle: "Le sort des biens communs à l’heure des réseaux numériques"
date: 2005-02-01
image: "/images/biblio/lawrence-lessig-avenir-idees.jpg"
author: "Christophe Masutti"
description: "Lessig, Lawrence. L’avenir des idées: le sort des biens communs à l’heure des réseaux numériques, Presses universitaires de Lyon, 2005."
type: bibliographie
categories: ["bibliographie"]
auteurs: ["Lessig"]
---

L'hostilité de Lawrence Lessig à l'égard des dérives monopolistiques et des excès de la réglementation, notamment celle du droit d'auteur, ne se fonde pas sur des présupposés idéologiques, mais sur une analyse précise, illustrée par de nombreuses études de cas, des conséquences catastrophiques pour l'innovation et la créativité que ne manqueront pas d'avoir les évolutions récentes de l'architecture d'Internet. De plus en plus fermée, propriétarisée et centralisée, celle-ci est en train de stériliser la prodigieuse inventivité à laquelle l'Internet a pu donner lieu à ses débuts. Historien scrupuleux des trente années de développement de ce moyen de communication interactif, d'échange de connaissances, de création de richesses intellectuelles sans précédent, Lawrence Lessig pose le problème en juriste, mais aussi en philosophe et en politique. C'est une certaine idée du partage des savoirs et de la création artistique qui est en jeu dans les tendances actuelles qui dénaturent les principes démocratiques de l'Internet originel. Cette étude parfaitement documentée est aussi un pressant cri d'alarme.

----

Lessig, Lawrence. L’avenir des idées: le sort des biens communs à l’heure des réseaux numériques, Presses universitaires de Lyon, 2005. 



**Lien vers le site de l'éditeur :** http://presses.univ-lyon2.fr/produit.php?id_produit=731


----
