---
title: "Algorithmes: la bombe à retardement"
date: 2018-08-03
image: "/images/biblio/Cathy-ONeil.jpg"
author: "Christophe Masutti"
description: "O’Neil, Cathy. Algorithmes: la bombe à retardement, les Arènes, 2018."
type: bibliographie
categories: ["bibliographie"]
auteurs: ["O'Neil"]
---

Qui choisit votre université ? Qui vous accorde un crédit, une assurance, et sélectionne vos professeurs ? Qui influence votre vote aux élections ? Ce sont des formules mathématiques. Ancienne analyste à Wall Street devenue une figure majeure de la lutte contre les dérives des algorithmes, Cathy O'Neil dévoile ces « armes de destruction mathématiques » qui se développent grâce à l'ultra-connexion et leur puissance de calcul exponentielle. Brillante mathématicienne, elle explique avec une simplicité percutante comment les algorithmes font le jeu du profit. Cet ouvrage fait le tour du monde depuis sa parution. Il explore des domaines aussi variés que l'emploi, l'éducation, la politique, nos habitudes de consommation. Nous ne pouvons plus ignorer les dérives croissantes d'une industrie des données qui favorise les inégalités et continue d'échapper à tout contrôle. Voulons-nous que ces formules mathématiques décident à notre place ? C'est un débat essentiel, au cœur de la démocratie.


----

O’Neil, Cathy. *Algorithmes: la bombe à retardement*, les Arènes, 2018.


**Lien vers le site de l'éditeur :** http://www.arenes.fr/livre/algorithmes-la-bombe-a-retardement/

----
