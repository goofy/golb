---
title: "Utopie du logiciel libre"
date: 2018-03-31
image: "/images/biblio/Sebastien-Broca.jpg"
author: "Christophe Masutti"
description: "Broca, Sébastien. Utopie du logiciel libre. Éditions le Passager clandestin, 2018"
categories: ["bibliographie"]
auteurs: ["Broca"]
---

Né dans les années 1980 de la révolte de hackers contre la privatisation du code informatique, le mouvement du logiciel libre a peu à peu diffusé ses valeurs et ses pratiques à d’autres domaines, dessinant une véritable « utopie concrète ». Celle-ci a fait sienne plusieurs exigences : bricoler nos technologies au lieu d’en être les consommateurs sidérés, défendre la circulation de l’information contre l’extension des droits de propriété intellectuelle, lier travail et accomplissement personnel en minimisant les hiérarchies. De GNU/Linux à Wikipédia, de la licence GPL aux Creative Commons, des ordinateurs aux imprimantes 3D, ces aspirations se sont concrétisées dans des objets techniques, des outils juridiques et des formes de collaboration qui nourrissent aujourd’hui une nouvelle sphère des communs. Dans cette histoire du Libre, les hackers inspirent la pensée critique (d’André Gorz à la revue Multitudes) et les entrepreneurs open source côtoient les défenseurs des biens communs. De ce bouillonnement de pratiques, de luttes et de théories, l’esprit du Libre émerge comme un déjà là propre à encourager l’inventivité collective. Mais il est aussi un prisme pour comprendre comment, en quelques décennies, on est passé du capitalisme de Microsoft – la commercialisation de petites boîtes des biens informationnels protégés par des droits de propriété intellectuelle – au capitalisme numérique des Gafa (Google, Amazon, Facebook, Apple), fondé sur l’exploitation de nos données et la toute puissance des algorithmes.

----

Broca, Sébastien. *Utopie du logiciel libre*. Éditions le Passager clandestin, 2018.

**Lien vers le site de l'éditeur :** http://lepassagerclandestin.fr/catalogue/poche/utopie-du-logiciel-libre.html

- [Lire le PDF](http://lepassagerclandestin.fr/fileadmin/assets/catalog/essais/Utopie_logiciel_libre__Broca__Le_passager_clandestin.pdf)
----
