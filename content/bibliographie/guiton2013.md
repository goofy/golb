---
title: "Hackers : au cœur de la résistance numérique"
date: 2013-04-20
image: "/images/biblio/amaelle-guiton.jpg"
author: "Christophe Masutti"
description: "Guiton, Amaelle. Hackers: au cœur de la résistance numérique. Au diable Vauvert, 2013."
type: bibliographie
categories: ["bibliographie"]
auteurs: ["Guiton"]
---

Communiquer, partager, s’informer librement : c’était l’utopie des pionniers du Net. Quarante ans après ses premiers balbutiements, les gouvernements et les grands acteurs privés contrôlent toujours plus étroitement les échanges, rongent liberté d’expression et droit à la vie privée. Le Réseau est une extension du domaine de la lutte politique.Ils sont nés avec un ordinateur dans les mains, ont grandi sur la Toile, connaissent tous les avantages et les pièges de la vie en ligne. Ils ont soutenu WikiLeaks et les cyberdissidents des printemps arabes, se sont mobilisés contre les lois sécuritaires, exfiltrent des témoignages de répression, échangent avec les Indignés du monde entier. Ils créent des réseaux alternatifs. On les retrouve jusque dans les Parlements européens. Ils réinventent la politique. Amaelle Guiton a interviewé ceux qui, sous le masque Anonymous ou à découvert, sont les artisans d’un Internet libre. Elle livre une enquête passionnante au cœur de la résistance numérique, pour savoir ce que « hacker » veut dire.

----

Guiton, Amaelle. *Hackers: au cœur de la résistance numérique*. Au diable Vauvert, 2013.


**Lien vers le site de l'éditeur :** https://audiable.com/boutique/cat_document/hackers/

----
