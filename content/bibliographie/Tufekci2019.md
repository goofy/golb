---
title: "Twitter et les gaz lacrymogènes"
subtitle: "Forces et fragilités de la contestation connectée"
date: 2019-10-21
image: "/images/biblio/zynep-tufekci.jpg"
author: "Christophe Masutti"
description: "Tufekci, Zeynep. Twitter et les gaz lacrymogènes. Forces et fragilités de la contestation connectée. C&F éditions, 2019."
type: bibliographie
categories: ["bibliographie"]
auteurs: ["Tufekci"]
---

Les mouvements sociaux à travers le monde utilisent massivement les technologies numériques. Zeynep Tufekci était présente sur la place Tahrir et en Tunisie lors des printemps arabes, à Istanbul pour la défense du parc Gezi, dans les rues de New York avec Occupy et à Hong-Kong lors du mouvement des parapluies. Elle y a observé les usages des téléphones mobiles et des médias sociaux et nous en propose ici un récit captivant. Les réseaux numériques permettent de porter témoignage et d'accélérer les mobilisations. Ils aident les mouvements à focaliser les regards sur leurs revendications. Cependant, l'espace public numérique dépend des monopoles de l'économie du web. Leurs algorithmes, choisis pour des raisons économiques, peuvent alors affaiblir l'écho des contestations. Au delà de leur puissance pour mobiliser et réagir, faire reposer la construction des mouvements sur ces technologies fragilise les organisations quand il s'agit de les pérenniser, quand il faut négocier ou changer d'objectif tactique. De leur côté, les pouvoirs en place ont appris à utiliser les médias numériques pour créer de la confusion, de la désinformation, pour faire diversion, et pour démobiliser les activistes, produisant ainsi résignation, cynisme et sentiment d'impuissance. Une situation qui montre que les luttes sociales doivent dorénavant intégrer dans leur stratégie les enjeux de l'information et de la communication aux côtés de leurs objectifs spécifiques.


----

Tufekci, Zeynep. *Twitter et les gaz lacrymogènes. Forces et fragilités de la contestation connectée*. C&F éditions, 2019.




**Lien vers le site de l'éditeur :** https://cfeditions.com/lacrymo/


----
