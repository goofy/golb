---
title: "Le travail invisible des données"
subtitle: "Éléments pour une sociologie des infrastructures scripturales"
date: 2018-03-31
image: "/images/biblio/Jerome-Denis.jpg"
author: "Christophe Masutti"
description: "Denis, Jérôme, et Delphine Gardey. Le travail invisible des données. Éléments pour une sociologie des infrastructures scripturales. Presses des Mines - Transvalor, 2018."
type: bibliographie
categories: ["bibliographie"]
auteurs: ["denis", "gardey"]
---

Ouvertes, massives, brutes... les données sont aujourd'hui au coeur de nombreux débats. Les optimistes y voient une ressource naturelle dont la récolte et la circulation sont en passe de révolutionner l'innovation et la démocratie, tandis que les pessimistes les dépeignent comme le carburant de mécanismes qui ne profiteront qu'aux puissants et renforceront les inégalités. Face aux enthousiasmes et aux affolements, face au vocabulaire de la transparence, de la fluidité et de l'automatisation qu'ils mobilisent, ce livre fait un pas de côté et défend la nécessité d'étudier les modalités concrètes de la production et de la circulation des données. Les données ne tombent en effet jamais du ciel. Elles n'affleurent pas non plus sous le sol des organisations. En amont de leurs traitements si prometteurs ou inquiétants, elles font l'objet d'un travail dont la nature, l'organisation et les processus mêmes qui mènent à son invisibilité restent à explorer. En articulant les apports de la sociologie des sciences et des techniques, de l'histoire, de l'anthropologie de l'écriture, de la sociologie du travail et des accounting studies, l'ouvrage compose un outillage conceptuel et méthodologique original pour interroger au plus près ce travail des données, qui est devenu central dans les entreprises et les administrations à partir de la fin du XIX<sup>e</sup> siècle.

----

Denis, Jérôme, et Delphine Gardey. *Le travail invisible des données. Éléments pour une sociologie des infrastructures scripturales*. Presses des Mines - Transvalor, 2018. 

**Lien vers le site de l'éditeur :** https://www.pressesdesmines.com/produit/le-travail-invisible-des-donnees/

----
