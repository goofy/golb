---
title: "Technologies numériques : en finir avec le capitalisme de surveillance"
date: 2019-02-01
image: "/images/biblio/larevuedurable.jpg"
author: "Christophe Masutti"
description: "La Revue durable. Technologies numériques : en finir avec le capitalisme de surveillance. 2019"
type: bibliographie
categories: ["bibliographie"]
auteurs: ["Stallman", "Zuffery"]
---

Les hommes sont nés libres. Ils sont libres, par exemple, d'affronter l'urgence écologique.

Mais cela est d'autant plus difficile que de puissantes forces entravent leur capacité à exercer cette liberté. Pour mettre en œuvre des choix éthiques et constructifs, il faut un arrière-fond culturel et des institutions guidées par de hauts objectifs, que le capitalisme de surveillance s'emploie à freiner.

Les exemples proposés dans ce soixante-troisième dossier montrent les impacts écologiques et sociétaux du numérique et leur effrayante évolution, et en dévoilent les enjeux cachés : les internautes, croyant profiter de services gratuits, sont en réalités passés du statut de clients à celui de marchandise. Chaque empreinte digitale laissée est exploitée pour anticiper et stimuler les réactions neurobiologiques de chaque public dans le but de le rendre dépendant, de le pousser à la surconsommation et influencer tous ses choix, au point de mettre en péril la démocratie.

Les alternatives existantes y sont également présentées, grâce aussi à une riche interview avec Richard Stallman, président de la Free Sotfware Foundation et créateur de GNU, ainsi que les actions concrètes entreprises par les Artisans de la transition et LaRevueDurable dans cette direction.

Une interview de Michelle Zuffery, secrétaire permanente d'Uniterre, enrichit en outre ce numéro, faisant le lien avec le nouveau projet des Artisans de la transition dans le domaine de l'agroécologie : construire et animer un réseau d'acteurs de tous horizons pour faire avancer les choses.

Au sommaire de ce numéro figure également un minidossier sur la désobéissance civile, ces actions non violentes visant à attirer l'attention d'une majorité sur la problématique écologique. Un sujet très actuel à l'heure où certains citoyens estiment que la politique ne réagit pas suffisamment face à l'urgence climatique.

----

La Revue durable. *Technologies numériques : en finir avec le capitalisme de surveillance*. num. 63. 2019


**Lien vers le site de l'éditeur :** https://www.larevuedurable.com/fr/democratie-et-gouvernance/1237-n63-technologies-numeriques-en-finir-avec-le-capitalisme-de-surveillance.html

----
