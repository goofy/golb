---
title: "Red Mirror"
subtitle: "L'avenir s'écrit en Chine"
date: 2021-02-09
image: "/images/biblio/red-mirror.jpg"
author: "Christophe Masutti"
description: "Pieranni, Simone. Red Mirror. L'avenir s'écrit en Chine. CF Editions, 2021."
type: bibliographie
categories: ["bibliographie"]
auteurs: ["Zuboff"]
---

La Chine a longtemps été considérée comme « l'usine du monde » fabriquant pour l'Occident, grâce à sa main d'oeuvre surexploitée, les biens de consommation puis les objets technologiques conçus dans la Silicon Valley.

Cette période est révolue : en développant massivement recherche, éducation et investissements, la Chine est devenue leader dans le domaine des technologies. Intelligence artificielle, villes intelligentes, paiement via les smartphones, surveillance et reconnaissance faciale sont déjà des réalités de l'autre côté de la Grande muraille numérique.

L'avenir s'écrit dorénavant en Chine. Mais quel avenir ?

Les stratégies géopolitiques de Xi Jinping, l'organisation du contrôle social et l'acceptation confucéenne de la surveillance personnalisée par le plus grand nombre sont le moteur de ce développement à marche forcée. Et ouvre la porte d'un monde qui ressemble déjà à la série dystopique dont s'inspire le titre de cet ouvrage.

Un regard lucide sur la place du numérique dans la Chine d'aujourd'hui, écrit par un journaliste qui y a vécu longtemps et qui continue de suivre les évolutions rapides des industries de pointe. Alors que les équilibres mondiaux changent, le récit de Simone Pieranni donne des clés essentielles pour comprendre la nouvelle situation.

----

Pieranni, Simone. *Red Mirror. L'avenir s'écrit en Chine*. CF Editions, 2021.


**Lien vers le site de l'éditeur :** [https://cfeditions.com/red-mirror/](https://cfeditions.com/red-mirror/)


----
