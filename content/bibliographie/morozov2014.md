---
title: "Pour tout résoudre, cliquez ici !"
subtitle: "L’aberration du solutionnisme technologique"
date: 2014-02-02
image: "/images/biblio/Evgeny-Morozov.jpg"
author: "Christophe Masutti"
description: "Morozov, Evgeny. Pour tout résoudre, cliquez ici ! L’aberration du solutionnisme technologique. Traduit par Marie-Caroline Braud, Fyp, 2014.."
type: bibliographie
categories: ["bibliographie"]
auteurs: ["Morozov"]
---

*Pour tout résoudre cliquez ici !* dénonce le discours employé par les entreprises et les chantres de la Silicon Valley qui veulent nous faire croire que grâce à l’Internet et aux nouvelles technologies tous les aspects de notre vie seront améliorés et la plupart des problèmes du monde disparaîtront.

Evgeny Morozov démontre qu’il n’y a pas une « application » comme réponse simple et immédiate à tous les enjeux sociétaux ni même à nos problèmes individuels. Il met en lumière deux concepts-clés, le solutionnisme et « l’Internet-centrisme », qui permettent de comprendre les schémas de pensée à l’œuvre derrière la révolution numérique.

Cet ouvrage porte un regard neuf et salutaire sur le numérique et sur nos usages. Il nous met en garde contre la croyance en un miracle technique et en un un monde à l’efficacité sans faille où chacun serait contraint de revêtir la camisole de force numérique de la Silicon Valley.

----

Morozov, Evgeny. *Pour tout résoudre, cliquez ici ! L’aberration du solutionnisme technologique*. Traduit par Marie-Caroline Braud, Fyp, 2014.


**Lien vers le site de l'éditeur :** http://www.fypeditions.com/resoudre-laberration-du-solutionnisme-technologique-evgeny-morozov/


----
