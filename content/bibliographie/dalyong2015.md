---
title: "Digital Platforms, Imperialism and Political Culture"
date: 2015-04-20
image: "/images/biblio/dal-yong-jin.jpg"
author: "Christophe Masutti"
description: "Jin, Dal Yong. Digital Platforms, Imperialism and Political Culture. Routledge, 2015."
type: bibliographie
categories: ["bibliographie"]
auteurs: ["Dal Yong"]
---

In the networked twenty-first century, digital platforms have significantly influenced capital accumulation and digital culture. Platforms, such as social network sites (e.g. Facebook), search engines (e.g. Google), and smartphones (e.g. iPhone), are increasingly crucial because they function as major digital media intermediaries. Emerging companies in non-Western countries have created unique platforms, controlling their own national markets and competing with Western-based platform empires in the global markets. The reality though is that only a handful of Western countries, primarily the U.S., have dominated the global platform markets, resulting in capital accumulation in the hands of a few mega platform owners. This book contributes to the platform imperialism discourse by mapping out several core areas of platform imperialism, such as intellectual property, the global digital divide, and free labor, focusing on the role of the nation-state alongside transnational capital.

----

Jin, Dal Yong. Digital Platforms, Imperialism and Political Culture. Routledge, 2015.


**Lien vers le site de l'éditeur :** https://www.routledge.com/Digital-Platforms-Imperialism-and-Political-Culture/Jin-Curran/p/book/9781138859562

----
