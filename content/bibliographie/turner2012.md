---
title: "Aux sources de l’utopie numérique"
subtitle: "De la contre-culture à la cyberculture : Stewart Brand, un homme d’influence"
date: 2012-10-21
image: "/images/biblio/fred-turner.jpg"
author: "Christophe Masutti"
description: "Turner, Fred. Aux sources de l’utopie numérique. De la contre-culture à la cyberculture : Stewart Brand, un homme d’influence. C&F éditions, 2012."
type: bibliographie
categories: ["bibliographie"]
auteurs: ["Turner"]
---

Stewart Brand occupe une place essentielle, celle du passeur qui au-delà de la technique fait naître les rêves, les utopies et les justifications auto- réalisatrices. Depuis la fin des années soixante, il a construit et promu les mythes de l’informatique avec le Whole Earth Catalog, le magazine Wired ou le système de conférences électroniques du WELL et ses communautés virtuelles. Aux sources de l’utopie numérique nous emmène avec lui à la découverte du mouvement de la contre-culture et de son rôle déterminant dans l’histoire de l’internet.


----

Turner, Fred. *Aux sources de l’utopie numérique. De la contre-culture à la cyberculture : Stewart Brand, un homme d’influence*. C&F éditions, 2012.




**Lien vers le site de l'éditeur :** https://cfeditions.com/utopieNumerique/


----
