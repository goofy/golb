---
title: "Creditworthy"
subtitle: "A history of consumer surveillance and financial identity in America"
date: 2017-02-01
image: "/images/biblio/Josh-lauer.jpg"
author: "Christophe Masutti"
description: "Lauer, Josh. Creditworthy. A history of consumer surveillance and financial identity in America. Columbia University Press, 2017"
type: bibliographie
categories: ["bibliographie"]
auteurs: ["Lauer"]
---

The first consumer credit bureaus appeared in the 1870s and quickly amassed huge archives of deeply personal information. Today, the three leading credit bureaus are among the most powerful institutions in modern life — yet we know almost nothing about them. Experian, Equifax, and TransUnion are multi-billion-dollar corporations that track our movements, spending behavior, and financial status. This data is used to predict our riskiness as borrowers and to judge our trustworthiness and value in a broad array of contexts, from insurance and marketing to employment and housing. In *Creditworthy*, the first comprehensive history of this crucial American institution, Josh Lauer explores the evolution of credit reporting from its nineteenth-century origins to the rise of the modern consumer data industry. By revealing the sophistication of early credit reporting networks, Creditworthy highlights the leading role that commercial surveillance has played — ahead of state surveillance systems — in monitoring the economic lives of Americans. Lauer charts how credit reporting grew from an industry that relied on personal knowledge of consumers to one that employs sophisticated algorithms to determine a person's trustworthiness. Ultimately, Lauer argues that by converting individual reputations into brief written reports — and, later, credit ratings and credit scores — credit bureaus did something more profound: they invented the modern concept of financial identity. Creditworthy reminds us that creditworthiness is never just about economic "facts." It is fundamentally concerned with — and determines — our social standing as an honest, reliable, profit-generating person."

----

Lauer, Josh. *Creditworthy. A history of consumer surveillance and financial identity in America*. Columbia University Press, 2017.


**Lien vers le site de l'éditeur :** https://cup.columbia.edu/book/creditworthy/9780231168083

----
