---
title: "L’éthique hacker et l’esprit de l’ère de l’information"
date: 2001-04-20
image: "/images/biblio/Pekka-himanen.jpg"
author: "Christophe Masutti"
description: "Himanen, Pekka. L’éthique Hacker et l’esprit de l’ère de l’information. Exils, 2001"
type: bibliographie
categories: ["bibliographie"]
auteurs: ["Himanen"]
---

Himanen présente les termes de l’éthique hacker selon trois pôles, en l’opposant à l’éthique protestante caractéristique du capitalisme : l’éthique du travail, l’éthique de l’argent, et la néthique ou éthique du réseau. Dans l’éthique protestante du travail, il s’agit de vivre pour travailler. Le moteur principal de la mise au travail des hackers du logiciel libre consiste dans le plaisir, dans le jeu, dans l’engagement dans une passion. Pour Linus Torvalds « Linux a largement été un hobby (mais un sérieux, le meilleur de tous). »

Le deuxième plan qui caractérise l’éthique hacker porte sur l’argent. Le mobile de l’activité du hacker n’est pas l’argent. Un des fondements même du mouvement du logiciel libre, initié par les hackers, consiste précisément à rendre impossible l’appropriabilité privée de la production logicielle et donc la perspective d’en tirer profit. Là encore, on trouve comme mobiles qui président à l’engagement dans le travail coopératif volontaire la passion, la créativité, et la socialisation.

Un point particulier mentionné par Himanen, qui porte sur l’organisation et la coordination du travail chez les hackers, les hackers parviennent à s’affranchir du recours à l’autorité hiérarchique pour coordonner leurs activités, en lui substituant comme modalité principale la coopération directe.

L’éthique hacker selon Himanen, est « une nouvelle éthique du travail qui s’oppose à l’éthique protestante du travail telle que l’a définie Max Weber. » Elle constitue une innovation sociale susceptible d’avoir une portée qui dépasse largement les limites de l’activité informatique.

(Source : [Wikipedia](https://fr.wikipedia.org/wiki/L%27%C3%89thique_hacker))

----

Himanen, Pekka. *L’éthique Hacker et l’esprit de l’ère de l’information*. Exils, 2001.


**Lien vers le site de l'éditeur :** http://www.editions-exils.fr/exils/l-ethique-hacker-et-l-esprit-de-l-ere-de-l-information

----
