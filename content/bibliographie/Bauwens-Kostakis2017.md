---
title: "Manifeste pour une véritable économie collaborative : Vers une société des communs"
date: 2017-03-31
image: "/images/biblio/Michel-Bauwens.jpg"
author: "Christophe Masutti"
description: "Bauwens, M., & Kostakis, V. (2017). Manifeste pour une véritable économie collaborative : Vers une société des communs (O. Petitjean, Trad.). Éditions Charles Léopold Mayer."
type: bibliographie
categories: ["bibliographie"]
auteurs: ["Bauwens", "Kostakis"]
---

Partage de fichiers, distribution de musique, installation de logiciels, la technologie du peer-to-peer (P2P) permet différents types de coopération via un échange direct de données entre ordinateurs, sans passer par des serveurs centralisés. Mais ce genre d’utilisation a au fond une portée limitée, et si l’on adopte un point de vue plus large, le P2P peut être considéré comme un nouveau modèle de relations humaines. Dans cet ouvrage, Michel Bauwens et Vasilis Kostakis décrivent et expliquent l’émergence d’une dynamique du P2P fondée sur la protection et le développement des communs, et la replacent dans le cadre de l’évolution des différents modes de production. Cette nouvelle modalité de création et de distribution de la valeur, qui favorise les relations horizontales, crée les conditions pour une transition vers une nouvelle économie, respectueuse de la nature et des personnes, une véritable économie collaborative.

----

Bauwens, M., & Kostakis, V. (2017). *Manifeste pour une véritable économie collaborative : Vers une société des communs* (O. Petitjean, Trad.). Éditions Charles Léopold Mayer.

**Lien vers le site de l'éditeur :** http://www.eclm.fr/ouvrage-386.html

----
