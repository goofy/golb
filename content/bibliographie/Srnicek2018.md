---
title: "Capitalisme de plateforme"
subtitle: "L’hégémonie de l’économie numérique"
date: 2018-05-21
image: "/images/biblio/Nick-Srnicek.jpg"
author: "Christophe Masutti"
description: "Srnicek, Nick. Capitalisme de plateforme. L’hégémonie de l’économie numérique, Lux Éditeur, 2018."
type: bibliographie
categories: ["bibliographie"]
auteurs: ["Srnicek"]
---

Google et Facebook, Apple et Microsoft, Siemens et GE, Uber et Airbnb : les entreprises qui adoptent et perfectionnent le modèle d'affaires dominant aujourd'hui, celui des plateformes pair-à-pair du capitalisme numérique, s'enrichissent principalement par la collecte de données et le statut d'intermédiaire qu'il leur confère. Si elles prospèrent, ces compagnies peuvent créer leur propre marché, voire finir par contrôler une économie entière, un potentiel monopolistique inusité qui, bien qu'il s'inscrive dans la logique du capitalisme dit « classique », présente un réel danger aux yeux de quiconque s'applique à imaginer un futur postcapitaliste. Dans ce texte bref et d'une rare clarté, Nick Srnicek retrace la genèse de ce phénomène, analyse celui-ci de manière limpide et aborde la question de son impact sur l'avenir. Un livre essentiel pour comprendre comment les GAFA et autres géants du numérique transforment l'économie mondiale, et pour envisager des pistes d'action susceptibles d'en contrer les effets délétères.


----

Srnicek, Nick. *Capitalisme de plateforme. L’hégémonie de l’économie numérique*, Lux Éditeur, 2018.




**Lien vers le site de l'éditeur :** https://www.luxediteur.com/catalogue/capitalisme-de-plateforme/


----
